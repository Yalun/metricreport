var gulp  = require('gulp');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var karma = require('gulp-karma');
var jshint = require('gulp-jshint');
var jscs = require('gulp-jscs');

var appFiles = [
	'app/*.js',

	'app/common/**/*.js',

	'app/VaR-and-sVaR/*.js',

	'app/comprehensive-PnL/*.js',

	'app/inventory-turnover/*.js',

	'app/risk-factor-sensitivities/*.js',

	'app/risk-position-limits-usage/*.js',

	'app/inventory-aging/*.js',

	'app/table-of-content/*.js'
];

var testFiles = [
	'app/public/bower/angular/angular.js',
	'app/public/bower/angular-mocks/angular-mocks.js',
	"app/public/bower/angular-messages/angular-messages.min.js",
	"app/public/bower/angular-resource/angular-resource.min.js",
	"app/public/bower/angular-ui-router/release/angular-ui-router.min.js",
	"app/public/bower/angular-bootstrap/ui-bootstrap-tpls.min.js",
	"app/public/bower/angular-sanitize/angular-sanitize.min.js",
	"app/public/bower/angular-ui-grid/ui-grid.js",
	"app/public/bower/angular-ui-tree/dist/angular-ui-tree.min.js",
	"app/public/bower/ui-select/dist/select.min.js",
	"app/public/bower/angular-toastr/dist/angular-toastr.tpls.js",
	"app/public/bower/spin.js/spin.js",
	"app/public/bower/angular-spinner/angular-spinner.js",

	'app/*.js',

	'app/common/services/remote-services.js',
	'app/common/**/*.js',
	'app/**/config.js',

	'app/VaR-and-sVaR/*.js',

	'app/comprehensive-PnL/*.js',

	'app/inventory-turnover/*.js',

	'app/risk-factor-sensitivities/*.js',

	'app/risk-position-limits-usage/*.js',

	'app/inventory-aging/*.js',

	'app/table-of-content/*.js',

	'app/test/spec/**/*.spec.js'
];

var sassFiles = [
	'app/public/assets/sass/*.scss',
	// "!app/public/assets/sass/_*.scss"
];

gulp.task('default', ['serve', 'test', 'watch']);

gulp.task('reload', function () {
	browserSync.reload();
});

gulp.task('watch', function () {
	console.log('gulp watch...');
	gulp.watch(['*.js', '**/*.js', '**/**/*.js', '*.html', '**/*.html', '**/**/*.html'], {cwd: 'app'}, ['ci','reload']);
	gulp.watch(sassFiles, ['sass'], browserSync.reload);
	gulp.watch(['app/test/spec/**/*.spec.js'], ['ci']);
});

// Static server
gulp.task('serve', function() {
	browserSync.init({
		server: {
			baseDir: 'app'
		},
		port: 12345
	});
});

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
	return gulp.src(sassFiles)
		.pipe(sass())
		.on('error', sass.logError)
		.pipe(gulp.dest('app/public/assets/css'))
		.pipe(browserSync.stream());
});

gulp.task('sass-watcher', function () {
	console.log('sass watch...');
	gulp.watch(sassFiles, ['sass'], browserSync.reload);
});

// there is also a standalone version of unit test
// just open app/test/spec/SpecRunner.html in browser
gulp.task('test', function(coverage) {
	gulp.src(testFiles)
	.pipe(karma({
		configFile: 'karma.conf.js',
		action: 'run'
	}))
	// .on('error', function(err) {
	// // Make sure failed tests cause gulp to exit non-zero
	// 	throw err;
	// });
});

gulp.task('ci', function () {
	return gulp.src(testFiles)
	.pipe(karma({
		configFile: 'karma.conf.js',
		action: 'run'
	}));
});

gulp.task('watch-test', function () {
	gulp.watch(['app/test/spec/**/*.spec.js'], ['ci']);
});

gulp.task('js-style', function () {
	return gulp.src(appFiles)
		// .pipe(jscs())
		.pipe(jshint())
		.pipe(jshint.reporter('jshint-stylish', {verbose: true}))
		// .pipe(jshint.reporter('fail'));
});
