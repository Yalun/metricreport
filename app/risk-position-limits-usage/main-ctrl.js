angular.module('app.risk-position-limits-usage').controller('riskPositionLimitsUsageController',
		['$scope', '$state', 'riskPositionLimitsUsageReportServices', 'CacheService', '$controller', '$window', '$filter', 'toastr', 'ErrorService',
function ($scope, $state, ReportServices, CacheService, $controller, $window, $filter, toastr, ErrorService){

	var baseController = $controller('PanelController', {
		$scope: $scope,
		// ReportServices: ReportServices,
		moduleName: 'risk-position-limits-usage'
	});

	$scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
		if (toState.name.match(/[^.]*/i)[0] !== fromState.name.match(/[^.]*/i)[0]) {
			ReportServices.clearCache();
		} else {
			// console.log('change between sibiling routes within ', toState.name.match(/[^.]*/i)[0]);
			$scope.backTo = fromState.name;
		}
	});

	$scope.routeToCognosChart = function () {
		$window.open(ReportServices.CognosReport, '_blank', '');
	};

	baseController.routeToMultiDay();
}]);
