angular.module('app.VaR-and-sVaR').controller('VaRandSVaRController',
	['$scope',
	 'VaRandSVaRReportServices',
	 'templates',
	 '$controller',
	 'ErrorService',
	 function ($scope, ReportServices, templates, $controller, ErrorService) {

	var baseController = $controller('PanelController', {
		$scope: $scope,
		// ReportServices: ReportServices,
		moduleName: 'VaR-and-sVaR'
	});

	$scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {

		// clear all cache for previous panel when switching panel
		if (toState.name.match(/[^.]*/i)[0] !== fromState.name.match(/[^.]*/i)[0]) {
			// $scope.clearCache();
			ReportServices.clearCache();
		} else {
			// console.log('change between sibiling routes within ', toState.name.match(/[^.]*/i)[0]);
			$scope.backTo = fromState.name;
		}
	});

	// $scope.multiDayTemplates = templates[0];
	// $scope.dayTemplates = templates[1];
	// $scope.detailTemplates = templates[2];

	baseController.routeToMultiDay();
}]);
