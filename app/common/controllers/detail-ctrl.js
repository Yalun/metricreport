angular.module('app.common').controller('DetailController', ['$scope', '$state', '$location', '$anchorScroll', '$timeout', 'ErrorService', 'CommonHelperService', 'uiGridConstants', 'ReportServices', function ($scope, $state, $location, $anchorScroll, $timeout, ErrorService, CommonHelperService, uiGridConstants, ReportServices){

	$scope.$parent.activeTab = ''; //reset tab group
	$scope.deskName = decodeURIComponent($state.params.desk);
	$scope.date = decodeURIComponent($state.params.date);
	$scope.deskID = decodeURIComponent($state.params.deskID);
	// $scope.selectedTemplate = $scope.detailTemplates[0];
	// $scope.saveState = function () {
	// 	var newState = $scope.gridApi.saveState.save();
	// 	CommonHelperService.saveTemplate($scope, $scope.selectedTemplate, $scope.detailTemplates, newState);
	// };

	// $scope.loadState = function () {
	// 	CommonHelperService.loadTemplate($scope, $scope.selectedTemplate, $scope.detailTemplates, $scope.gridApi.saveState.restore);
	// };

	//TODO: this likely only use in Panel 1, move later
	$scope.gridScope = {
		documentModal: $scope.documentModal,
		commentModal: $scope.commentModal,
		// showPosition: showPosition
	};

	$scope.goBack = function () {
		$state.go($scope.backTo); // $scope.backTo from ./main-ctrl.js
	};

	$scope.showAuditHistory = function () {
		ReportServices.AuditHistory.query({p_iReviewId: $scope.desk.ReviewID}, function(data) {
			$scope.auditHistory = data;
			$scope.isHistoryShown = !$scope.isHistoryShown;

			$location.hash('audit-history');
			$timeout(function () {
				$anchorScroll();
			}, 0);
		}, function (error) {
			$scope.historyError = ErrorService.handleError(error.data);
		});
	};

}]);
