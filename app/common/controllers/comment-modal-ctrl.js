var common = angular.module('app.common')
	.controller('CommentModalController', ['$scope', '$http', '$modalInstance','row', 'reviewID', function ($scope, $http, $modalInstance, row, reviewID ){
		var ReviewCommentsURL =  '../volckeroverrideswebapi/ReviewComments';

		if (row) {
			var params = {
				p_iReviewID: row.grid.parentRow && row.grid.parentRow.entity.ReviewID || reviewID,
				p_dMeasureId: row.entity.dMeasureId
			};
		} else {
			var params = {
				p_iReviewID: reviewID,
			}
		}

		$scope.comment = '';

		$http.get(ReviewCommentsURL,{
			params: {
				'p_iReviewID': params.p_iReviewID,
				'p_dMeasureId': params.p_dMeasureId //for panel 1 only
			}
		}).success(function (data) {
			$scope.previousComments = data;
		}).error(function (error) {
			$scope.error = error.Message || 'error';
		});

		$scope.update = function () {
			$scope.error = null;
			params.p_sComment = $scope.comment;

			$http.post(ReviewCommentsURL + '?p_iReviewID='+ params.p_iReviewID + '&p_dMeasureId=' + params.p_dMeasureId + '&p_sComment=' + encodeURIComponent(params.p_sComment) )
			.success(function (success) {
				$modalInstance.dismiss('update');
			})
			.error(function (error) {
				$scope.error = error.Message || 'error';
				console.log(error);
			});
		};

		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};

	}]);
