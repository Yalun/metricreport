var common = angular.module('app.common',[]);
common.factory('RemoteService', function(){

	// var env = 'prod'; //for Production (Cognos has different endpoints)
	var env = 'dev'; // or you call it UAT
	// var env = 'local';

	var urls = {
		'volckerDesksURL' : {
				'dev': '../volckeroverrideswebapi/VolckerDesk',
				'prod': '../volckeroverrideswebapi/VolckerDesk',
				'local': 'data/desks.json'
		},
		'deskRiskFactorsURL': {
			'dev': '../marketriskwebapi/VolckerDeskRiskMeasure',
			'prod': '../marketriskwebapi/VolckerDeskRiskMeasure',
			'local': 'data/risk-factors.json'
		},
		'holidaysURL' : {
			'dev': '',
			'prod': '',
			'local': 'data/holidays.json'
		},
		'reviewCommentsURL' : {
			'dev': '../volckeroverrideswebapi/ReviewComments',
			'prod': '../volckeroverrideswebapi/ReviewComments',
			'local': '../volckeroverrideswebapi/ReviewComments'
		},
		'review': {
			'dev': '../volckeroverrideswebapi/Review',
			'prod': '../volckeroverrideswebapi/Review',
			'local': '../volckeroverrideswebapi/Review'
		},
		'OverrideURL': {
			'dev': '../volckeroverrideswebapi/LimitUsageOverride',
			'prod': '../volckeroverrideswebapi/LimitUsageOverride',
			'local': '../volckeroverrideswebapi/LimitUsageOverride'
		},
		'RFSOverrideURL': {
			'dev': '../volckeroverrideswebapi/RFSOverride',
			'prod': '../volckeroverrideswebapi/RFSOverride',
			'local': '../volckeroverrideswebapi/RFSOverride',
		},
		'AgeOverrideURL' : {
			'dev': '../volckeroverrideswebapi/AgeOverride',
			'prod': '../volckeroverrideswebapi/AgeOverride',
			'local': '../volckeroverrideswebapi/AgeOverride',
		},
		'CognosDeskLevelURL' : {
			'dev': 'http://msusa-report-uat/ibmcognos/cgi-bin/cognosisapi.dll?b_action=cognosViewer&ui.action=run',
			'prod': 'http://msusa-report/ibmcognos/cgi-bin/cognosisapi.dll?b_action=cognosViewer&ui.action=run',
			'local': 'http://msusa-report-uat/ibmcognos/cgi-bin/cognosisapi.dll?b_action=cognosViewer&ui.action=run'
		},
		'SharepointURL' : {
			'dev': '../volckeroverrideswebapi/SharepointFile',
			'prod': '../volckeroverrideswebapi/SharepointFile',
			'local': '../volckeroverrideswebapi/SharepointFile'
		}
	};

	var reportUrls = {
		'riskPositionLimitsUsage' : {
			'multiDayDataURL': {
					'dev': '../volckeroverrideswebapi/LimitUsage',
					'prod': '../volckeroverrideswebapi/LimitUsage',
					'local': 'data/panel-1/multi-day-sample.json'
			},
			'dayReportDataURL' : {
					'dev': '../volckeroverrideswebapi/LimitUsage',
					'prod': '../volckeroverrideswebapi/LimitUsage',
					'local': 'data/panel-1/day-sample.json'
			},
			'detailReportDataURL' : {
					'dev': '../volckeroverrideswebapi/LimitUsage',
					'prod': '../volckeroverrideswebapi/LimitUsage',
					'local': 'data/panel-1/detail-sample.json'
			},
			'tradedProductsURL' : {
				'dev': '../volckeroverrideswebapi/TradedProduct',
				'prod': '../volckeroverrideswebapi/TradedProduct',
				'local': 'data/panel-1/traded-products.json'
			},
			'unauthorizedProductsURL' : {
				'dev': '../volckeroverrideswebapi/TradedProduct',
				'prod': '../volckeroverrideswebapi/TradedProduct',
				'local': 'data/panel-1/breached-products.json'
			},
			'positionDetailReportDataURL' : {
				'dev': '../volckeroverrideswebapi/LimitUsage',
				'prod': '../volckeroverrideswebapi/LimitUsage',
				'local': 'data/panel-1/position-level-sample-2.json'
			},
			'auditHistoryURL' : {
				'dev': '../volckeroverrideswebapi/LimitUsageOverride',
				'prod': '../volckeroverrideswebapi/LimitUsageOverride',
				'local': 'data/panel-1/audit-history-sample.json'
			},
			'ExportToCsvURL' : {
				'dev': '../volckeroverrideswebapi/LimitUsageExcelExport?',
				'prod': '../volckeroverrideswebapi/LimitUsageExcelExport?',
				'local': '../volckeroverrideswebapi/LimitUsageExcelExport?'
			},
			'CognosChartViewURL': {
				'dev': 'http://SNYCCGNSWNUAT01:80/ibmcognos/cgi-bin/cognosisapi.dll?b_action=cognosViewer&ui.action=run&ui.object=%2fcontent%2ffolder%5b%40name%3d%27Corporate%27%5d%2ffolder%5b%40name%3d%27Volcker%27%5d%2ffolder%5b%40name%3d%27Panel%201%27%5d%2ffolder%5b%40name%3d%27Chart%20View%27%5d%2freport%5b%40name%3d%27Volcker%20Metrics%2c%20Panel%201%20%e2%80%93%20Risk%20and%20Position%20Limits%20and%20Usage%20(Desk%20Level)%20Chart%27%5d&ui.name=Volcker%20Metrics%2c%20Panel%201%20%e2%80%93%20Risk%20and%20Position%20Limits%20and%20Usage%20(Desk%20Level)%20Chart&run.outputFormat=spreadsheetML&run.prompt=true',

				'prod': 'http://msusa-report/ibmcognos/cgi-bin/cognosisapi.dll?b_action=cognosViewer&ui.action=run&ui.object=%2fcontent%2ffolder%5b%40name%3d%27Corporate%27%5d%2ffolder%5b%40name%3d%27Volcker%27%5d%2ffolder%5b%40name%3d%27Panel%201%27%5d%2ffolder%5b%40name%3d%27Chart%20View%27%5d%2freport%5b%40name%3d%27Volcker%20Metrics%2c%20Panel%201%20%e2%80%93%20Risk%20and%20Position%20Limits%20and%20Usage%20(Desk%20Level)%20Chart%27%5d&ui.name=Volcker%20Metrics%2c%20Panel%201%20%e2%80%93%20Risk%20and%20Position%20Limits%20and%20Usage%20(Desk%20Level)%20Chart&run.outputFormat=spreadsheetML&run.prompt=true',

				'local': 'http://SNYCCGNSWNUAT01:80/ibmcognos/cgi-bin/cognosisapi.dll?b_action=cognosViewer&ui.action=run&ui.object=%2fcontent%2ffolder%5b%40name%3d%27Corporate%27%5d%2ffolder%5b%40name%3d%27Volcker%27%5d%2ffolder%5b%40name%3d%27Panel%201%27%5d%2ffolder%5b%40name%3d%27Chart%20View%27%5d%2freport%5b%40name%3d%27Volcker%20Metrics%2c%20Panel%201%20%e2%80%93%20Risk%20and%20Position%20Limits%20and%20Usage%20(Desk%20Level)%20Chart%27%5d&ui.name=Volcker%20Metrics%2c%20Panel%201%20%e2%80%93%20Risk%20and%20Position%20Limits%20and%20Usage%20(Desk%20Level)%20Chart&run.outputFormat=spreadsheetML&run.prompt=true'
			}
		},
		'RiskFactorSensitivities' : {
			'multiDayDataURL': {
				'dev': '../volckeroverrideswebapi/RFS',
				'prod': '../volckeroverrideswebapi/RFS',
				'local': 'data/risk-factor-sensitivities/multi-day.json'
			},
			'dayReportDataURL' : {
				'dev': '../volckeroverrideswebapi/RFS',
				'prod': '../volckeroverrideswebapi/RFS',
				'local': 'data/risk-factor-sensitivities/day.json'
			},
			'detailReportDataURL' : {
				'dev': '../volckeroverrideswebapi/RFS',
				'prod': '../volckeroverrideswebapi/RFS',
				'local': 'data/risk-factor-sensitivities/detail.json'
			},
			'positionDetailReportDataURL' : {
				'dev': '../volckeroverrideswebapi/RFS',
				'prod': '../volckeroverrideswebapi/RFS',
				'local': 'data/risk-factor-sensitivities/position.json'
			},
			'auditHistoryURL': {
				'dev': '../volckeroverrideswebapi/RFSOverride',
				'prod': '../volckeroverrideswebapi/RFSOverride',
				'local': 'data/risk-factor-sensitivities/audit-history-sample.json',
			},
			'ExportToCsvURL' : {
				'dev': '../volckeroverrideswebapi/RFSExcelExport?',
				'prod': '../volckeroverrideswebapi/RFSExcelExport?',
				'local': '../volckeroverrideswebapi/RFSExcelExport?'
			},
		},
		'VaRandSVaR' : {
			'multiDayDataURL': {
				'dev': '../volckeroverrideswebapi/VAR',
				'prod': '../volckeroverrideswebapi/VAR',
				'local': 'data/VaR-and-sVaR/multi-day.json'
			},
			'dayReportDataURL' : {
				'dev': '../volckeroverrideswebapi/VAR',
				'prod': '../volckeroverrideswebapi/VAR',
				'local': 'data/VaR-and-sVaR/day.json'
			},
			'detailReportDataURL' : {
				'dev': '../volckeroverrideswebapi/VAR',
				'prod': '../volckeroverrideswebapi/VAR',
				'local': 'data/VaR-and-sVaR/detail.json'
			},
			'positionDetailReportDataURL' : {
				'dev': '../volckeroverrideswebapi/VAR',
				'prod': '../volckeroverrideswebapi/VAR',
				'local': 'data/VaR-and-sVaR/position.json'
			},
			'auditHistoryURL': {
				'dev': '../volckeroverrideswebapi/VAROverride',
				'prod': '../volckeroverrideswebapi/VAROverride',
				'local': 'data/risk-factor-sensitivities/audit-history-sample.json',
			},
			'ExportToCsvURL' : {
				'dev': '../volckeroverrideswebapi/VARExcelExport?',
				'prod': '../volckeroverrideswebapi/VARExcelExport?',
				'local': '../volckeroverrideswebapi/VARExcelExport?'
			}
		},
		'ComprehensivePnL' : {
			// Sub panel A
			'multiDayDataURL': {
				'dev': '../volckeroverrideswebapi/PL',
				'prod': '../volckeroverrideswebapi/PL',
				'local': 'data/comprehensive-PnL/a/multi-day.json'
			},
			'dayReportDataURL' : {
				'dev': '../volckeroverrideswebapi/PL',
				'prod': '../volckeroverrideswebapi/PL',
				'local': 'data/comprehensive-PnL/a/day.json'
			},
			'detailReportDataURL' : {
				'dev': '../volckeroverrideswebapi/PL',
				'prod': '../volckeroverrideswebapi/PL',
				'local': 'data/comprehensive-PnL/a/detail.json'
			},
			'positionDetailReportDataURL' : {
				'dev': '../volckeroverrideswebapi/PL',
				'prod': '../volckeroverrideswebapi/PL',
				'local': 'data/comprehensive-PnL/a/position.json'
			},
			'auditHistoryURL': {
				'dev': '../volckeroverrideswebapi/PLOverride',
				'prod': '../volckeroverrideswebapi/PLOverride',
				'local': 'data/comprehensive-PnL/a/audit-history-sample.json',
			},
			'ExportToCsvURL' : {
				'dev': '../volckeroverrideswebapi/PLExcelExport?',
				'prod': '../volckeroverrideswebapi/PLExcelExport?',
				'local': '../volckeroverrideswebapi/PLExcelExport?'
			}
		},
		'ComprehensivePnLB' : {
			// Sub panel B
			'multiDayDataURL': {
				'dev': '../volckeroverrideswebapi/PLByRFS',
				'prod': '../volckeroverrideswebapi/PLByRFS',
				'local': 'data/comprehensive-PnL/b/multi-day.json'
			},
			'dayReportDataURL' : {
				'dev': '../volckeroverrideswebapi/PLByRFS',
				'prod': '../volckeroverrideswebapi/PLByRFS',
				'local': 'data/comprehensive-PnL/b/day.json'
			},
			'detailReportDataURL' : {
				'dev': '../volckeroverrideswebapi/PLByRFS',
				'prod': '../volckeroverrideswebapi/PLByRFS',
				'local': 'data/comprehensive-PnL/b/detail-tree.json'
			},
			'positionDetailReportDataURL' : {
				'dev': '../volckeroverrideswebapi/PLByRFS',
				'prod': '../volckeroverrideswebapi/PLByRFS',
				'local': 'data/comprehensive-PnL/b/position.json'
			},
			'auditHistoryURL': {
				'dev': '../volckeroverrideswebapi/PLBYRFSOverride',
				'prod': '../volckeroverrideswebapi/PLBYRFSOverride',
				'local': 'data/comprehensive-PnL/b/audit-history-sample.json',
			},
			'ExportToCsvURL' : {
				'dev': '../volckeroverrideswebapi/PLByRFSExcelExport?',
				'prod': '../volckeroverrideswebapi/PLByRFSExcelExport?',
				'local': '../volckeroverrideswebapi/PLByRFSExcelExport?'
			}
		},
		'inventoryTurnover' : {
			'multiDayDataURL': {
				'dev': '../volckeroverrideswebapi/InventoryTurnover',
				'prod': '../volckeroverrideswebapi/InventoryTurnover',
				'local': 'data/inventory-turnover/multi-day.json'
			},
			'dayReportDataURL' : {
				'dev': '../volckeroverrideswebapi/InventoryTurnover',
				'prod': '../volckeroverrideswebapi/InventoryTurnover',
				'local': 'data/inventory-turnover/day.json'
			},
			'detailReportDataURL' : {
				'dev': '../volckeroverrideswebapi/InventoryTurnover',
				'prod': '../volckeroverrideswebapi/InventoryTurnover',
				'local': 'data/inventory-turnover/detail.json'
			},
			'positionDetailReportDataURL' : {
				'dev': '../volckeroverrideswebapi/InventoryTurnover',
				'prod': '../volckeroverrideswebapi/InventoryTurnover',
				'local': 'data/inventory-turnover/position.json'
			},
			'auditHistoryURL': {
				'dev': '../volckeroverrideswebapi/InventoryTurnoverOverride',
				'prod': '../volckeroverrideswebapi/InventoryTurnoverOverride',
				'local': 'data/inventory-turnover/audit-history-sample.json',
			},
			'ExportToCsvURL' : {
				'dev': '../volckeroverrideswebapi/InventoryTurnoverExcelExport?',
				'prod': '../volckeroverrideswebapi/InventoryTurnoverExcelExport?',
				'local': '../volckeroverrideswebapi/InventoryTurnoverExcelExport?'
			}
		},
		'inventoryAging' : {
			'multiDayDataURL': {
				'dev': '',
				'prod': '',
				'local': 'data/inventory-aging/multi-day.json'
			},
			'dayReportDataURL' : {
				'dev': '',
				'prod': '',
				'local': 'data/inventory-aging/day.json'
			},
			'detailReportDataURL' : {
				'dev': '',
				'prod': '',
				'local': 'data/inventory-aging/detail.json'
			},
			'positionDetailReportDataURL' : {
				'dev': '',
				'prod': '',
				'local': 'data/inventory-aging/position.json'
			},
			'auditHistoryURL': {
				'dev': '../volckeroverrideswebapi/',
				'prod': '../volckeroverrideswebapi/',
				'local': 'data/inventory-aging/audit-history-sample.json',
			},
			'ExportToCsvURL' : {
				// 'dev': '../volckeroverrideswebapi/VARExcelExport?',
				// 'prod': '../volckeroverrideswebapi/VARExcelExport?',
				// 'local': '../volckeroverrideswebapi/VARExcelExport?'
			}
		},
		'customerFacingTradeRatio' : {
			'multiDayDataURL': {
				'dev': '../volckeroverrideswebapi/CFTR',
				'prod': '../volckeroverrideswebapi/CFTR',
				'local': 'data/VaR-and-sVaR/multi-day.json'
			},
			'dayReportDataURL' : {
				'dev': '../volckeroverrideswebapi/CFTR',
				'prod': '../volckeroverrideswebapi/CFTR',
				'local': 'data/VaR-and-sVaR/day.json'
			},
			'detailReportDataURL' : {
				'dev': '../volckeroverrideswebapi/CFTR',
				'prod': '../volckeroverrideswebapi/CFTR',
				'local': 'data/VaR-and-sVaR/detail.json'
			},
			'positionDetailReportDataURL' : {
				'dev': '../volckeroverrideswebapi/CFTR',
				'prod': '../volckeroverrideswebapi/CFTR',
				'local': 'data/VaR-and-sVaR/position.json'
			},
			'auditHistoryURL': {
				'dev': '../volckeroverrideswebapi/CFTROverride',
				'prod': '../volckeroverrideswebapi/CFTROverride',
				'local': 'data/risk-factor-sensitivities/audit-history-sample.json',
			},
			'ExportToCsvURL' : {
				'dev': '../volckeroverrideswebapi/CFTRExcelExport?',
				'prod': '../volckeroverrideswebapi/CFTRExcelExport?',
				'local': '../volckeroverrideswebapi/CFTRExcelExport?'
			}
		}
	};

	return {
		'getEndpoint' : function(path) {
			return urls[path][env];
		},
		'getReportEndpoint' : function (panel, view) {
			return reportUrls[panel][view][env];
		}
	};
});
