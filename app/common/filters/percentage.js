angular.module('app').filter('percentage', ['$filter', function ($filter) {
  return function (input, decimals) {
	if (input === null || input === undefined) {
		return input;
	}
    return $filter('number')(input * 100, decimals) + '%';
  };
}]);