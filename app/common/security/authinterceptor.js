/**
* app.security Module
*
* Description
* This module handles adding the security token
* to every request from the client app. 
*/
angular.module('app.security', []);

angular.module('app.security').factory('AuthInterceptor', function(AuthToken){
            return {
             request: addToken
            };

            function addToken(config){
                var token = AuthToken.getToken();
                if (token){
                    config.headers = config.headers || {};
                    config.headers.Authorization = 'Bearer ' + token;
                }
                return config;
            }

        });

angular.module('app.security').factory('AuthToken', function($window){
            var store = $window.localStorage;
            var key = 'auth-token';

            return {
                getToken: getToken,
                setToken: setToken
            };

            function getToken(){
                return store.getItem(key);
            }

            function setToken(token){
                if(token){
                    store.setItem(key, token);
                } else {
                    store.removeItem(key);
                }
            }
        })