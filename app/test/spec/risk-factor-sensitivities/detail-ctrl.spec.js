'use strict';

describe('Panel 2 (Risk Factor Sensitivities) - detail', function () {
	var ctrl,
		mainCtrl,
		ctrlDeps,
		baseDetailCtrl,
		baseDetailCtrlDeps,
		$scope;

	beforeEach(module('app.risk-factor-sensitivities', 'app.common', 'ui.router', 'app'));

	beforeEach(inject(function ($controller, $rootScope, $modal, $state, $filter, $timeout, ErrorService, $http, uiGridConstants, $location, $anchorScroll, RiskFactorSensitivitiesReportServices, CacheService, usSpinnerService, CommonHelperService, $window) {

		$scope = $rootScope.$new();

		baseDetailCtrlDeps = {
			$scope: $scope,
			ErrorService: ErrorService,
			CacheService: CacheService,
			ReportServices: RiskFactorSensitivitiesReportServices,
			$state: $state,
			$timeout: $timeout,
			$anchorScroll: $anchorScroll,
			CommonHelperService: CommonHelperService
		};

		baseDetailCtrl = $controller('DetailController', baseDetailCtrlDeps);

		mainCtrl = $controller('RiskFactorSensitivitiesController', {
			$scope: $scope,
			$state: $state,
			ReportServices: RiskFactorSensitivitiesReportServices,
			CacheService: CacheService,
			$controller: $controller,
			$window: $window,
			$modal: $modal,
			$filter: $filter,
			templates: [ [], [], [] ]
		});
		// console.dir(baseDetailCtrl);

		ctrlDeps = {
			$scope: $scope,
			$controller: $controller,
			$filter: $filter,
			$http: $http,
			$modal: $modal,
			$timeout: $timeout,
			$location: $location,
			$anchorScroll: $anchorScroll,
			ErrorService: ErrorService,
			uiGridConstants: uiGridConstants,
			ReportServices: RiskFactorSensitivitiesReportServices,
			CacheService: CacheService,
			usSpinnerService: usSpinnerService
		};

		ctrl = $controller('RiskFactorSensitivitiesDetailController', ctrlDeps);
	}));
	
	it('should be defined', function () {
		expect(ctrl).toBeDefined();
		// console.dir(ctrl);
	});

	describe('reviewReport', function () {
		// var $httpBackend;
		beforeEach(inject(function($injector) {
			// define fake function
			$scope.gridApi = {
				selection : {
					getSelectedRows: function () {
						return [{report:1}];
					}
				}
			};
			
			// $httpBackend = $injector.get('$httpBackend');
		}));

		it('should be defined', function () {
			expect($scope.reviewReport).toBeDefined();
		});

	});

	describe('detailGridOptions', function () {

		it('should be defined', function () {
			expect($scope.detailGridOptions).toBeDefined();
		});

		it('should have columnDefs', function () {
			expect($scope.detailGridOptions.columnDefs).toBeDefined();
		});

		it('should have virtualizationThreshold', function () {
			expect($scope.detailGridOptions.virtualizationThreshold).toBeDefined();
		});

		it('should have expandableRowTemplate', function () {
			expect($scope.detailGridOptions.expandableRowTemplate).toEqual('risk-factor-sensitivities/detailSubgridTemplate.html');
		});

	});

	describe('exportToExcel', function () {
		it('should be defined', function () {
			expect($scope.exportToExcel).toBeDefined();
		});

		// it('should open excel file in a new window', function () {

		// 	$scope.gridApi = {
		// 		core : {
		// 			getVisibleRows: function(grid) {
		// 				return [{},{},{}];
		// 			}
		// 		}
		// 	};

		// 	spyOn( ctrlDeps.$window, 'open' ).and.callFake( function() {
		// 		return true;
		// 	} );

		// 	$scope.exportToExcel();
		// 	expect(ctrlDeps.$window.open).toHaveBeenCalled();
		// });

		// it('should alert if no report is being displayed', function () {

		// 	$scope.gridApi = {
		// 		core : {
		// 			getVisibleRows: function(grid) {
		// 				return [];
		// 			}
		// 		}
		// 	};

		// 	spyOn( ctrlDeps.$window, 'alert' ).and.callFake( function() {
		// 		return true;
		// 	} );

		// 	$scope.exportToExcel();
		// 	expect(ctrlDeps.$window.alert).toHaveBeenCalled();
		// });
	});

	describe('getDetailReport', function() {

		it('should be defined', function() {
			expect($scope.getDetailReport).toBeDefined();
		});

		it('should call ReportServices DetailReport', function () {
			spyOn(ctrlDeps.ReportServices.DetailReport, 'query').and.callFake(function() {
				return [];
			});
			$scope.getDetailReport();
			expect(ctrlDeps.ReportServices.DetailReport.query).toHaveBeenCalled();
		});

		it('should set noReportData flag to false if no data comes back', function () {
			// $scope.getDetailReport();
			// expect($scope.noReportData).toBe(true);
		});

	});

	describe('disableReview', function() {
		it('should disable review button when it is clicked or no report has been generated', function () {
			$scope.isReviewing = true;
			expect($scope.disableReview()).toBe(true);
			$scope.desk = null;
			expect($scope.disableReview()).toBe(true);
		});
	});

	describe('subGridColDefs', function() {
		it('should be defined', function () {
			expect($scope.subGridColDefs).toBeDefined();
		});
	});
});