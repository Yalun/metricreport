'use strict';

describe('Panel 2 (Risk Factor Sensitivities) - main', function () {
	var ctrl,
		ctrlDeps,
		panelCtrl,
		panelCtrlDeps,
		$scope,
		moduleName;

	beforeEach(module('app.risk-factor-sensitivities', 'app.common', 'ui.router', 'app'));

	beforeEach(inject(function ($controller, $rootScope, $state, CacheService, RiskFactorSensitivitiesReportServices, $filter, ErrorService, CommonHelperService) {

		moduleName = 'risk-factor-sensitivities';

		$scope = $rootScope.$new();
		// console.log($scope);
		panelCtrlDeps = {
			$scope: $scope,
			$state: $state,
			$filter: $filter,
			// ReportServices: RiskFactorSensitivitiesReportServices,
			ErrorService: ErrorService,
			CommonHelperService: CommonHelperService,
			moduleName: moduleName
		};
		// base panel controller which contains common shared functionality for all */main.js
		panelCtrl = $controller('PanelController', panelCtrlDeps );

		ctrlDeps = {
			$scope: $scope,
			$controller: $controller,
			$state: $state,
			// PanelController: panelCtrl,
			ReportServices: RiskFactorSensitivitiesReportServices,
			CacheService: CacheService,
			templates: [ [], [], [] ]
		};

		ctrl = $controller('RiskFactorSensitivitiesController', ctrlDeps );
	}));

	it('should be defined', function () {
		expect(ctrl).toBeDefined();
		// console.dir(ctrl);
	});

	// it('should have module name: "risk-factor-sensitivities"', function () {
	// 	expect($scope.moduleName).toEqual('risk-factor-sensitivities');
	// });

	// describe('cellStatusClass', function () {
	// 	it('should accept a boolean flag and return a function', function () {
	// 		expect(typeof $scope.cellStatusClass(false)).toBe('function');
	// 		expect(typeof $scope.cellStatusClass(true)).toBe('function');

	// 	});

	// 	it ('should return a function which used as UI Grid\'s cellClass function, which return text-right by default', function () {
	// 		var grid = {};
	// 		var row = {
	// 			entity: {
	// 				NameOfRiskFactorSensitivity: 'whatever',
	// 				IR_DV01 : {
	// 					val: 12345,
	// 					bIsOverridden: false
	// 				}
	// 			}
	// 		};
	// 		var col = {
	// 			field: 'IR_DV01.val'
	// 		};
	// 		var fn = $scope.cellStatusClass();
	// 		expect(fn(grid, row, col)).toEqual('text-right');

	// 		var fn2 = $scope.cellStatusClass(true);
	// 		expect(fn2(grid, row, col)).toEqual('text-right');

	// 	});

	// 	it ('the return function should add "editable" class if  NameOfRiskFactorSensitivity is "Aggregate Change in Value across All Positions and boolean flag set to true" ', function () {
	// 		var grid = {};
	// 		var row = {
	// 			entity: {
	// 				NameOfRiskFactorSensitivity: 'Aggregate Change in Value across All Positions',
	// 				IR_DV01 : {
	// 					val: 12345,
	// 					bIsOverridden: false
	// 				}
	// 			}
	// 		};
	// 		var col = {
	// 			field: 'IR_DV01.val'
	// 		};
	// 		var fn = $scope.cellStatusClass();
	// 		expect(fn(grid, row, col)).toEqual('text-right');

	// 		var fn2 = $scope.cellStatusClass(true);
	// 		expect(fn2(grid, row, col)).toEqual('text-right editable');

	// 	});

	// 	it ('the return function should add overridden highlighting CSS class to the cell if the column has bIsOverridden = true', function () {
	// 		var grid = {};
	// 		var row = {
	// 			entity: {
	// 				NameOfRiskFactorSensitivity: 'Aggregate Change in Value across All Positions',
	// 				IR_DV01 : {
	// 					val: 12345,
	// 					bIsOverridden: true
	// 				}
	// 			}
	// 		};
	// 		var col = {
	// 			field: 'IR_DV01.val'
	// 		};

	// 		var fn = $scope.cellStatusClass();
	// 		expect(fn(grid, row, col)).toEqual('text-right revised');

	// 		var fn2 = $scope.cellStatusClass(true);
	// 		expect(fn2(grid, row, col)).toEqual('text-right editable revised');

	// 	});
	// });
	
	describe('when State changes from one panel to another', function () {
		var toState = {name: 'some-other-panel'};
		var toParams = {};
		var fromState = {name: 'risk-factor-sensitivities'};
		var fromParams = {};
		
		it('should clear the cache stored in CacheService', function () {
			spyOn(ctrlDeps.ReportServices, 'clearCache');
			$scope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
			expect(ctrlDeps.ReportServices.clearCache).toHaveBeenCalled();
		});
	});

	describe('when State changes within same module (AKA panel)', function () {
		it('from MultiDay to Day, should not clear the cache stored in CacheService', function () {
			var toState = {name: 'risk-factor-sensitivities.day'};
			var toParams = {};
			var fromState = {name: 'risk-factor-sensitivities.multi-day'};
			var fromParams = {};

			spyOn(ctrlDeps.ReportServices, 'clearCache');
			$scope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
			expect(ctrlDeps.ReportServices.clearCache).not.toHaveBeenCalled();
		});

		it('from MultiDay to Detail, should not clear the cache stored in CacheService', function () {
			var toState = {name: 'risk-factor-sensitivities.detail'};
			var toParams = {};
			var fromState = {name: 'risk-factor-sensitivities.multi-day'};
			var fromParams = {};

			spyOn(ctrlDeps.ReportServices, 'clearCache');
			$scope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
			expect(ctrlDeps.ReportServices.clearCache).not.toHaveBeenCalled();
		});

		it('from Day to MultiDay, should not clear the cache stored in CacheService', function () {
			var toState = {name: 'risk-factor-sensitivities.multi-day'};
			var toParams = {};
			var fromState = {name: 'risk-factor-sensitivities.day'};
			var fromParams = {};

			spyOn(ctrlDeps.ReportServices, 'clearCache');
			$scope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
			expect(ctrlDeps.ReportServices.clearCache).not.toHaveBeenCalled();
		});

		it('from Day to Detail, should not clear the cache stored in CacheService', function () {
			var toState = {name: 'risk-factor-sensitivities.detail'};
			var toParams = {};
			var fromState = {name: 'risk-factor-sensitivities.day'};
			var fromParams = {};

			spyOn(ctrlDeps.ReportServices, 'clearCache');
			$scope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
			expect(ctrlDeps.ReportServices.clearCache).not.toHaveBeenCalled();
		});

		it('from Detail to MultiDay, should not clear the cache stored in CacheService', function () {
			var toState = {name: 'risk-factor-sensitivities.multi-day'};
			var toParams = {};
			var fromState = {name: 'risk-factor-sensitivities.detail'};
			var fromParams = {};

			spyOn(ctrlDeps.ReportServices, 'clearCache');
			$scope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
			expect(ctrlDeps.ReportServices.clearCache).not.toHaveBeenCalled();
		});

		it('from Detail to Day, should not clear the cache stored in CacheService', function () {
			var toState = {name: 'risk-factor-sensitivities.day'};
			var toParams = {};
			var fromState = {name: 'risk-factor-sensitivities.detail'};
			var fromParams = {};

			spyOn(ctrlDeps.ReportServices, 'clearCache');
			$scope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
			expect(ctrlDeps.ReportServices.clearCache).not.toHaveBeenCalled();
		});

	});
});