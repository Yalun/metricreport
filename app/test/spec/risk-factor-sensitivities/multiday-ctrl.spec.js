'use strict';

describe('Panel 2 (Risk Factor Sensitivities) - multi-day', function () {
	var ctrl,
		mainCtrl,
		ctrlDeps,
		baseMultiDayCtrl,
		baseMultiDayCtrlDeps,
		VolckerDesks,
		$scope;

	beforeEach(module('app.risk-factor-sensitivities', 'app.common', 'ui.router', 'app'));

	beforeEach(inject(function ($controller, $rootScope, $q, $http, $filter, $modal, $window, $timeout, ErrorService, uiGridConstants, RiskFactorSensitivitiesReportServices, CacheService, usSpinnerService, toastr, $state) {

		$scope = $rootScope.$new();

		VolckerDesks = [
			{sVolckerDeskName: 'Desk 1', VolckerDeskID: 1},
			{sVolckerDeskName: 'Desk 2', VolckerDeskID: 2},
			{sVolckerDeskName: 'Desk 3', VolckerDeskID: 3}
		];

		baseMultiDayCtrlDeps = {
			$scope: $scope,
			$filter: $filter,
			$timeout: $timeout,
			ErrorService: ErrorService,
			CacheService: CacheService,
			ReportServices: RiskFactorSensitivitiesReportServices,
			VolckerDesks: VolckerDesks
		};

		baseMultiDayCtrl = $controller('MultiDayController', baseMultiDayCtrlDeps);

		// console.dir(baseMultiDayCtrl);
		mainCtrl = $controller('RiskFactorSensitivitiesController', {
			$scope: $scope,
			$controller: $controller,
			$state: $state,
			// PanelController: panelCtrl,
			ReportServices: RiskFactorSensitivitiesReportServices,
			CacheService: CacheService,
			templates: [ [], [], [] ]
		});

		ctrlDeps = {
			$scope: $scope,
			$filter: $filter,
			$state: $state,
			$timeout: $timeout,
			ErrorService: ErrorService,
			$http: $http,
			uiGridConstants: uiGridConstants,
			ReportServices: RiskFactorSensitivitiesReportServices,
			CacheService: CacheService,
			usSpinnerService: usSpinnerService,
			$controller: $controller,
			toastr: toastr,
			$window: $window,
			VolckerDesks: VolckerDesks
		};

		ctrl = $controller('RiskFactorSensitivitiesMultiDayController', ctrlDeps);

	}));

	beforeEach(function () {
		// set up fake data
		var selectedDesk = {
			sVolckerDeskName : 'hello desk'
		};

		$scope.desk = {
			selected : {
				VolckerDeskID : 123456
			}
		};

		ctrlDeps.ReportServices.multiDayStartDate = '01/01/2015';
		ctrlDeps.ReportServices.multiDayEndDate = '12/31/2015';
		ctrlDeps.CacheService.selectedDesk = selectedDesk;
	});

	it('should be defined', function () {
		expect(ctrl).toBeDefined();
		// console.dir(ctrl);
	});

	// describe('subGridScope', function() {
	// 	it('should be defined', function () {
	// 		expect($scope.subGridScope).toBeDefined();
	// 	});
	// });

	describe('gridOptions', function () {

		it('should be defined', function () {
			expect($scope.gridOptions).toBeDefined();
		});

		it('should have columnDefs', function () {
			expect($scope.gridOptions.columnDefs).toBeDefined();
		});

		it('should have virtualizationThreshold', function () {
			expect($scope.gridOptions.virtualizationThreshold).toBeDefined();
		});

		it('should have expandableRowTemplate', function () {
			expect($scope.gridOptions.expandableRowTemplate).toEqual('risk-factor-sensitivities/rowTemplate.html');
		});

	});

	describe('transitionToCognos', function () {
		it('should be defined', function () {
			expect($scope.transitionToCognos).toBeDefined();
		});

		it('should open cognos report in a new window', function () {
			spyOn( ctrlDeps.$window, 'open' ).and.callFake( function() {
				return true;
			} );

			$scope.gridOptions.data = [{},{},{}];
			$scope.gridApi = {
				core : {
					getVisibleRows: function(grid) {
						return [{},{},{}];
					}
				}
			};

			$scope.transitionToCognos();
			expect(ctrlDeps.$window.open).toHaveBeenCalled();
		});

		it('should alert user there is zero report', function () {
			spyOn(ctrlDeps.$window, 'alert').and.callFake(function() {
				return true;
			});

			$scope.gridOptions.data = [];
			$scope.gridApi = {
				core : {
					getVisibleRows: function(grid) {
						return [{},{},{}];
					}
				}
			};

			$scope.transitionToCognos();
			expect(ctrlDeps.$window.alert).toHaveBeenCalled();
		});

		it('should alert user there is no report being shown', function () {
			spyOn(ctrlDeps.$window, 'alert').and.callFake(function() {
				return true;
			});

			$scope.gridOptions.data = [{report: 1},{report:2},{report:3}];
			$scope.gridApi = {
				core : {
					getVisibleRows: function(grid) {
						return [];
					}
				}
			};

			$scope.transitionToCognos();
			expect(ctrlDeps.$window.alert).toHaveBeenCalled();
		});
	});

	describe('exportToExcel', function () {
		it('should be defined', function () {
			expect($scope.exportToExcel).toBeDefined();
		});

		it('should open excel file in a new window', function () {

			$scope.gridApi = {
				core : {
					getVisibleRows: function(grid) {
						return [{},{},{}];
					}
				}
			};

			spyOn( ctrlDeps.$window, 'open' ).and.callFake( function() {
				return true;
			} );

			$scope.exportToExcel();
			expect(ctrlDeps.$window.open).toHaveBeenCalled();
		});

		it('should alert if no report is being displayed', function () {

			$scope.gridApi = {
				core : {
					getVisibleRows: function(grid) {
						return [];
					}
				}
			};

			spyOn( ctrlDeps.$window, 'alert' ).and.callFake( function() {
				return true;
			} );

			$scope.exportToExcel();
			expect(ctrlDeps.$window.alert).toHaveBeenCalled();
		});
	});

	// describe('reviewReport', function () {
	// 	// var $httpBackend;
	// 	beforeEach(inject(function($injector) {
	// 		// define fake function
	// 		$scope.gridApi = {
	// 			selection : {
	// 				getSelectedRows: function () {
	// 					return [{report:1}];
	// 				}
	// 			}
	// 		};
			
	// 		// $httpBackend = $injector.get('$httpBackend');
	// 	}));

	// 	it('should be defined', function () {
	// 		expect($scope.reviewReport).toBeDefined();
	// 	});

	// 	it('should set isReviewing to true', function () {
	// 		$scope.reviewReport();
	// 		expect($scope.isReviewing).toBe(true);
	// 	});

	// 	function fakePromise () {
	// 		return true;
	// 		// return function () {
	// 		// 	return {
	// 		// 		then: function (data, success) {

	// 		// 		}
	// 		// 	}
	// 		// }
	// 	}

	// 	it('TODO: should make a $http POST request', function () {
	// 		// spyOn(ctrlDeps.$http, 'post')//.and.callFake(fakePromise);
	// 		// $httpBackend.flush();
	// 		// $httpBackend.expectPOST('../volckeroverrideswebapi/Review');
	// 		// $scope.reviewReport();
	// 		// $httpBackend.flush();
	// 	});
	// });

	describe('getReports', function() {
		it('should be defined', function() {
			expect($scope.getReports).toBeDefined();
		});

		it('should call ReportServices MultiDayReport', function () {
			spyOn(ctrlDeps.ReportServices.MultiDayReport, 'query').and.callFake(function() {
				return [];
			});
			$scope.getReports();
			expect(ctrlDeps.ReportServices.MultiDayReport.query).toHaveBeenCalled();
		});

		it('should set noReportData flag to false if no data comes back', function () {
			// $scope.getReports();
			// expect($scope.noReportData).toBe(true);
		});
	});

	describe('search', function () {
		it('should be defined', function () {
			expect($scope.search).toBeDefined();
		});

		it('should alert user if date range is not selected', function () {
			$scope.dateOptions.startDate = undefined;
			$scope.dateOptions.endDate = undefined;
			spyOn(ctrlDeps.$window, 'alert');
			$scope.search();
			expect(ctrlDeps.$window.alert).toHaveBeenCalled();
		});

		it('should do nothing if date range, breach status and desk are not modified', function() {
			$scope.dateOptions.startDate = '01/01/2015';
			$scope.dateOptions.endDate = '12/01/2015';
			$scope.desk = {
				selected: {'sVolckerDeskName': 'hello'}
			};
			$scope.breachOnly = { val: true};

			ctrlDeps.ReportServices.multiDayStartDate = '01/01/2015';
			ctrlDeps.ReportServices.multiDayEndDate = '12/01/2015';
			ctrlDeps.CacheService.selectedDesk = $scope.desk.selected;
			ctrlDeps.ReportServices.breachOnly = $scope.breachOnly.val;
			$scope.search();
			expect($scope.isSearching).toBeFalsy();
		});
	});

});