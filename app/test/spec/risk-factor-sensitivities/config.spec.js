'use strict';

describe('Panel 2 (Risk Factor Sensitivities) - config', function () {
    var $state, route;

    beforeEach(module('ui.router', 'app.risk-factor-sensitivities'));

    beforeEach(inject(function (_$state_) {
        $state = _$state_;
    }));

    describe('Base Route: /risk-factor-sensitivities', function () {
        beforeEach(function () {
            route = $state.get('risk-factor-sensitivities');
            // console.log(route);
        });
        
        it('should have route: /risk-factor-sensitivities', function () {
            expect(route.url).toBe('/risk-factor-sensitivities');
        });

        it('should use template: common/templates/panelHeader.html', function () {
            expect(route.templateUrl).toBe('common/templates/panelHeader.html');
        });

        it('should use controller: RiskFactorSensitivitiesController', function () {
            expect(route.controller).toBe('RiskFactorSensitivitiesController');
        });
    });

    describe('MultiDay View Nested Route: /risk-factor-sensitivities/multi-day', function () {
        beforeEach(function () {
            route = $state.get('risk-factor-sensitivities.multi-day');
            // console.log(route);
        });
        
        it('should have sub route: /multi-day', function () {
            expect(route.url).toBe('/multi-day');
        });

        it('should use template: risk-factor-sensitivities/report-multi-day.html', function () {
            expect(route.templateUrl).toBe('risk-factor-sensitivities/report-multi-day.html');
        });

        it('should use nested controller: RiskFactorSensitivitiesMultiDayController', function () {
            expect(route.controller).toBe('RiskFactorSensitivitiesMultiDayController');
        });
    });

    describe('Day View Nested Route: /risk-factor-sensitivities/day', function () {
        beforeEach(function () {
            route = $state.get('risk-factor-sensitivities.day');
            // console.log(route);
        });
        
        it('should have sub route: /day', function () {
            expect(route.url).toBe('/day');
        });

        it('should use template: risk-factor-sensitivities/report-day.html', function () {
            expect(route.templateUrl).toBe('risk-factor-sensitivities/report-day.html');
        });

        it('should use nested controller: RiskFactorSensitivitiesDayController', function () {
            expect(route.controller).toBe('RiskFactorSensitivitiesDayController');
        });
    });

    describe('Detail View Nested Route: /risk-factor-sensitivities/detail', function () {
        beforeEach(function () {
            route = $state.get('risk-factor-sensitivities.detail');
            // console.log(route);
        });
        
        it('should have sub route: /detail/:desk/:deskID/:date', function () {
            expect(route.url).toBe('/detail/:desk/:deskID/:date');
        });

        it('should use template: risk-factor-sensitivities/report-detail.html', function () {
            expect(route.templateUrl).toBe('risk-factor-sensitivities/report-detail.html');
        });

        it('should use nested controller: RiskFactorSensitivitiesDetailController', function () {
            expect(route.controller).toBe('RiskFactorSensitivitiesDetailController');
        });
    });
});
