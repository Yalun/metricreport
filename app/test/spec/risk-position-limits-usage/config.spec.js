'use strict';

describe('Panel 1 (Risk Position Limits Usage) - config', function () {
    var $state, route;

    beforeEach(module('ui.router', 'app.risk-position-limits-usage'));

    beforeEach(inject(function (_$state_) {
        $state = _$state_;
    }));

    describe('Base Route: /risk-position-limits-usage', function () {
        beforeEach(function () {
            route = $state.get('risk-position-limits-usage');
            // console.log(route);
        });
        
        it('should have route: /risk-position-limits-usage', function () {
            expect(route.url).toBe('/risk-position-limits-usage');
        });

        it('should use template: common/templates/panelHeaderWithChart.html', function () {
            expect(route.templateUrl).toBe('common/templates/panelHeaderWithChart.html');
        });

        it('should use controller: riskPositionLimitsUsageController', function () {
            expect(route.controller).toBe('riskPositionLimitsUsageController');
        });
    });

    describe('MultiDay View Nested Route: /risk-position-limits-usage/multi-day', function () {
        beforeEach(function () {
            route = $state.get('risk-position-limits-usage.multi-day');
            // console.log(route);
        });
        
        it('should have sub route: /multi-day', function () {
            expect(route.url).toBe('/multi-day');
        });

        it('should use template: risk-position-limits-usage/report-multi-day.html', function () {
            expect(route.templateUrl).toBe('risk-position-limits-usage/report-multi-day.html');
        });

        it('should use nested controller: riskPositionLimitsUsageMultiDayController', function () {
            expect(route.controller).toBe('riskPositionLimitsUsageMultiDayController');
        });
    });

    describe('Day View Nested Route: /risk-position-limits-usage/day', function () {
        beforeEach(function () {
            route = $state.get('risk-position-limits-usage.day');
            // console.log(route);
        });
        
        it('should have sub route: /day', function () {
            expect(route.url).toBe('/day');
        });

        it('should use template: risk-position-limits-usage/report-day.html', function () {
            expect(route.templateUrl).toBe('risk-position-limits-usage/report-day.html');
        });

        it('should use nested controller: riskPositionLimitsUsageDayController', function () {
            expect(route.controller).toBe('riskPositionLimitsUsageDayController');
        });
    });

    describe('Detail View Nested Route: /risk-position-limits-usage/detail', function () {
        beforeEach(function () {
            route = $state.get('risk-position-limits-usage.detail');
            // console.log(route);
        });
        
        it('should have sub route: /detail/:desk/:deskID/:date', function () {
            expect(route.url).toBe('/detail/:desk/:deskID/:date');
        });

        it('should use template: risk-position-limits-usage/report-detail.html', function () {
            expect(route.templateUrl).toBe('risk-position-limits-usage/report-detail.html');
        });

        it('should use nested controller: riskPositionLimitsUsageDetailController', function () {
            expect(route.controller).toBe('riskPositionLimitsUsageDetailController');
        });
    });
});
