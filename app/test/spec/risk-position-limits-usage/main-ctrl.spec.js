'use strict';

describe('Panel 1 (Risk Position Limits Usage) - main', function () {
	var ctrl,
		ctrlDeps,
		panelCtrl,
		panelCtrlDeps,
		$scope,
		moduleName;

	beforeEach(module('app.risk-position-limits-usage', 'app.common', 'ui.router', 'app'));

	beforeEach(inject(function ($controller, $rootScope, $state, $window, CacheService, riskPositionLimitsUsageReportServices, $filter, ErrorService, CommonHelperService) {

		moduleName = 'risk-position-limits-usage';

		$scope = $rootScope.$new();
		// console.log($scope);
		panelCtrlDeps = {
			$scope: $scope,
			$state: $state,
			$filter: $filter,
			// ReportServices: riskPositionLimitsUsageReportServices,
			ErrorService: ErrorService,
			CommonHelperService: CommonHelperService,
			moduleName: moduleName
		};
		// base panel controller which contains common shared functionality for all */main.js
		panelCtrl = $controller('PanelController', panelCtrlDeps );

		ctrlDeps = {
			$scope: $scope,
			$controller: $controller,
			$state: $state,
			$window: $window,
			// PanelController: panelCtrl,
			ReportServices: riskPositionLimitsUsageReportServices,
			CacheService: CacheService
		};

		ctrl = $controller('riskPositionLimitsUsageController', ctrlDeps );
	}));

	it('should be defined', function () {
		expect(ctrl).toBeDefined();
		// console.dir(ctrl);
	});

	// describe('productStatus', function () {
	// 	it('should accept the type of product as 1st argument and boolean flag for styling as an optional 2nd argument, and returns a function', function () {
	// 		expect(typeof $scope.productStatus('primary')).toBe('function');
	// 		expect(typeof $scope.productStatus('hedge', true)).toBe('function');
	// 		expect(typeof $scope.productStatus('total', false)).toBe('function');
	// 		expect(typeof $scope.productStatus('hellowhatever1293')).toBe('function');

	// 	});
	// 	it ('should return a function which used as UI Grid\'s cellClass function', function () {
	// 		var grid = {};
	// 		var row = {
	// 			entity: {
	// 				'oPrimaryAmountsDTO' : [],
	// 				'oHedgeAmountsDTO': [],
	// 				'oTotalAmountsDTO': []
	// 			}
	// 		};
	// 		var col = {
	// 			field: 'oPrimaryAmountsDTO.dUsage'
	// 		};

	// 		var fn = $scope.productStatus('gibberish', true); //for the type that does not exist
	// 		expect(fn(grid, row, col)).toEqual('text-right editable');

	// 		var fn2 = $scope.productStatus('primary');
	// 		expect(fn2(grid, row, col)).toEqual('text-right primary-col');

	// 		var fn3 = $scope.productStatus('hedge', true);
	// 		expect(fn3(grid, row, col)).toEqual('text-right hedge-col editable');

	// 		var fn4 = $scope.productStatus('total', false);
	// 		expect(fn4(grid, row)).toEqual('text-right total-col');
	// 	});
	// });

	describe('routeToCognosChart', function () {
		it('should have routeToCognosChart', function () {
			expect($scope.routeToCognosChart).toBeDefined();
		});

		it('should open a new window', function () {
			spyOn(ctrlDeps.$window, 'open');

			$scope.routeToCognosChart();
			expect(ctrlDeps.$window.open).toHaveBeenCalled();
		});
	});


	describe('when State changes from one panel to another', function () {
		var toState = {name: 'some-other-panel'};
		var toParams = {};
		var fromState = {name: 'risk-position-limits-usage'};
		var fromParams = {};
		
		it('should clear the cache stored in CacheService', function () {
			spyOn(ctrlDeps.ReportServices, 'clearCache').and.callFake(function () {
				return false;
			});

			$scope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
			expect(ctrlDeps.ReportServices.clearCache).toHaveBeenCalled();
		});

	});

	describe('when State changes within same module (AKA panel)', function () {
		it('from MultiDay to Day, should not clear the cache stored in CacheService', function () {
			var toState = {name: 'risk-position-limits-usage.day'};
			var toParams = {};
			var fromState = {name: 'risk-position-limits-usage.multi-day'};
			var fromParams = {};

			spyOn(ctrlDeps.ReportServices, 'clearCache').and.callFake(function () {
				return false;
			});
			$scope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
			expect(ctrlDeps.ReportServices.clearCache).not.toHaveBeenCalled();
		});

		it('from MultiDay to Detail, should not clear the cache stored in CacheService', function () {
			var toState = {name: 'risk-position-limits-usage.detail'};
			var toParams = {};
			var fromState = {name: 'risk-position-limits-usage.multi-day'};
			var fromParams = {};

			spyOn(ctrlDeps.ReportServices, 'clearCache').and.callFake(function () {
				return false;
			});
			$scope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
			expect(ctrlDeps.ReportServices.clearCache).not.toHaveBeenCalled();
		});

		it('from Day to MultiDay, should not clear the cache stored in CacheService', function () {
			var toState = {name: 'risk-position-limits-usage.multi-day'};
			var toParams = {};
			var fromState = {name: 'risk-position-limits-usage.day'};
			var fromParams = {};

			spyOn(ctrlDeps.ReportServices, 'clearCache').and.callFake(function () {
				return false;
			});
			$scope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
			expect(ctrlDeps.ReportServices.clearCache).not.toHaveBeenCalled();
		});

		it('from Day to Detail, should not clear the cache stored in CacheService', function () {
			var toState = {name: 'risk-position-limits-usage.detail'};
			var toParams = {};
			var fromState = {name: 'risk-position-limits-usage.day'};
			var fromParams = {};

			spyOn(ctrlDeps.ReportServices, 'clearCache').and.callFake(function () {
				return false;
			});
			$scope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
			expect(ctrlDeps.ReportServices.clearCache).not.toHaveBeenCalled();
		});

		it('from Detail to MultiDay, should not clear the cache stored in CacheService', function () {
			var toState = {name: 'risk-position-limits-usage.multi-day'};
			var toParams = {};
			var fromState = {name: 'risk-position-limits-usage.detail'};
			var fromParams = {};

			spyOn(ctrlDeps.ReportServices, 'clearCache').and.callFake(function () {
				return false;
			});
			$scope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
			expect(ctrlDeps.ReportServices.clearCache).not.toHaveBeenCalled();
		});

		it('from Detail to Day, should not clear the cache stored in CacheService', function () {
			var toState = {name: 'risk-position-limits-usage.day'};
			var toParams = {};
			var fromState = {name: 'risk-position-limits-usage.detail'};
			var fromParams = {};

			spyOn(ctrlDeps.ReportServices, 'clearCache').and.callFake(function () {
				return false;
			});
			$scope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
			expect(ctrlDeps.ReportServices.clearCache).not.toHaveBeenCalled();
		});

	});
	
	// things that are inherit from PanelController... same for main-ctrl of all modules
	describe('scope variables inherit from PanelController', function () {
		it('should have dateFormat', function () {
			expect($scope.dateFormat).toBeDefined();
		});

		it('should have queryFormat', function () {
			expect($scope.queryFormat).toBeDefined();
		});

		it('should have today', function () {
			expect($scope.today).toBeDefined();
		});

		it('should have previousDate', function () {
			expect($scope.previousDate).toBeDefined();
		});

		it('should have quarterStartDate', function () {
			expect($scope.quarterStartDate).toBeDefined();
		});

		it('should have dateRangeEndDate', function () {
			expect($scope.dateRangeEndDate).toBeDefined();
		});

		it('should have dateSortingFn', function () {
			expect($scope.dateSortingFn).toBeDefined();
		});

		it('should have convertToDateFormat', function () {
			expect($scope.convertToDateFormat).toBeDefined();
		});

		it('should have documentModal', function () {
			expect($scope.documentModal).toBeDefined();
		});

		it('should have commentModal', function () {
			expect($scope.commentModal).toBeDefined();
		});

		it('should have seeDetail', function () {
			expect($scope.seeDetail).toBeDefined();
		});

		it('should have activeTab', function () {
			expect($scope.activeTab).toBeDefined();
		});

		it('should have routeToMultiDay (private helper function)', function () {
			expect(panelCtrl.routeToMultiDay).toBeDefined();
		});

	});
});