'use strict';

describe('Panel 1 (Risk Position Limits Usage) - detail', function () {
	var ctrl,
		mainCtrl,
		ctrlDeps,
		baseDetailCtrl,
		baseDetailCtrlDeps,
		$scope;

	beforeEach(module('app.risk-position-limits-usage', 'app.common', 'ui.router', 'app'));

	beforeEach(inject(function ($controller, $rootScope, $modal, $state, $filter, $timeout, ErrorService, $http, uiGridConstants, $location, $anchorScroll, riskPositionLimitsUsageReportServices, CacheService, usSpinnerService, CommonHelperService, $window) {

		$scope = $rootScope.$new();

		baseDetailCtrlDeps = {
			$scope: $scope,
			ErrorService: ErrorService,
			CacheService: CacheService,
			ReportServices: riskPositionLimitsUsageReportServices,
			$state: $state,
			$timeout: $timeout,
			$anchorScroll: $anchorScroll,
			CommonHelperService: CommonHelperService
		};

		baseDetailCtrl = $controller('DetailController', baseDetailCtrlDeps);

		mainCtrl = $controller('riskPositionLimitsUsageController', {
			$scope: $scope,
			$state: $state,
			ReportServices: riskPositionLimitsUsageReportServices,
			CacheService: CacheService,
			$controller: $controller,
			$window: $window,
			$modal: $modal,
			$filter: $filter
		});
		// console.dir(baseDetailCtrl);

		ctrlDeps = {
			$scope: $scope,
			$controller: $controller,
			$filter: $filter,
			$http: $http,
			$modal: $modal,
			$timeout: $timeout,
			$location: $location,
			$anchorScroll: $anchorScroll,
			ErrorService: ErrorService,
			uiGridConstants: uiGridConstants,
			ReportServices: riskPositionLimitsUsageReportServices,
			CacheService: CacheService,
			usSpinnerService: usSpinnerService
		};

		ctrl = $controller('riskPositionLimitsUsageDetailController', ctrlDeps);

		$scope.date = '01/01/2015';
		$scope.deskName = 'Agencies/CP';
	}));
	
	it('should be defined', function () {
		expect(ctrl).toBeDefined();
		// console.dir(ctrl);
	});

	describe('showUnauthorizedProductValues', function() {
		it('should be defined', function() {
			expect($scope.showUnauthorizedProductValues).toBeDefined();
		});

		it('should call ReportServices.showUnauthorizedProducts', function () {
			var breachedProduct = [
				{
					dNotional: 123,
					sCalendarDate: '01/01/2015',
					sCusip: 'Aggr_D000_AB',
					sIssueName: 'Aggr_D00_AB',
					sProduct: 'Securities Financing',
					sProductSubType: 'Stock Borrow',
					sProductType: 'Stock Borrow',
					sTradebook: 'A123',
					sVolckerDesk: 'Agencies/CP'
				},
				{},
				{}
			];

			spyOn(ctrlDeps.ReportServices, 'showUnauthorizedProducts').and.callFake(function () {
				return breachedProduct;
			});

			$scope.showUnauthorizedProductValues('Security Borrow');
			expect(ctrlDeps.ReportServices.showUnauthorizedProducts).toHaveBeenCalledWith(
				'Security Borrow',
				$scope.date,
				$scope.date,
				$scope.deskName
			);
		});
	});

	describe('getTradedProducts', function() {
		it('should be defined', function () {
			expect($scope.getTradedProducts).toBeDefined();
		});

		it('should call ReportServices.TradedProducts', function () {
			spyOn(ctrlDeps.ReportServices.TradedProducts, 'get').and.callFake(function() {
				return [];
			});
			$scope.getTradedProducts('01-01-1999', '01-31-1999', 'hello');
			expect(ctrlDeps.ReportServices.TradedProducts.get).toHaveBeenCalled();
		});
	});

	describe('showPosition', function() {
		// TODO : open position level modal
	});

	describe('tradedProducts', function() {
		it('should be defined', function () {
			expect($scope.tradedProducts).toBeDefined();
		});
	});

	describe('reviewReport', function () {
		it('should call ReportServices.review', function () {
			spyOn(ctrlDeps.ReportServices, 'review').and.callFake(function () {
				return {
					data: [
						{
							sReviewerLogin: 'USI//someguy'
						}
					]
				};
			});
			$scope.desk = {
				//
			};

			$scope.reviewReport();
			expect(ctrlDeps.ReportServices.review).toHaveBeenCalled();
		});

	});

	describe('detailGridOptions', function () {

		it('should be defined', function () {
			expect($scope.detailGridOptions).toBeDefined();
		});

		it('should have columnDefs', function () {
			expect($scope.detailGridOptions.columnDefs).toBeDefined();
		});

		it('should have virtualizationThreshold', function () {
			expect($scope.detailGridOptions.virtualizationThreshold).toBeDefined();
		});

		it('should have expandableRowTemplate', function () {
			expect($scope.detailGridOptions.expandableRowTemplate).toEqual('risk-position-limits-usage/rowTemplate.html');
		});

	});

	describe('exportToExcel', function () {
		it('should be defined', function () {
			expect($scope.exportToExcel).toBeDefined();
		});

		it('should call ReportServices.exportToCsv', function () {
			spyOn(ctrlDeps.ReportServices, 'exportToCsv').and.callFake(function () {
				return false;
			});

			$scope.exportToExcel();
			expect(ctrlDeps.ReportServices.exportToCsv).toHaveBeenCalled();
		});
	});

	describe('getDetailReport', function() {

		it('should be defined', function() {
			expect($scope.getDetailReport).toBeDefined();
		});

		it('should call ReportServices DetailReport', function () {
			spyOn(ctrlDeps.ReportServices.DetailReport, 'query').and.callFake(function() {
				return [];
			});
			$scope.getDetailReport();
			expect(ctrlDeps.ReportServices.DetailReport.query).toHaveBeenCalled();
		});

		it('should set noReportData flag to false if no data comes back', function () {
			// $scope.getDetailReport();
			// expect($scope.noReportData).toBe(true);
		});

	});

	describe('disableReview', function() {
		it('should disable review button when it is clicked or no report has been generated', function () {
			$scope.isReviewing = true;
			expect($scope.disableReview()).toBe(true);
			$scope.desk = null;
			expect($scope.disableReview()).toBe(true);
		});
	});

	describe('subGridColDefs', function() {
		it('should be defined', function () {
			expect($scope.subGridColDefs).toBeDefined();
		});
	});

	//----------------inherit from common/controllers/detail-ctrl.js-----------
	describe('scope variables inherit from base DetailController', function () {
		it('should have $parent.activeTab', function () {
			expect($scope.$parent.activeTab).toBeDefined();
		});

		it('deskName should be defined', function () {
			expect($scope.deskName).toBeDefined();
		});

		it('date should be defined', function () {
			expect($scope.date).toBeDefined();
		});

		it('deskID should be defined', function () {
			expect($scope.deskID).toBeDefined();
		});

		it('goBack should be defined', function () {
			expect($scope.goBack).toBeDefined();
		});

		it('showAuditHistory should be defined', function () {
			expect($scope.showAuditHistory).toBeDefined();
		});

	});
});