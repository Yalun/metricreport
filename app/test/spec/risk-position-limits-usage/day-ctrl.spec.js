'use strict';

describe('Panel 1 (Risk Position Limits Usage) - day', function () {
	var ctrl,
		ctrlDeps,
		baseDayCtrl,
		baseDayCtrlDeps,
		httpBackend,
		mockData,
		$scope;

	beforeEach(module('app.risk-position-limits-usage', 'app.common', 'ui.router', 'app'));

	beforeEach(inject(function ($controller, $rootScope, $http, $state, $filter, $window, $timeout, ErrorService, uiGridConstants, riskPositionLimitsUsageReportServices, CacheService, usSpinnerService, CommonHelperService, $httpBackend) {

		$scope = $rootScope.$new();

		baseDayCtrlDeps = {
			$scope: $scope,
			$filter: $filter,
			ErrorService: ErrorService,
			CacheService: CacheService,
			ReportServices: riskPositionLimitsUsageReportServices,
			$state: $state,
			$timeout: $timeout,
			CommonHelperService: CommonHelperService
		};

		baseDayCtrl = $controller('DayController', baseDayCtrlDeps);

		// console.dir(baseDayCtrl);

		ctrlDeps = {
			$scope: $scope,
			$controller: $controller,
			$filter: $filter,
			$window: $window,
			$http: $http,
			$timeout: $timeout,
			ErrorService: ErrorService,
			uiGridConstants: uiGridConstants,
			ReportServices: riskPositionLimitsUsageReportServices,
			CacheService: CacheService,
			usSpinnerService: usSpinnerService
		};

		ctrl = $controller('riskPositionLimitsUsageDayController', ctrlDeps);

		ctrlDeps.ReportServices.dayReportDate = '01/01/2015';
		$scope.deskName = 'Agencies/CP';

		httpBackend = $httpBackend;
		httpBackend.whenGET(/\.html$/).respond(''); // avoid getting unexpected request when getting templates
		// use regex to match the URL and any parameter after it
		httpBackend.whenGET(/^\..\/volckeroverrideswebapi\/LimitUsage\?.*/).respond(200, mockData); //when env is prod
		httpBackend.whenGET(/^data\/panel\-1\/day\-sample\.json\?.*/).respond(200, mockData); //when env is local
	}));

	it('should be defined', function () {
		expect(ctrl).toBeDefined();
		// console.dir(ctrl);
	});

	describe('tradedProducts', function() {
		it('should be defined', function () {
			expect($scope.tradedProducts).toBeDefined();
		});
	});

	describe('gridOptions', function () {

		it('should be defined', function () {
			expect($scope.gridOptions).toBeDefined();
		});

		it('should have columnDefs', function () {
			expect($scope.gridOptions.columnDefs).toBeDefined();
		});

		it('should have virtualizationThreshold', function () {
			expect($scope.gridOptions.virtualizationThreshold).toBeDefined();
		});

		it('should have expandableRowTemplate', function () {
			expect($scope.gridOptions.expandableRowTemplate).toEqual('risk-position-limits-usage/rowTemplate.html');
		});

	});

	describe('exportToExcel', function () {
		it('should be defined', function () {
			expect($scope.exportToExcel).toBeDefined();
		});

		it('should open excel file in a new window', function () {

			$scope.gridApi = {
				core : {
					getVisibleRows: function(grid) {
						return [{},{},{}];
					}
				}
			};

			spyOn( ctrlDeps.$window, 'open' ).and.callFake( function() {
				return true;
			} );

			$scope.exportToExcel();
			expect(ctrlDeps.$window.open).toHaveBeenCalled();
		});

		it('should alert if no report is being displayed', function () {

			$scope.gridApi = {
				core : {
					getVisibleRows: function(grid) {
						return [];
					}
				}
			};

			spyOn( ctrlDeps.$window, 'alert' ).and.callFake( function() {
				return true;
			} );

			$scope.exportToExcel();
			expect(ctrlDeps.$window.alert).toHaveBeenCalled();
		});
	});

	describe('reviewReport', function () {
		it('should be defined', function () {
			expect($scope.reviewReport).toBeDefined();
		});

		it('should call ReportServices.reviewMultiDayAndDayView', function () {
			spyOn(ctrlDeps.ReportServices, 'reviewMultiDayAndDayView').and.callFake(function () {
				return false;
			});

			$scope.reviewReport();
			expect(ctrlDeps.ReportServices.reviewMultiDayAndDayView).toHaveBeenCalledWith($scope);
		});
	});

	describe('getReports', function() {
		var data;

		it('should be defined', function() {
			expect($scope.getReports).toBeDefined();
		});

		it('should query day report', function () {
			spyOn(ctrlDeps.ReportServices.DayReport, 'query').and.callFake(function() {
				data = [
					{
					LimitUsageDTOData: [
						{
							oBookDetailsDTOData: null,
							oPrimaryAmountsDTO: {
							sProductUsage: "Primary",
							dUsage: 411858621.48000014,
							dLimUtil: null,
							dLimitAmount: null,
							dLimitMin: null,
							dLimitMax: null,
							dLimitBreach: 80,
							sLimitDisplay: "/",
							bBreachError: false,
							bBreachWarning: false,
							bIsOverridden: false
						},
						oHedgeAmountsDTO: {
							sProductUsage: "Hedge",
							dUsage: 242283277.44,
							dLimUtil: null,
							dLimitAmount: null,
							dLimitMin: null,
							dLimitMax: null,
							dLimitBreach: 80,
							sLimitDisplay: "/",
							bBreachError: false,
							bBreachWarning: false,
							bIsOverridden: false
						},
						oTotalAmountsDTO: {
							sProductUsage: "Total",
							dUsage: 654141898.9200001,
							dLimUtil: 0.43609459928000005,
							dLimitAmount: 1500000000,
							dLimitMin: null,
							dLimitMax: 1500000000,
							dLimitBreach: 80,
							sLimitDisplay: " / 1,500,000,000",
							bBreachError: false,
							bBreachWarning: false,
							bIsOverridden: false
						},
						sCalendarDate: "2015-07-01",
							sDivision: "Fixed Income",
							sVolckerDesk: "Agencies/CP",
							sTradebook: null,
							sMeasureType: "Gross MV",
							dMeasureId: 3,
							sUnit: "$",
							sSecurityId: null,
							sSecurityDescription: null,
							sProductHierarchy1: null,
							sProductHierarchy2: null,
							sProductHierarchy3: null
						},
						{},
						{},
						{},
						{},
						{},
						{},
						{},
						{},
						{}
						],
						bIsReviewClicked: false,
						ReviewID: 1141,
						sPanel: "1",
						VolckerDeskID: 1,
						sVolckerDeskName: "Agencies/CP",
						dtReview: "7/1/2015",
						ReviewStatusID: 1,
						sReviewerLogin: "USI\yalunzhu",
						dtTimeStamp: "7/16/2015",
						sDescription: "Reviewed",
						sComment: "OK again, OK",
						ReviewStatus: "",
						BreachStatus: "Breached"
					},
					{},
					{}
				];
			});
			// TODO
			$scope.getReports();
			expect(ctrlDeps.ReportServices.DayReport.query).toHaveBeenCalled();
		});

		it('getReports should set noReportData flag to false if no data comes is feteched', function () {
			mockData = [];
			$scope.getReports();
			httpBackend.flush();
			expect($scope.noReportData).toBe(true);
		});
	});

	describe('searchReports', function () {
		it('should be defined', function () {
			expect($scope.searchReports).toBeDefined();
		});
	});

	describe('showUnauthorizedProductValues', function () {

		it('should be defined' ,function () {
			expect($scope.showUnauthorizedProductValues).toBeDefined();
		});

		it('should call ReportServices.showUnauthorizedProducts', function () {

			var breachedProduct = [
				{
					dNotional: 123,
					sCalendarDate: '01/01/2015',
					sCusip: 'Aggr_D000_AB',
					sIssueName: 'Aggr_D00_AB',
					sProduct: 'Securities Financing',
					sProductSubType: 'Stock Borrow',
					sProductType: 'Stock Borrow',
					sTradebook: 'A123',
					sVolckerDesk: 'Agencies/CP'
				},
				{},
				{}
			];

			spyOn(ctrlDeps.ReportServices, 'showUnauthorizedProducts').and.callFake(function () {
				return breachedProduct;
			});

			$scope.showUnauthorizedProductValues('Security Borrow');
			expect(ctrlDeps.ReportServices.showUnauthorizedProducts).toHaveBeenCalledWith(
				'Security Borrow',
				ctrlDeps.ReportServices.dayReportDate,
				ctrlDeps.ReportServices.dayReportDate,
				$scope.deskName
			);
		});
	});

	//------ inherit from common/controllers/day-ctrl.js -------------------------
	describe('scope variables inherit from baseDayCtrl', function () {
		it('should have $parent.activeTab', function () {
			expect($scope.$parent.activeTab).toBeDefined();
		});

		it('should have breachOnly', function () {
			expect($scope.breachOnly).toBeDefined();
		});

		it('should have formatSelectedDate', function () {
			expect($scope.formatSelectedDate).toBeDefined();
		});

		it('should have disableReview', function () {
			expect($scope.disableReview).toBeDefined();
		});

		it('should have dateOptions', function () {
			expect($scope.dateOptions).toBeDefined();
		});

		it('should have openCalendar', function () {
			expect($scope.openCalendar).toBeDefined();
		});

		it('should have transitionToCognos', function () {
			expect($scope.transitionToCognos).toBeDefined();
		});

		describe('transitionToCognos', function () {
			it('should be defined', function () {
				expect($scope.transitionToCognos).toBeDefined();
			});

			it('should open cognos report in a new window', function () {
				spyOn( ctrlDeps.$window, 'open' ).and.callFake( function() {
					return true;
				} );

				$scope.gridOptions.data = [{},{},{}];
				$scope.gridApi = {
					core : {
						getVisibleRows: function(grid) {
							return [{},{},{}];
						}
					}
				};

				$scope.transitionToCognos();
				expect(ctrlDeps.$window.open).toHaveBeenCalled();
			});

			it('should alert user there is no report being shown', function () {
				spyOn(ctrlDeps.$window, 'alert').and.callFake(function() {
					return true;
				});

				$scope.gridOptions.data = [{report: 1},{report:2},{report:3}];
				$scope.gridApi = {
					core : {
						getVisibleRows: function(grid) {
							return [];
						}
					}
				};

				$scope.transitionToCognos();
				expect(ctrlDeps.$window.alert).toHaveBeenCalled();
			});

		});
		
	});
	
	describe('private helper functions from baseDayCtrl', function () {
		it('should have initSearch', function () {
			expect(baseDayCtrl.initSearch).toBeDefined();
		});

		it('should have cacheReport', function () {
			expect(baseDayCtrl.cacheReport).toBeDefined();
		});

		it('should have onReportError', function () {
			expect(baseDayCtrl.onReportError).toBeDefined();
		});

		it('should have searchWrapper', function () {
			expect(baseDayCtrl.searchWrapper).toBeDefined();
		});

		it('should have exportToExcel', function () {
			expect(baseDayCtrl.exportToExcel).toBeDefined();
		});

		it('should have loadReportFromCache', function () {
			expect(baseDayCtrl.loadReportFromCache).toBeDefined();
		});
	})
});
