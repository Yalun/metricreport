'use strict';

describe('Panel 1 (Risk Position Limits Usage) - multi-day', function () {
	var ctrl,
		ctrlDeps,
		baseMultiDayCtrl,
		baseMultiDayCtrlDeps,
		VolckerDesks,
		$scope,
		mainCtrl,
		$q,
		$rootScope,
		mockData,
		httpBackend;

	beforeEach(module('app.risk-position-limits-usage', 'app.common', 'ui.router', 'app'));

	beforeEach(inject(function ($controller, $rootScope, $q, $http, $filter, $modal, $window, $timeout, ErrorService, uiGridConstants, riskPositionLimitsUsageReportServices, CacheService, usSpinnerService, toastr, $state, $httpBackend) {

		$scope = $rootScope.$new();

		VolckerDesks = [
			{sVolckerDeskName: 'Desk 1', VolckerDeskID: 1},
			{sVolckerDeskName: 'Desk 2', VolckerDeskID: 2},
			{sVolckerDeskName: 'Desk 3', VolckerDeskID: 3}
		];

		baseMultiDayCtrlDeps = {
			$scope: $scope,
			$filter: $filter,
			ErrorService: ErrorService,
			CacheService: CacheService,
			ReportServices: riskPositionLimitsUsageReportServices,
			VolckerDesks: VolckerDesks
		};

		//base multi day controller (all multi day views extend upon this controller)
		baseMultiDayCtrl = $controller('MultiDayController', baseMultiDayCtrlDeps);

		//parent controller of multiday-ctrl (main controller of module, parent controller of multi day view, day view, and detail view)
		mainCtrl = $controller('riskPositionLimitsUsageController', {
			$scope: $scope,
			$controller: $controller,
			$state: $state,
			// PanelController: panelCtrl,
			ReportServices: riskPositionLimitsUsageReportServices,
			CacheService: CacheService,
			$window: $window,
			$modal: $modal,
			$filter: $filter
		});

		ctrlDeps = {
			$scope: $scope,
			$rootScope: $rootScope,
			$controller: $controller,
			$q: $q,
			$filter: $filter,
			$modal: $modal,
			$window: $window,
			$http: $http,
			$timeout: $timeout,
			ErrorService: ErrorService,
			uiGridConstants: uiGridConstants,
			ReportServices: riskPositionLimitsUsageReportServices,
			CacheService: CacheService,
			usSpinnerService: usSpinnerService,
			VolckerDesks: VolckerDesks,
			toastr: toastr
		};

		ctrl = $controller('riskPositionLimitsUsageMultiDayController', ctrlDeps);

		var selectedDesk = {
			sVolckerDeskName : 'hello desk'
		};

		$scope.desk = {
			selected : {
				sVolckerDeskName : 'hello desk',
				VolckerDeskID : 123456
			}
		};

		ctrlDeps.ReportServices.multiDayStartDate = '01/01/2015';
		ctrlDeps.ReportServices.multiDayEndDate = '12/31/2015';
		ctrlDeps.CacheService.selectedDesk = $scope.desk.selected;

		httpBackend = $httpBackend;
		httpBackend.whenGET(/\.html$/).respond(''); // avoid getting unexpected request when getting templates

		// use regex to match the URL and any parameter after it
		httpBackend.whenGET(/^\..\/volckeroverrideswebapi\/LimitUsage\?.*/).respond(200, mockData); //when env is prod
		httpBackend.whenGET(/^data\/panel\-1\/multi\-day\-sample\.json\?.*/).respond(200, mockData); //when env is local
		httpBackend.whenGET(/^\/volckeroverrideswebapi\/LimitUsageExcelExport\?.*/).respond(404, ''); // dont let Export to excel to respond anything
		httpBackend.whenGET(/^\/volckeroverrideswebapi\/LimitUsageExcelExport\?.*/).respond(''); // dont let Export to excel to respond anything
		httpBackend.when('GET', '../volckeroverrideswebapi/VolckerDesk').respond(200, VolckerDesks);
		httpBackend.when('GET', 'data/desks.json').respond(200, VolckerDesks);

	}));

	it('should be defined', function () {
		expect(ctrl).toBeDefined();
	});

	describe('subGridScope', function() {
		it('should be defined', function () {
			expect($scope.subGridScope).toBeDefined();
		});
	});

	describe('tradedProducts', function() {
		it('should be defined', function() {
			expect($scope.tradedProducts).toBeDefined();
		});
	});

	describe('showUnauthorizedProductValues', function () {
		it('should be defined' ,function () {
			expect($scope.showUnauthorizedProductValues).toBeDefined();
		});

		it('should call ReportServices.showUnauthorizedProducts', function () {
			var breachedProduct = [
				{
					dNotional: 123,
					sCalendarDate: '01/01/2015',
					sCusip: 'Aggr_D000_AB',
					sIssueName: 'Aggr_D00_AB',
					sProduct: 'Securities Financing',
					sProductSubType: 'Stock Borrow',
					sProductType: 'Stock Borrow',
					sTradebook: 'A123',
					sVolckerDesk: 'Agencies/CP'
				},
				{},
				{}
			];

			spyOn(ctrlDeps.ReportServices, 'showUnauthorizedProducts').and.callFake(function () {
				return breachedProduct;
			});

			$scope.showUnauthorizedProductValues('Security Borrow');
			expect(ctrlDeps.ReportServices.showUnauthorizedProducts).toHaveBeenCalledWith(
				'Security Borrow',
				ctrlDeps.ReportServices.multiDayStartDate,
				ctrlDeps.ReportServices.multiDayEndDate,
				ctrlDeps.CacheService.selectedDesk.sVolckerDeskName
			);
		});
	});

	describe('gridOptions', function () {

		it('should be defined', function () {
			expect($scope.gridOptions).toBeDefined();
		});

		it('should have columnDefs', function () {
			expect($scope.gridOptions.columnDefs).toBeDefined();
		});

		it('should have virtualizationThreshold', function () {
			expect($scope.gridOptions.virtualizationThreshold).toBeDefined();
		});

		it('should have expandableRowTemplate', function () {
			expect($scope.gridOptions.expandableRowTemplate).toEqual('risk-position-limits-usage/rowTemplate.html');
		});

	});
	
	describe('exportToExcel', function () {
		var exportParams;
		beforeEach(function () {
			exportParams = {
				p_iVolckerDeskId: $scope.desk.selected.VolckerDeskID,
				p_sPanel: 1,
				p_dtReviewStart: '01-01-2015',
				p_dtReviewEnd: '12-31-2015',
				p_bIsMultidayView: true,
				p_bBreachOnly: false
			};

		});

		it('should be defined', function () {

			expect($scope.exportToExcel).toBeDefined();
			expect(baseMultiDayCtrl.exportToExcel).toBeDefined();
		});

		it('should open excel file in a new window', function () {
			spyOn(ctrlDeps.ReportServices, 'exportToCsv').and.callFake(function () {
				ctrlDeps.$window.open();
			});

			spyOn( ctrlDeps.$window, 'open' ).and.callFake( function() {
				return true;
			} );

			$scope.gridApi = {
				core : {
					getVisibleRows: function(grid) {
						return [{},{},{}];
					}
				}
			};

			$scope.exportToExcel(1, exportParams);
			expect(ctrlDeps.$window.open).toHaveBeenCalled();
			expect(ctrlDeps.ReportServices.exportToCsv).toHaveBeenCalled();
		});

		it('should alert if no report is being displayed', function () {
			spyOn(ctrlDeps.ReportServices, 'exportToCsv').and.callFake(function () {
				return false;
			});

			$scope.gridApi = {
				core : {
					getVisibleRows: function(grid) {
						return [];
					}
				}
			};

			spyOn( ctrlDeps.$window, 'alert' ).and.callFake( function() {
				return true;
			} );

			$scope.exportToExcel(1, exportParams);
			expect(ctrlDeps.$window.alert).toHaveBeenCalled();
			expect(ctrlDeps.ReportServices.exportToCsv).not.toHaveBeenCalled();
		});
	});

	describe('reviewReport', function () {
		it('should call ReportServices.reviewMultiDayAndDayView', function () {
			spyOn(ctrlDeps.ReportServices, 'reviewMultiDayAndDayView').and.callFake(function () {
				return true;
			});
			$scope.reviewReport();
			expect(ctrlDeps.ReportServices.reviewMultiDayAndDayView).toHaveBeenCalled();
		});
		
	});

	describe('getReports', function() {

		it('should be defined', function() {
			expect($scope.getReports).toBeDefined();
		});

		it('should call ReportServices MultiDayReport', function () {
			var data;
			var defer;
			spyOn(ctrlDeps.ReportServices.MultiDayReport, 'query').and.callFake(function() {

				data = [
					{
					LimitUsageDTOData: [
						{
							oBookDetailsDTOData: null,
							oPrimaryAmountsDTO: {
							sProductUsage: "Primary",
							dUsage: 411858621.48000014,
							dLimUtil: null,
							dLimitAmount: null,
							dLimitMin: null,
							dLimitMax: null,
							dLimitBreach: 80,
							sLimitDisplay: "/",
							bBreachError: false,
							bBreachWarning: false,
							bIsOverridden: false
						},
						oHedgeAmountsDTO: {
							sProductUsage: "Hedge",
							dUsage: 242283277.44,
							dLimUtil: null,
							dLimitAmount: null,
							dLimitMin: null,
							dLimitMax: null,
							dLimitBreach: 80,
							sLimitDisplay: "/",
							bBreachError: false,
							bBreachWarning: false,
							bIsOverridden: false
						},
						oTotalAmountsDTO: {
							sProductUsage: "Total",
							dUsage: 654141898.9200001,
							dLimUtil: 0.43609459928000005,
							dLimitAmount: 1500000000,
							dLimitMin: null,
							dLimitMax: 1500000000,
							dLimitBreach: 80,
							sLimitDisplay: " / 1,500,000,000",
							bBreachError: false,
							bBreachWarning: false,
							bIsOverridden: false
						},
						sCalendarDate: "2015-07-01",
							sDivision: "Fixed Income",
							sVolckerDesk: "Agencies/CP",
							sTradebook: null,
							sMeasureType: "Gross MV",
							dMeasureId: 3,
							sUnit: "$",
							sSecurityId: null,
							sSecurityDescription: null,
							sProductHierarchy1: null,
							sProductHierarchy2: null,
							sProductHierarchy3: null
						},
						{},
						{},
						{},
						{},
						{},
						{},
						{},
						{},
						{}
						],
						bIsReviewClicked: false,
						ReviewID: 1141,
						sPanel: "1",
						VolckerDeskID: 1,
						sVolckerDeskName: "Agencies/CP",
						dtReview: "7/1/2015",
						ReviewStatusID: 1,
						sReviewerLogin: "USI\yalunzhu",
						dtTimeStamp: "7/16/2015",
						sDescription: "Reviewed",
						sComment: "OK again, OK",
						ReviewStatus: "",
						BreachStatus: "Breached"
					},
					{},
					{}
				];
				defer = ctrlDeps.$q.defer();
				defer.resolve(data);
				return defer.promise;
			});
			$scope.getReports();
			expect(ctrlDeps.ReportServices.MultiDayReport.query).toHaveBeenCalled();
			expect($scope.gridOptions.data).toBe(defer.promise);
			expect($scope.gridOptions.data.$$state.value).toBe(data);
		});
		
		it('getReports should set noReportData flag to false if no data comes is feteched', function () {
			mockData = [];
			$scope.getReports();
			httpBackend.flush();
			expect($scope.noReportData).toBe(true);
		});
	});

	describe('search', function () {
		it('should be defined', function () {
			expect($scope.search).toBeDefined();
		});

		it('should alert user if date range is not selected', function () {
			$scope.dateOptions.startDate = undefined;
			$scope.dateOptions.endDate = undefined;
			spyOn(ctrlDeps.$window, 'alert');
			$scope.search();
			expect(ctrlDeps.$window.alert).toHaveBeenCalled();
		});

		it('should do nothing if date range, breach status and desk are not modified', function() {
			$scope.dateOptions.startDate = '01/01/2015';
			$scope.dateOptions.endDate = '12/01/2015';
			$scope.desk = {
				selected: {'sVolckerDeskName': 'hello'}
			};
			$scope.breachOnly = { val: true};

			ctrlDeps.ReportServices.multiDayStartDate = '01/01/2015';
			ctrlDeps.ReportServices.multiDayEndDate = '12/01/2015';
			ctrlDeps.CacheService.selectedDesk = $scope.desk.selected;
			ctrlDeps.ReportServices.breachOnly = $scope.breachOnly.val;
			$scope.search();
			expect($scope.isSearching).toBeFalsy();
		});

	});
	
	// Here are the $scope functions which are extracted to common/controllers/multi-day-ctrl.
	describe('$scope variables that are "inherited" from base MultiDayController', function () {
		it('should have activeTab', function () {
			expect($scope.$parent.activeTab).toEqual('/multi-day');
		});

		it('should have desks', function () {
			expect($scope.desks).toBeDefined();
		});

		it('should have desk', function () {
			expect($scope.desk).toBeDefined();
		});

		it('should have formatStartDate', function () {
			expect($scope.formatStartDate).toBeDefined();
		});

		it('should have formatEndDate', function () {
			expect($scope.formatEndDate).toBeDefined();
		});

		it('should have breachOnly', function () {
			expect($scope.breachOnly).toBeDefined();
		});

		it('should have dateOptions', function () {
			expect($scope.dateOptions).toBeDefined();
		});

		it('should have gridScope', function () {
			expect($scope.gridScope).toBeDefined();
		});

		it('should have disableReview', function () {
			expect($scope.disableReview).toBeDefined();
		});

		it('should have openStartCalendar', function () {
			expect($scope.openStartCalendar).toBeDefined();
		});

		it('should have openEndCalendar', function () {
			expect($scope.openEndCalendar).toBeDefined();
		});

		it('should have transitionToCognos', function () {
			expect($scope.transitionToCognos).toBeDefined();
		});

		describe('transitionToCognos', function () {
			it('should be defined', function () {
				expect($scope.transitionToCognos).toBeDefined();
			});

			it('should open cognos report in a new window', function () {
				spyOn( ctrlDeps.$window, 'open' ).and.callFake( function() {
					return true;
				} );

				$scope.gridOptions.data = [{},{},{}];
				$scope.gridApi = {
					core : {
						getVisibleRows: function(grid) {
							return [{},{},{}];
						}
					}
				};

				$scope.transitionToCognos();
				expect(ctrlDeps.$window.open).toHaveBeenCalled();
			});

			it('should alert user there is zero report', function () {
				spyOn(ctrlDeps.$window, 'alert').and.callFake(function() {
					return true;
				});

				$scope.gridOptions.data = [];
				$scope.gridApi = {
					core : {
						getVisibleRows: function(grid) {
							return [{},{},{}];
						}
					}
				};

				$scope.transitionToCognos();
				expect(ctrlDeps.$window.alert).toHaveBeenCalled();
			});

			it('should alert user there is no report being shown', function () {
				spyOn(ctrlDeps.$window, 'alert').and.callFake(function() {
					return true;
				});

				$scope.gridOptions.data = [{report: 1},{report:2},{report:3}];
				$scope.gridApi = {
					core : {
						getVisibleRows: function(grid) {
							return [];
						}
					}
				};

				$scope.transitionToCognos();
				expect(ctrlDeps.$window.alert).toHaveBeenCalled();
			});
		});
		
	});

	describe('baseMultiDayCtrl helper private functions', function () {
		it('should have searchWrapper', function () {
			expect(baseMultiDayCtrl.searchWrapper).toBeDefined();
		});

		it('should have initSearch', function () {
			expect(baseMultiDayCtrl.initSearch).toBeDefined();
		});

		it('should have cacheReport', function () {
			expect(baseMultiDayCtrl.cacheReport).toBeDefined();
		});

		it('should have onReportError', function () {
			expect(baseMultiDayCtrl.onReportError).toBeDefined();
		});

		it('should have exportToExcel', function () {
			expect(baseMultiDayCtrl.exportToExcel).toBeDefined();
		});

		it('should have loadReportFromCache', function () {
			expect(baseMultiDayCtrl.loadReportFromCache).toBeDefined();
		});
	});
});
