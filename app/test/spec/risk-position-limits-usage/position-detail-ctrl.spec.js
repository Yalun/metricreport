'use strict';

describe('Panel 1 (Risk Position Limits Usage) - detail - position level', function () {
	var ctrl,
		ctrlDeps,
		$scope,
		$modalInstance;
	
	beforeEach(module('app.risk-position-limits-usage', 'app.common', 'ui.router', 'app', 'ui.bootstrap'));

	beforeEach(inject(function ($controller, $rootScope, $http, riskPositionLimitsUsageReportServices, usSpinnerService) {

		$scope = $rootScope.$new();

		//http://stackoverflow.com/questions/22246813/unit-testing-testing-a-modalinstance-controller-with-karma-jasmine
		$modalInstance = {                    // Create a mock object using spies
			close: jasmine.createSpy('modalInstance.close'),
			dismiss: jasmine.createSpy('modalInstance.dismiss'),
			result: {
				then: jasmine.createSpy('modalInstance.result.then')
			}
		};

		var row = {
			entity: {
				sVolckerDesk: 'hello',
				sTradebook: 'what',
				sMeasureType: 'type A',
				sCalendarDate: '2015-01-01'
			}
		};

		ctrlDeps = {
			$scope: $scope,
			$http: $http,
			$modalInstance: $modalInstance,
			row: row,
			deskID: 12345,
			ReportServices:riskPositionLimitsUsageReportServices,
			usSpinnerService: usSpinnerService
		};

		spyOn(ctrlDeps.ReportServices.PositionDetailReport, 'query').and.callFake(function() {
				return [];
			});

		ctrl = $controller('PositionUsageController', ctrlDeps);
	}));
	
	it('should be defined', function () {
		expect(ctrl).toBeDefined();
		// console.dir(ctrl);
	});

	it('should specify the selected desk, book, limit', function () {
		expect($scope.selectedDesk).toEqual(ctrlDeps.row.entity.sVolckerDesk);
		expect($scope.selectedBook).toEqual(ctrlDeps.row.entity.sTradebook);
		expect($scope.selectedLimit).toEqual(ctrlDeps.row.entity.sMeasureType);
	});

	it('should have positionsGridOptions', function () {
		expect($scope.positionsGridOptions).toBeDefined();
		expect($scope.positionsGridOptions.columnDefs).toBeDefined();
	});

	it('should make API call to get the position level detail', function () {
		expect(ctrlDeps.ReportServices.PositionDetailReport.query).toHaveBeenCalled();
	});
});