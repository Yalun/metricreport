'use strict';

describe('(Table of) ContentController', function () {
	var ctrl, $scope;

	beforeEach(module('app', 'app.table-of-content'));
	beforeEach(inject(function ($controller, $rootScope) {
		$scope = $rootScope.$new();
		ctrl = $controller('ContentController', {
			$scope: $scope
		});
	}));

	it('should be defined', function () {
		expect(ctrl).toBeDefined();
	});

	it('should have table of content of 18 items', function () {
		// console.log($scope.tableContent);
		expect($scope.tableContent.length).toEqual(18);
	})
});