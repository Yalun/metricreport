'use strict';

describe('Panel 3 (VaR and sVaR) - day', function () {
	var ctrl,
		ctrlDeps,
		baseDayCtrl,
		baseDayCtrlDeps,
		$scope;

	beforeEach(module('app.VaR-and-sVaR', 'app.common', 'ui.router', 'app'));

	beforeEach(inject(function ($controller, $rootScope, $http, $state, $filter, $window, $timeout, ErrorService, uiGridConstants, VaRandSVaRReportServices, CacheService, usSpinnerService, CommonHelperService) {

		$scope = $rootScope.$new();

		baseDayCtrlDeps = {
			$scope: $scope,
			$filter: $filter,
			ErrorService: ErrorService,
			CacheService: CacheService,
			ReportServices: VaRandSVaRReportServices,
			$state: $state,
			$timeout: $timeout,
			CommonHelperService: CommonHelperService
		};

		baseDayCtrl = $controller('DayController', baseDayCtrlDeps);

		ctrlDeps = {
			$scope: $scope,
			$controller: $controller,
			$filter: $filter,
			$window: $window,
			$http: $http,
			$timeout: $timeout,
			ErrorService: ErrorService,
			uiGridConstants: uiGridConstants,
			ReportServices: VaRandSVaRReportServices,
			CacheService: CacheService,
			usSpinnerService: usSpinnerService
		};

		ctrl = $controller('VaRandSVaRDayController', ctrlDeps);
	}));

	it('should be defined', function () {
		expect(ctrl).toBeDefined();
		// console.dir(ctrl);d
	});

	describe('reviewReport', function () {
		// var $httpBackend;
		beforeEach(inject(function($injector) {
			// define fake function
			$scope.gridApi = {
				selection : {
					getSelectedRows: function () {
						return [{report:1}];
					}
				}
			};

			// $httpBackend = $injector.get('$httpBackend');
		}));

		it('should be defined', function () {
			expect($scope.reviewReport).toBeDefined();
		});

	});

	describe('gridOptions', function () {

		it('should be defined', function () {
			expect($scope.gridOptions).toBeDefined();
		});

		it('should have columnDefs', function () {
			expect($scope.gridOptions.columnDefs).toBeDefined();
		});

		it('should have virtualizationThreshold', function () {
			expect($scope.gridOptions.virtualizationThreshold).toBeDefined();
		});

	});

	describe('exportToExcel', function () {
		it('should be defined', function () {
			expect($scope.exportToExcel).toBeDefined();
		});

		it('should open excel file in a new window', function () {

			$scope.gridApi = {
				core : {
					getVisibleRows: function(grid) {
						return [{},{},{}];
					}
				}
			};

			spyOn( ctrlDeps.$window, 'open' ).and.callFake( function() {
				return true;
			} );

			$scope.exportToExcel();
			expect(ctrlDeps.$window.open).toHaveBeenCalled();
		});

		it('should alert if no report is being displayed', function () {

			$scope.gridApi = {
				core : {
					getVisibleRows: function(grid) {
						return [];
					}
				}
			};

			spyOn( ctrlDeps.$window, 'alert' ).and.callFake( function() {
				return true;
			} );

			$scope.exportToExcel();
			expect(ctrlDeps.$window.alert).toHaveBeenCalled();
		});
	});

	describe('transitionToCognos', function () {
		it('should be defined', function () {
			expect($scope.transitionToCognos).toBeDefined();
		});

		it('should open cognos report in a new window', function () {
			spyOn( ctrlDeps.$window, 'open' ).and.callFake( function() {
				return true;
			} );

			$scope.gridOptions.data = [{},{},{}];
			$scope.gridApi = {
				core : {
					getVisibleRows: function(grid) {
						return [{},{},{}];
					}
				}
			};

			$scope.transitionToCognos();
			expect(ctrlDeps.$window.open).toHaveBeenCalled();
		});

		it('should alert user there is zero report', function () {
			spyOn(ctrlDeps.$window, 'alert').and.callFake(function() {
				return true;
			});

			$scope.gridOptions.data = [];
			$scope.gridApi = {
				core : {
					getVisibleRows: function(grid) {
						return [{},{},{}];
					}
				}
			};

			$scope.transitionToCognos();
			expect(ctrlDeps.$window.alert).toHaveBeenCalled();
		});

		it('should alert user there is no report being shown', function () {
			spyOn(ctrlDeps.$window, 'alert').and.callFake(function() {
				return true;
			});

			$scope.gridOptions.data = [{report: 1},{report:2},{report:3}];
			$scope.gridApi = {
				core : {
					getVisibleRows: function(grid) {
						return [];
					}
				}
			};

			$scope.transitionToCognos();
			expect(ctrlDeps.$window.alert).toHaveBeenCalled();
		});
	});

	describe('getReports', function() {

		it('should be defined', function() {
			expect($scope.getReports).toBeDefined();
		});

        it('should query day report', function () {
            spyOn(ctrlDeps.ReportServices.DayReport, 'query').and.callFake(function() {
				return [];
			});
			$scope.getReports();
			expect(ctrlDeps.ReportServices.DayReport.query).toHaveBeenCalled();
        });
	});

	describe('searchReports', function () {
		it('should be defined', function () {
			expect($scope.searchReports).toBeDefined();
		});
	});

});
