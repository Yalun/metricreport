'use strict';

describe('Panel 3 (Var and sVaR) - main', function () {
	var ctrl,
		ctrlDeps,
		panelCtrl,
		panelCtrlDeps,
		templates,
		$scope,
		moduleName;

	beforeEach(module('app.VaR-and-sVaR', 'app.common', 'ui.router', 'app'));

	beforeEach(inject(function ($controller, $rootScope, $state, CacheService, VaRandSVaRReportServices, $filter, ErrorService, CommonHelperService) {

		moduleName = 'VaR-and-sVaR';

		$scope = $rootScope.$new();
		// console.log($scope);

		panelCtrlDeps = {
			$scope: $scope,
			$state: $state,
			$filter: $filter,
			CacheService: CacheService,
			// ReportServices: VaRandSVaRReportServices,
			ErrorService: ErrorService,
			CommonHelperService: CommonHelperService,
			moduleName: moduleName
		};
		// base panel controller which contains common shared functionality for all */main.js
		panelCtrl = $controller('PanelController', panelCtrlDeps );

		templates = [ [], [], [] ];

		ctrlDeps = {
			$scope: $scope,
			$controller: $controller,
			templates: templates,
			ReportServices: VaRandSVaRReportServices
		};

		ctrl = $controller('VaRandSVaRController', ctrlDeps );
	}));

	it('should be defined', function () {
		expect(ctrl).toBeDefined();
		// console.dir(ctrl);
	});

	describe('when State changes from one panel to another', function () {
		var toState = {name: 'some-other-panel'};
		var toParams = {};
		var fromState = {name: 'VaR-and-sVaR'};
		var fromParams = {};

		it('should clear the cache stored in CacheService', function () {
			// spyOn($scope, 'clearCache');
			$scope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
			// expect($scope.clearCache).toHaveBeenCalled();
		});

	});

	describe('when State changes within same module (AKA panel)', function () {
		it('from MultiDay to Day, should not clear the cache stored in CacheService', function () {
			var toState = {name: 'VaR-and-sVaR.day'};
			var toParams = {};
			var fromState = {name: 'VaR-and-sVaR.multi-day'};
			var fromParams = {};

			spyOn(ctrlDeps.ReportServices, 'clearCache');
			$scope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
			expect(ctrlDeps.ReportServices.clearCache).not.toHaveBeenCalled();
		});

		it('from MultiDay to Detail, should not clear the cache stored in CacheService', function () {
			var toState = {name: 'VaR-and-sVaR.detail'};
			var toParams = {};
			var fromState = {name: 'VaR-and-sVaR.multi-day'};
			var fromParams = {};

			spyOn(ctrlDeps.ReportServices, 'clearCache');
			$scope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
			expect(ctrlDeps.ReportServices.clearCache).not.toHaveBeenCalled();
		});

		it('from Day to MultiDay, should not clear the cache stored in CacheService', function () {
			var toState = {name: 'VaR-and-sVaR.multi-day'};
			var toParams = {};
			var fromState = {name: 'VaR-and-sVaR.day'};
			var fromParams = {};

			spyOn(ctrlDeps.ReportServices, 'clearCache');
			$scope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
			expect(ctrlDeps.ReportServices.clearCache).not.toHaveBeenCalled();
		});

		it('from Day to Detail, should not clear the cache stored in CacheService', function () {
			var toState = {name: 'VaR-and-sVaR.detail'};
			var toParams = {};
			var fromState = {name: 'VaR-and-sVaR.day'};
			var fromParams = {};

			spyOn(ctrlDeps.ReportServices, 'clearCache');
			$scope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
			expect(ctrlDeps.ReportServices.clearCache).not.toHaveBeenCalled();
		});

		it('from Detail to MultiDay, should not clear the cache stored in CacheService', function () {
			var toState = {name: 'VaR-and-sVaR.multi-day'};
			var toParams = {};
			var fromState = {name: 'VaR-and-sVaR.detail'};
			var fromParams = {};

			spyOn(ctrlDeps.ReportServices, 'clearCache');
			$scope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
			expect(ctrlDeps.ReportServices.clearCache).not.toHaveBeenCalled();
		});

		it('from Detail to Day, should not clear the cache stored in CacheService', function () {
			var toState = {name: 'VaR-and-sVaR.day'};
			var toParams = {};
			var fromState = {name: 'VaR-and-sVaR.detail'};
			var fromParams = {};

			spyOn(ctrlDeps.ReportServices, 'clearCache');
			$scope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
			expect(ctrlDeps.ReportServices.clearCache).not.toHaveBeenCalled();
		});

	});

});
