angular.module('app.inventory-turnover').service('inventoryTurnoverReportServices', ['BaseFactory', function (BaseFactory) {

	var module = 'inventoryTurnover';
	var service = new BaseFactory(module);

	service.showCognos = function (params) {
		var panelName =
			"&ui.object=%2fcontent%2ffolder%5b%40name%3d%27Corporate%27%5d%2ffolder%5b%40name%3d%27Volcker%27%5d%2ffolder%5b%40name%3d%27Panel%205%27%5d%2freport%5b%40name%3d%27Volcker%20Metrics%2c%20Panel%205%20%E2%80%93%20Inventory%20Turnover%20(Desk%20Level)%27%5d&ui.name=Volcker%20Metrics%2c%20Panel%205%20%E2%80%93%20Inventory%20Turnover%20(Desk%20Level)"; //TODO

		service.constructCongosURL(panelName, params, 5);
	};

	service.isDeskLevel = function (scope) {
		return !scope.row.entity.sTradebook;
	};

	service.editableIfDeskLevel = function (grid, row, col, rowRenderIndex, colRenderIndex) {
		// var classes = service.cellStatus(grid, row, col, rowRenderIndex, colRenderIndex);
		var classes = ['text-right'];
		if (!row.entity.sTradebook) {
			classes.push('editable');
		}
		return classes.join(' ');
	};

	var cellStatus = function (grid, row, col, rowRenderIndex, colRenderIndex) {
		var statuses = ['breached', 'approaching', 'revised'];
		var predefinedClass = ['text-right'];
		var colField = col.field.substring(0, col.field.indexOf('.'));
		var rowData;
		if (row.entity.oInventoryTurnoverItemDTO) {
			rowData = row.entity.oInventoryTurnoverItemDTO;
		} else {
			rowData = row.entity;
		}

		if (rowData[colField] && rowData[colField].bIsOverridden) {
			predefinedClass.push(statuses[2]);
		} else if (rowData[colField] && rowData[colField].bIsWarn) {
			predefinedClass.push(statuses[1]);
		} else if (rowData[colField] && rowData[colField].bIsBreach) {
			predefinedClass.push(statuses[0]);
		}
		return predefinedClass.join(' ');
	}
	
	service.cellStatus = function (type, editable) {
		var predefinedCellClass = 'text-right';
		var measureType = '';
		if (type === 'primary') {
			measureType = 'oPrimaryAmountsDTO';
			predefinedCellClass += ' primary-col';
		} else if (type === 'hedge') {
			measureType = 'oHedgeAmountsDTO';
			predefinedCellClass += ' hedge-col';
		} else if (type === 'total') {
			predefinedCellClass += ' total-col';
			measureType = 'oTotalAmountsDTO';
		}

		function breachStatus (grid, row, col, rowRenderIndex, colRenderIndex) {
			var e = row.entity;
			var cellClass = '';
			if (editable && e.desk) {
				 cellClass = 'editable ' ; // added white cell background
			}
			
			
			if (e[measureType] && e[measureType].bIsOverridden) {
				cellClass = cellClass + predefinedCellClass + ' revised';
			} else if (e[measureType] && e[measureType].bBreachError) {
				cellClass = cellClass + predefinedCellClass + ' breached';
			} else if (e[measureType] && e[measureType].bBreachWarning) {
				cellClass = cellClass + predefinedCellClass + ' approaching';
			} else {
				cellClass = cellClass + predefinedCellClass;
			}
			
			return cellClass;
		}
		return breachStatus;
	};
	return service;
}]);