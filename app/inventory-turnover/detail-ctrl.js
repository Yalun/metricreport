var inventoryTurnover = angular.module('app.inventory-turnover');

inventoryTurnover.controller('inventoryTurnoverDetailController', ['$scope', '$modal', '$filter', 'ErrorService', 'uiGridConstants', 'inventoryTurnoverReportServices', 'usSpinnerService', '$controller', 'toastr', function ($scope, $modal, $filter, ErrorService, uiGridConstants, ReportServices, usSpinnerService, $controller, toastr){

	$controller('DetailController', {
		$scope: $scope,
		ReportServices: ReportServices
	});
	
	var params = {
		p_iVolckerDeskId: $scope.deskID,
		p_sPanel: 5,
		p_dtReviewStart: $filter('date')(new Date($scope.date), $scope.queryFormat),
		p_dtReviewEnd: $filter('date')(new Date($scope.date), $scope.queryFormat),
		p_bIsMultidayView: false,
		p_bBreachOnly: false,
		p_sExtract: 'Tradebook'
	};

	var columnDefs = [
		{
			field: 'sTradebook',
			displayName: 'Book',
			width: 100,
			// enableSorting: false,
			enableFiltering: true,
			enableCellEdit: false,
			enableColumnResizing: true,
			// cellTemplate: '<div ng-if="row.entity.$$treeLevel === 0" class="ui-grid-cell-contents" ng-bind="row.entity[col.field]">{{row.entity[col.field]}}</div>'
			// cellTemplate:'<div class="ui-grid-cell-contents"><a ng-click=\"grid.appScope.showPosition(row)\" ng-bind="row.entity[col.field]">{{row.entity[col.field]}}</a></div>'
			
		},
		// {
		// 	field: 'document',
		// 	displayName: '',
		// 	width: 30,
		// 	enableFiltering: false,
		// 	enableColumnResizing: false,
		// 	enableCellEdit: false,
		// 	enableSorting: false,
		// 	cellTemplate:'<div class="ui-grid-cell-contents text-center" ng-if="row.entity.desk"><a class="glyphicon glyphicon-file" ng-click="grid.appScope.documentModal(row)"></a></div>'
		// },
		// {
		// 	field: 'comment',
		// 	displayName: '',
		// 	width: 30,
		// 	enableFiltering: false,
		// 	enableColumnResizing: false,
		// 	enableCellEdit: false,
		// 	enableSorting: false,
		// 	cellTemplate:'<div class="ui-grid-cell-contents text-center" ng-if="row.entity.desk"><a class="glyphicon glyphicon-pencil" ng-click="grid.appScope.commentModal(row)"></a></div>'
		// },
		// {
		// 	field: 'bookName',
		// 	width: 100,
		// 	enableFiltering: true,
		// 	enableCellEdit: false,
		// 	enableColumnResizing: true,
		// 	cellTemplate:'<div class="ui-grid-cell-contents"><a ng-click=\"grid.appScope.showPosition(row)\" ng-bind="row.entity[col.field]">{{row.entity[col.field]}}</a></div>'
		// },
		{
			field: 'oDays30DTO.dNumerator',
			displayName: '30 Day Numerator',
			minWidth: 122,
			enableFiltering: false,
			enableHiding: false,
			headerCellClass: 'dark-header',
			cellFilter: 'currency:"":0',
			cellClass: ReportServices.editableIfDeskLevel,
			type: 'number',
			cellEditableCondition: ReportServices.isDeskLevel
		},
		{
			field: 'oDays30DTO.dDenominator',
			displayName: '30 Day Denominator',
			minWidth: 122,
			enableFiltering: false,
			enableHiding: false,
			headerCellClass: 'dark-header',
			cellFilter: 'currency:"":0',
			cellClass: ReportServices.editableIfDeskLevel,
			type: 'number',
			cellEditableCondition: ReportServices.isDeskLevel
		},
		{
			field: 'oDays30DTO.dRatio',
			displayName: '30 Day Ratio',
			minWidth: 85,
			enableFiltering: false,
			enableHiding: false,
			enableCellEdit: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus(''),
			cellFilter: 'percentage:2'
		},
		{
			field: 'oDays60DTO.dNumerator',
			displayName: '60 Day Numerator',
			minWidth: 122,
			enableFiltering: false,
			enableHiding: false,
			cellFilter: 'currency:"":0',
			cellClass: ReportServices.editableIfDeskLevel,
			type: 'number',
			cellEditableCondition: ReportServices.isDeskLevel
		},
		{
			field: 'oDays60DTO.dDenominator',
			displayName: '60 Day Denominator',
			minWidth: 122,
			enableFiltering: false,
			enableHiding: false,
			cellFilter: 'currency:"":0',
			cellClass: ReportServices.editableIfDeskLevel,
			type: 'number',
			cellEditableCondition: ReportServices.isDeskLevel
		},
		{
			field: 'oDays60DTO.dRatio',
			displayName: '60 Day Ratio',
			minWidth: 85,
			enableFiltering: false,
			enableHiding: false,
			enableCellEdit: false,
			cellClass: ReportServices.cellStatus(''),
			cellFilter: 'percentage:2'
		},
		{
			field: 'oDays90DTO.dNumerator',
			displayName: '90 Day Numerator',
			minWidth: 122,
			enableFiltering: false,
			enableHiding: false,
			headerCellClass: 'dark-header',
			cellFilter: 'currency:"":0',
			cellClass: ReportServices.editableIfDeskLevel,
			type: 'number',
			cellEditableCondition: ReportServices.isDeskLevel
		},
		{
			field: 'oDays90DTO.dDenominator',
			displayName: '90 Day Denominator',
			minWidth: 122,
			enableFiltering: false,
			enableHiding: false,
			headerCellClass: 'dark-header',
			cellFilter: 'currency:"":0',
			cellClass: ReportServices.editableIfDeskLevel,
			type: 'number',
			cellEditableCondition: ReportServices.isDeskLevel
		},
		{
			field: 'oDays90DTO.dRatio',
			displayName: '90 Day Ratio',
			minWidth: 85,
			enableFiltering: false,
			enableHiding: false,
			enableCellEdit: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus(''),
			cellFilter: 'percentage:2'
		}
	];

	// $scope.gridScope = {
	// 	documentModal: $scope.documentModal,
	// 	commentModal: $scope.commentModal,
	// 	// showPosition: showPosition
	// };

	$scope.detailGridOptions = {
		virtualizationThreshold: 50,
		enableFiltering: true,
		enableCellEdit: true,
		enableCellEditOnFocus: true,
		enableSorting: true,
		enablePinning: true,
		enableGridMenu: false,
		enableColumnMenus: false,
		// enableColumnResizing: true,
		onRegisterApi : function (gridApi) {
			$scope.gridApi = gridApi;
			// $timeout(function () {
			// 	$scope.gridApi.saveState.restore($scope, $scope.selectedTemplate.state);
			// },0);

			// remove the cell highlight if VaR or sVaR limits has been modified
			$scope.gridApi.edit.on.afterCellEdit(null, overrideInventoryTurnover);
		},
		appScopeProvider: $scope.gridScope,
		columnDefs: columnDefs
	};

	$scope.reviewReport = function () {
		$scope.isReviewing = true;
		var data = [];
		$scope.desk.bIsReviewClicked = true;
		data.push($scope.desk);

		ReportServices.review(data, function(response) {
			$scope.desk.sDescription = 'Reviewed';
			$scope.desk.ReviewStatusID = 1;
			$scope.desk.sReviewerLogin = response.data[0].sReviewerLogin;
			//$scope.desk.sComment = data[0].sComment;

			$scope.isReviewing = false;
			ReportServices.isOutOfSync.multiDay = true;
			ReportServices.isOutOfSync.day = true;
			toastr.success('Desk Reviewed');
		}, function (error) {
			$scope.isReviewing = false;
			toastr.error(ErrorService.handleError(error.data));
		});
	};

	$scope.showDocument = function () {
		$scope.documentModal(null, $scope.desk);
	};

	$scope.showComment = function () {
		$scope.commentModal(null, $scope.desk.ReviewID);
	};

	$scope.exportToExcel = function () {
		ReportServices.exportToCsv(params);
	};
	
	$scope.getDetailReport = function () {
		usSpinnerService.spin('detail-spinner');
		$scope.noReportData = false;
		ReportServices.DetailReport.query(params, function (data) {
			$scope.detailGridOptions.data = flattenBook(data[0]).flattenedDetailsDTOData;

			$scope.noReportData = $scope.detailGridOptions.data.length === 0 ? true : false;

			$scope.desk = data[0];

			usSpinnerService.stop('detail-spinner');
		}, function (error) {
			usSpinnerService.stop('detail-spinner');
			$scope.error = ErrorService.handleError(error.data);
		});
	};

	$scope.getDetailReport();

	function flattenBook (data) {
		var oInventoryTurnoverItemDTO = data.oInventoryTurnoverItemDTO;
		var bookArray = oInventoryTurnoverItemDTO.oDetailsDTOData;
		var flattenedArray = [];
		var deskLevelDetail = {
			"oDays30DTO": oInventoryTurnoverItemDTO.oDays30DTO,
			"oDays60DTO": oInventoryTurnoverItemDTO.oDays60DTO,
			"oDays90DTO": oInventoryTurnoverItemDTO.oDays90DTO
		};

		flattenedArray.push(deskLevelDetail);
		for (var i = 0; i< bookArray.length; i++) {
			flattenedArray.push(bookArray[i]);
		}
		//re-append the flatten array back to the original data
		data.flattenedDetailsDTOData = flattenedArray;
		return data;
	}

	function overrideInventoryTurnover (rowEntity, colDef, newVal, oldVal) {
		var colDisplayName = colDef.displayName;
		var colDefField = colDef.field;

		if (newVal === null || newVal === undefined || newVal === '') {
			rowEntity[colDefField] = oldVal;
			return;
		}
		if (newVal !== oldVal) {
			confirmOverride(rowEntity, colDefField, colDisplayName, newVal, oldVal, uiGridConstants);
		}
	}

	function confirmOverride (entity, colDefField, colDisplayName, newVal, oldVal, uiGridConstants ) {
		var modalInstance = $modal.open({
			templateUrl: 'common/templates/overrideModal.html',
			controller: 'OverrideModalController',
			size: 'md',
			scope: $scope,
			backdrop: 'static',
			keyboard: false,
			resolve: {
				'OverrideURL': function () {
					return ReportServices.OverrideURL;
				},
				'entity' : function () {
					return {
						newVal : newVal,
						oldVal : oldVal,
						type: colDisplayName
					}
				},
				'overrideParams' : function () {
					var dayRange = colDefField.substring(0, colDefField.indexOf('.'));
					var numeratorOrDenominator = colDefField.substring(colDefField.indexOf('.') + 1);
					var p_iNumDays;

					if (dayRange === 'oDays30DTO') {
						p_iNumDays = 30;
					} else if (dayRange === 'oDays60DTO') {
						p_iNumDays = 60;
					} else if (dayRange === 'oDays90DTO') {
						p_iNumDays = 90;
					}

					return {
						p_iReviewId: $scope.desk.ReviewID,
						p_iNumDays: p_iNumDays,
						p_bIsDenominator: numeratorOrDenominator === 'dDenominator' ? true : false,
						p_dOverrideValue: newVal,
						p_sComment: ''
					};
				},
				'onOverrideSucess': function () {
					return function () {
						$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.EDIT);
						
						ReportServices.isOutOfSync.multiDay = true;
						ReportServices.isOutOfSync.day = true;

						$scope.getDetailReport();
					}
				},
				'resetVal' : function () {
					return function () {
						entity[colDefField] = oldVal;
						$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.EDIT);
					}
				}
			}
		});
	}
}]);

// Modal Controller
// inventoryTurnover.controller('inventoryTurnoverPositionUsageController', ['$scope', '$http', '$modalInstance', 'selectedDesk', 'selectedBook', 'selectedSensitivity', 'inventoryTurnoverReportServices', function ($scope, $http, $modalInstance, selectedDesk, selectedBook, selectedSensitivity, ReportServices) {
// 	$scope.selectedDesk = selectedDesk;
// 	$scope.selectedBook = selectedBook;
// 	$scope.selectedSensitivity = selectedSensitivity;
// 	$scope.cancel = function () {
// 		$modalInstance.dismiss('cancel');
// 	};

// 	$scope.positionsGridOptions = {
// 		enableFiltering: true,
// 		enableColumnResizing: true,
// 		enablePinning: true,
// 		enableColumnMoving: true
// 	};

// 	ReportServices.DetailReport.query(function (data) {
// 		$scope.detailGridOptions.data = data;
// 		$scope.noReportData = $scope.detailGridOptions.data.length === 0 ? true : false;
// 		usSpinnerService.stop('detail-spinner');
// 	}, function (error) {
// 		usSpinnerService.stop('detail-spinner');
// 		$scope.error = ErrorService.handleError(error.data);
// 	});

// 	$scope.positionsGridOptions.columnDefs = [
// 		{
// 			name: 'positionID',
// 			displayName: 'Position',
// 			width: 120,
// 			enableFiltering: true,
// 			enableSorting: true,
// 			enableHiding: false,
// 			pinnedLeft: true,
// 			enableHiding: false
// 		},
// 		{
// 			name: 'securityDescription',
// 			displayName: 'Security Description',
// 			width: 120,
// 			enableHiding: false
// 		},
// 		{
// 			name: 'productHierarchy1',
// 			displayName: 'Product Hierarchy 1',
// 			enableHiding: false,
// 			width: 120
// 		},
// 		{
// 			name: 'productHierarchy2',
// 			displayName: 'Product Hierarchy 2',
// 			enableHiding: false,
// 			width: 120
// 		},
// 		{
// 			name: 'productHierarchy3',
// 			displayName: 'Product Hierarchy 3',
// 			enableHiding: false,
// 			width: 120
// 		},
// 		{
// 			displayName: 'VaR Limit Size',
// 			field: 'VaRLimitSize',
// 			enableSorting: false,
// 			enableFiltering: false,
// 			headerCellClass: 'dark-header',
// 			enableHiding: false,
// 			width: 120,
// 			cellFilter: 'currency:"":0'
// 		},
// 		{
// 			displayName: 'VaR Val Usage',
// 			field: 'VaRValUsage',
// 			enableSorting: false,
// 			enableFiltering: false,
// 			headerCellClass: 'dark-header',
// 			enableHiding: false,
// 			width: 120,
// 			cellFilter: 'currency:"":0'
// 		},
// 		{
// 			displayName: 'VaR Limit Usage',
// 			field: 'VaRLimitUsage',
// 			enableSorting: false,
// 			enableFiltering: false,
// 			headerCellClass: 'dark-header',
// 			enableHiding: false,
// 			width: 120,
// 			cellFilter: 'percentage:2'
// 		},
// 		{
// 			displayName: 'VaR Unit of Measure',
// 			field: 'VaRUnit',
// 			enableSorting: false,
// 			enableHiding: false,
// 			enableFiltering: false,
// 			headerCellClass: 'dark-header',
// 			width: 120
// 		},
// 		{
// 			displayName: 'sVaR Limit Size',
// 			field: 'sVaRLimitSize',
// 			enableSorting: false,
// 			enableFiltering: false,
// 			enableHiding: false,
// 			width: 120,
// 			cellFilter: 'currency:"":0'
// 		},
// 		{
// 			displayName: 'sVaR Val Usage',
// 			field: 'sVaRValUsage',
// 			enableSorting: false,
// 			enableFiltering: false,
// 			enableHiding: false,
// 			width: 120,
// 			cellFilter: 'currency:"":0'
// 		},
// 		{
// 			displayName: 'sVaR Limit Usage',
// 			field: 'sVaRLimitUsage',
// 			enableSorting: false,
// 			enableFiltering: false,
// 			enableHiding: false,
// 			width: 120,
// 			cellFilter: 'percentage:2'
// 		},
// 		{
// 			displayName: 'sVaR Unit of Measure',
// 			field: 'sVaRUnit',
// 			enableSorting: false,
// 			enableFiltering: false,
// 			enableHiding: false,
// 			width: 120
// 		}
// 	];

// }]);