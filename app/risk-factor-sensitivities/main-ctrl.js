var RFS = angular.module('app.risk-factor-sensitivities');

RFS.controller('RiskFactorSensitivitiesController',['$scope', 'RiskFactorSensitivitiesReportServices', 'templates', '$controller', 'ErrorService', 'toastr', function ($scope, ReportServices, templates, $controller, ErrorService, toastr){

	var baseController = $controller('PanelController', {
		$scope: $scope,
		// ReportServices: ReportServices,
		moduleName: 'risk-factor-sensitivities'
	});

	$scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {

		// clear all cache for previous panel when switching panel
		if (toState.name.match(/[^.]*/i)[0] !== fromState.name.match(/[^.]*/i)[0]) {
			// $scope.clearCache();
			ReportServices.clearCache();
		} else {
			// console.log('change between sibiling routes within ', toState.name.match(/[^.]*/i)[0]);
			$scope.backTo = fromState.name;
		}
	});

	baseController.routeToMultiDay();

}]);
