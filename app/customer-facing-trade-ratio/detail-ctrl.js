var customerFacingTradeRatio = angular.module('app.customer-facing-trade-ratio');

customerFacingTradeRatio.controller('customerFacingTradeRatioDetailController', ['$scope', '$modal', 'ErrorService', 'uiGridConstants', 'customerFacingTradeRatioReportServices', 'usSpinnerService', '$controller', 'toastr', '$filter', function ($scope, $modal, ErrorService, uiGridConstants, ReportServices, usSpinnerService, $controller, toastr, $filter){

	$controller('DetailController', {
		$scope: $scope,
		ReportServices: ReportServices
	});

	var params = {
		p_iVolckerDeskId: $scope.deskID,
		p_sPanel: 7,
		p_dtReviewStart: $filter('date')(new Date($scope.date), $scope.queryFormat),
		p_dtReviewEnd: $filter('date')(new Date($scope.date), $scope.queryFormat),
		p_bIsMultidayView: false,
		p_bBreachOnly: false,
		p_sExtract: 'Tradebook'
	};

	function showPosition (row) {
		var modalInstance = $modal.open({
			templateUrl: 'customerFacingTradeRatioPositionLimitUsage.html',
			controller: 'customerFacingTradeRatioPositionUsageController',
			size: 'lg',
			windowClass: 'modal-xl',
			resolve: {
				'selectedSensitivity': function () {
					return row.entity.riskFactorSensitivity;
				},
				'params': function () {
					return {
						p_bIsMultidayView: false,
						p_sDesk: $scope.desk.sVolckerDeskName,
						p_iVolckerDeskId: $scope.desk.VolckerDeskID,
						p_dtReviewStart: $scope.desk.dtReview,
						p_dtReviewEnd: $scope.desk.dtReview,
						p_sBook: row.entity.sTradebook,
						p_sExtract: 'Position'
					}
				}
			}
		});
	};

	$scope.gridScope.showPosition = showPosition;

	$scope.exportToExcel = function () {
		ReportServices.exportToCsv(params);
	};

	$scope.reviewReport = function () {
		$scope.isReviewing = true;
		var data = [];
		$scope.desk.bIsReviewClicked = true;
		data.push($scope.desk);

		ReportServices.review(data, function(response) {
			$scope.desk.sDescription = 'Reviewed';
			$scope.desk.ReviewStatusID = 1;
			$scope.desk.sReviewerLogin = response.data[0].sReviewerLogin;
			//$scope.desk.sComment = data[0].sComment;

			$scope.isReviewing = false;
			ReportServices.isOutOfSync.multiDay = true;
			ReportServices.isOutOfSync.day = true;
			toastr.success('Desk Reviewed');
		}, function (error) {
			$scope.isReviewing = false;
			toastr.error(ErrorService.handleError(error.data));
		});
	};

	$scope.showDocument = function () {
		$scope.documentModal(null, $scope.desk);
	};

	$scope.showComment = function () {
		$scope.commentModal(null, $scope.desk.ReviewID);
	};

	var columnDefs = [
		{
			field: 'sTradebook',
			displayName: 'Tradebook',
			width: 100,
			// enableSorting: false,
			pinnedLeft: true,
			enableFiltering: true,
			enableCellEdit: false,
			enableColumnResizing: false,
			cellTemplate:'<div class="ui-grid-cell-contents"><a ng-click=\"grid.appScope.showPosition(row)\" ng-bind="row.entity[col.field]">{{row.entity[col.field]}}</a></div>'
		},
		{
			field: '30dayNumOfTransactionWithCustomers',
			displayName: '30 Day # of Transaction w/ Customers',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus,
			cellFilter: 'currency:"":0',
		},
		{
			field: '30dayNumOfTransactionNotWithCustomers',
			displayName: '30 Day # of Transaction w/ Non-Customers',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus,
			cellFilter: 'currency:"":0'
		},
		{
			field: '30RatioNumberTransaction',
			displayName: '30 Day CFTR (# of Transaction)',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus,
			cellFilter: 'percentage:2'
		},
		{
			field: '30ValueTransactionWithCustomers',
			displayName: '30 Day Value of Transaction w/ Customers',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus,
			cellFilter: 'currency:"":0'
		},
		{
			field: '30ValueTransactionNotWithCustomers',
			displayName: '30 Day Value of Transaction w/ Non-Customers',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus,
			cellFilter: 'currency:"":0'
		},
		{
			field: '30RatioValueOfTransactions',
			displayName: '30 Day CFTR (Value)',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus,
			cellFilter: 'percentage:2'
		},
		{
			field: '60NumberTransactionWithCustomers',
			displayName: '60 Day # of Transaction w/ Customers',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			cellClass: ReportServices.cellStatus,
			cellFilter: 'currency:"":0'
		},
		{
			field: '60NumberTransactionNotWithCustomers',
			displayName: '60 Day # of Transaction w/ Non-Customers',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			cellClass: ReportServices.cellStatus,
			cellFilter: 'currency:"":0'
		},
		{
			field: '60RatioNumberOfTransaction',
			displayName: '60 Day CFTR (# of Transaction)',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			cellClass: ReportServices.cellStatus,
			cellFilter: 'percentage:2'
		},
		{
			field: '60ValueTransactionWithCustomers',
			displayName: '60 Day Value of Transaction w/ Customers',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			cellClass: ReportServices.cellStatus,
			cellFilter: 'currency:"":0'
		},
		{
			field: '60ValueTransactionNotWithCustomers',
			displayName: '60 Day Value of Transaction w/ Non-Customers',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			cellClass: ReportServices.cellStatus,
			cellFilter: 'currency:"":0'
		},
		{
			field: '60RatioValueOfTransactions',
			displayName: '60 Day CFTR (Value)',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			cellClass: ReportServices.cellStatus,
			cellFilter: 'percentage:2'
		},
		{
			field: '90NumberTransactionWithCustomers',
			displayName: '90 Day # of Transaction w/ Customers',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus,
			cellFilter: 'currency:"":0'
		},
		{
			field: '90NumberTransactionNotWithCustomers',
			displayName: '90 Day # of Transaction w/ Non-Customers',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus,
			cellFilter: 'currency:"":0'
		},
		{
			field: '90RatioNumberOfTransaction',
			displayName: '90 Day CFTR (# of Transaction)',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus,
			cellFilter: 'percentage:2'
		},
		{
			field: '90ValueTransactionWithCustomers',
			displayName: '90 Day Value of Transaction w/ Customers',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus,
			cellFilter: 'currency:"":0'
		},
		{
			field: '90ValueTransactionNotWithCustomers',
			displayName: '90 Day Value of Transaction w/ Non-Customers',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus,
			cellFilter: 'currency:"":0'
		},
		{
			field: '90RatioValueOfTransactions',
			displayName: '90 Day CFTR (Value)',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus,
			cellFilter: 'percentage:2'
		}
		// {
		// 	displayName: 'VaR Limit Size',
		// 	field: 'dVaRLimitSize',
		// 	// enableSorting: false,
		// 	cellClass: ReportServices.cellStatus,
		// 	type: 'number',
		// 	cellEditableCondition: ReportServices.isDeskLevel,
		// 	enableFiltering: false,
		// 	enableCellEdit: false,
		// 	headerCellClass: 'dark-header',
		// 	cellFilter: 'currency:"":0',
		// 	width: 122
		// },
		// {
		// 	displayName: 'VaR Val Usage',
		// 	field: 'dVaRUsage',
		// 	cellClass: ReportServices.editableIfDeskLevel,
		// 	cellEditableCondition: ReportServices.isDeskLevel,
		// 	type: 'number',
		// 	// enableSorting: false,
		// 	enableCellEdit: true,
		// 	enableFiltering: false,
		// 	headerCellClass: 'dark-header',
		// 	cellFilter: 'currency:"":0',
		// 	width: 122
		// },
		// {
		// 	displayName: 'VaR Limit Usage',
		// 	field: 'dVaRLimitUsage',
		// 	cellClass: ReportServices.cellStatus,
		// 	enableCellEdit: false,
		// 	// enableSorting: false,
		// 	enableFiltering: false,
		// 	headerCellClass: 'dark-header',
		// 	cellFilter: 'percentage:2',
		// 	width: 152
		// },
		// {
		// 	displayName: 'sVaR Limit Size',
		// 	field: 'dSVaRLimitSize',
		// 	// enableSorting: false,
		// 	cellClass: ReportServices.cellStatus,
		// 	type: 'number',
		// 	enableFiltering: false,
		// 	enableCellEdit: false,
		// 	cellFilter: 'currency:"":0',
		// 	width: 122
		// },
		// {
		// 	displayName: 'sVaR Val Usage',
		// 	field: 'dSVaRUsage',
		// 	cellClass: ReportServices.editableIfDeskLevel,
		// 	type: 'number',
		// 	// enableSorting: false,
		// 	enableCellEdit: true,
		// 	enableFiltering: false,
		// 	cellFilter: 'currency:"":0',
		// 	width: 122
		// },
		// {
		// 	displayName: 'sVaR Limit Usage',
		// 	field: 'dSVaRLimitUsage',
		// 	cellClass: ReportServices.cellStatus,
		// 	// enableSorting: false,
		// 	enableCellEdit: false,
		// 	enableFiltering: false,
		// 	cellFilter: 'percentage:2',
		// 	width: 152
		// },
		// {
		// 	field: 'dNetMV',
		// 	displayName: 'Net MV',
		// 	width: 122,
		// 	// enableSorting: false,
		// 	enableFiltering: false,
		// 	headerCellClass: 'dark-header',
		// 	cellClass: ReportServices.cellStatus,
		// 	cellFilter: 'currency:"":0',

		// }
	];

	$scope.detailGridOptions = {
		virtualizationThreshold: 50,
		enableFiltering: true,
		enableCellEdit: true,
		enableCellEditOnFocus: true,
		enableSorting: true,
		enablePinning: true,
		enableGridMenu: false,
		enableColumnMenus: false,
		enableColumnMoving: false,
		// enableColumnResizing: true,
		onRegisterApi : function (gridApi) {
			$scope.gridApi = gridApi;
			// $timeout(function () {
			// 	$scope.gridApi.saveState.restore($scope, $scope.selectedTemplate.state);
			// },0);

			$scope.gridApi.edit.on.afterCellEdit(null, overrideUsage);
		},
		appScopeProvider: $scope.gridScope,
		columnDefs: columnDefs
	};

	function overrideUsage (rowEntity, colDef, newVal, oldVal) {
		var colDisplayName = colDef.displayName;
		var colDefField = colDef.field;
		// var scenario = colDefField.substring(0, colDefField.indexOf('.'));
		if (newVal === null || newVal === undefined || newVal === '') {
			rowEntity[colDefField] = oldVal;
			return;
		}
		if (newVal !== oldVal) {
			confirmOverride(rowEntity, colDefField, colDisplayName, newVal, oldVal, uiGridConstants);
		}
	}

	function confirmOverride (entity, colDefField, colDisplayName, newVal, oldVal, uiGridConstants ) {
		var modalInstance = $modal.open({
			templateUrl: 'common/templates/overrideModal.html',
			controller: 'OverrideModalController',
			size: 'md',
			scope: $scope,
			backdrop: 'static',
			keyboard: false,
			resolve: {
				'OverrideURL': function () {
					return ReportServices.OverrideURL;
				},
				'entity' : function () {
					return {
						newVal : newVal,
						oldVal : oldVal,
						type: colDisplayName
					};
				},
				'overrideParams' : function () {
					return {
						p_iReviewId: $scope.desk.ReviewID,
						p_bIsStressVar: colDefField === 'dSVaRUsage' ? true : false,
						p_dUsage: newVal,
						p_sComment: ''
					};
				},
				'onOverrideSucess': function () {
					return function () {
						entity[colDefField].bIsOverridden = true;
						$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.EDIT);
						
						ReportServices.isOutOfSync.multiDay = true;
						ReportServices.isOutOfSync.day = true;

						// $scope.getDetailReport(); // no needed because no recalculation is done on the backend
					}
				},
				'resetVal' : function () {
					return function () {
						entity[colDefField].val = oldVal;
						$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.EDIT);
					}
				}
			}
		});
	}

	function flattenBook (data) {
		var oVARItemDTO = data.oVARItemDTO;
		var bookArray = oVARItemDTO.oDetailsDTOData;
		var flattenedArray = [];
		var deskLevelDetail = {
			"dVaRLimitSize": oVARItemDTO.dVaRLimitSize,
			"dVaRUsage": oVARItemDTO.dVaRUsage,
			"dVaRLimitUsage": oVARItemDTO.dVaRLimitUsage,
			"dVaRBreach": oVARItemDTO.dVaRBreach,
			"dSVaRLimitSize": oVARItemDTO.dSVaRLimitSize,
			"dSVaRUsage": oVARItemDTO.dSVaRUsage,
			"dSVaRLimitUsage": oVARItemDTO.dSVaRLimitUsage,
			"dSVaRBreach": oVARItemDTO.dSVaRBreach,
			"dNetMV": oVARItemDTO.dNetMV,
			"sCalendarDate": oVARItemDTO.sCalendarDate,
			"sDivision": oVARItemDTO.sDivision,
			"sVolckerDesk": oVARItemDTO.sVolckerDesk,
			"sTradebook": oVARItemDTO.sTradebook,
			"sCusip": oVARItemDTO.sCusip,
			"sProductHierarchy": oVARItemDTO.sProductHierarchy,
			"sProductHierarchy2": oVARItemDTO.sProductHierarchy2,
			"sProductHierarchy3": oVARItemDTO.sProductHierarchy3,
			"bIsVaRBreach": oVARItemDTO.bIsVaRBreach,
			"bIsVaRWarn": oVARItemDTO.bIsVaRWarn,
			"bIsVaROverride": oVARItemDTO.bIsVaROverride,
			"bIsSVaRBreach": oVARItemDTO.bIsSVaRBreach,
			"bIsSVaRWarn": oVARItemDTO.bIsSVaRWarn,
			"bIsSVaROverride": oVARItemDTO.bIsSVaROverride
		};

		flattenedArray.push(deskLevelDetail);
		for (var i = 0; i< bookArray.length; i++) {
			flattenedArray.push(bookArray[i]);
		}
		//re-append the flatten array back to the original data
		data.flattenedDetailsDTOData = flattenedArray;
		return data;
	}

	$scope.getDetailReport = function () {
		usSpinnerService.spin('detail-spinner');
		$scope.noReportData = false;
		ReportServices.DetailReport.query(params, function (data) {
			$scope.detailGridOptions.data = flattenBook(data[0]).flattenedDetailsDTOData;
			// save a reference
			$scope.desk = data[0];
			$scope.noReportData = $scope.detailGridOptions.data.length === 0 ? true : false;
			usSpinnerService.stop('detail-spinner');
		}, function (error) {
			usSpinnerService.stop('detail-spinner');
			$scope.error = ErrorService.handleError(error.data);
		});
	};

	$scope.getDetailReport();
}]);

// Modal Controller
customerFacingTradeRatio.controller('customerFacingTradeRatioPositionUsageController', ['$scope', '$modalInstance', 'customerFacingTradeRatioReportServices', 'ErrorService', 'usSpinnerService', 'params', function ($scope, $modalInstance, ReportServices, ErrorService, usSpinnerService, params) {

	$scope.selectedDesk = params.p_sDesk;
	$scope.selectedBook = params.p_sBook;

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};

	$scope.positionsGridOptions = {
		enableFiltering: true,
		enableColumnResizing: true,
		enablePinning: true,
		enableColumnMoving: true,
		enableGridMenu: false,
		enableColumnMenus: false
	};

	ReportServices.PositionDetailReport.query(params, function (data) {
		$scope.positionsGridOptions.data = data[0].oVARItemDTO.oDetailsDTOData[0].oDetailsDTOData;

		$scope.noPositionData = $scope.positionsGridOptions.data.length === 0 ? true : false;

		usSpinnerService.stop('position-spinner');
	}, function (error) {
		$scope.error = ErrorService.handleError(error.data);
		usSpinnerService.stop('position-spinner');
	});

	$scope.exportPosition = function() {
		ReportServices.exportToCsv(params);
	};

	$scope.positionsGridOptions.columnDefs = [
		{
			name: 'sCusip',
			displayName: 'Position',
			width: 95,
			enableSorting: true,
			pinnedLeft: true,
			enableFiltering: true
		},
		{
			name: 'sSecurityDescription',
			displayName: 'Security Description',
			enableFiltering: false,
			width: 180,
		},
		{
			name: 'sProductHierarchy1',
			displayName: 'Product Hierarchy 1',
			enableFiltering: false,
			width: 180
		},
		{
			name: 'sProductHierarchy2',
			displayName: 'Product Hierarchy 2',
			enableFiltering: false,
			width: 180
		},
		{
			name: 'sProductHierarchy3',
			displayName: 'Product Hierarchy 3',
			enableFiltering: false,
			width: 180
		},
		{
			field: '30dayNumOfTransactionWithCustomers',
			displayName: '30 Day # of Transaction w/ Customers',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus,
			cellFilter: 'currency:"":0',
		},
		{
			field: '30dayNumOfTransactionNotWithCustomers',
			displayName: '30 Day # of Transaction w/ Non-Customers',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus,
			cellFilter: 'currency:"":0'
		},
		{
			field: '30RatioNumberTransaction',
			displayName: '30 Day CFTR (# of Transaction)',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus,
			cellFilter: 'percentage:2'
		},
		{
			field: '30ValueTransactionWithCustomers',
			displayName: '30 Day Value of Transaction w/ Customers',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus,
			cellFilter: 'currency:"":0'
		},
		{
			field: '30ValueTransactionNotWithCustomers',
			displayName: '30 Day Value of Transaction w/ Non-Customers',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus,
			cellFilter: 'currency:"":0'
		},
		{
			field: '30RatioValueOfTransactions',
			displayName: '30 Day CFTR (Value)',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus,
			cellFilter: 'percentage:2'
		},
		{
			field: '60NumberTransactionWithCustomers',
			displayName: '60 Day # of Transaction w/ Customers',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			cellClass: ReportServices.cellStatus,
			cellFilter: 'currency:"":0'
		},
		{
			field: '60NumberTransactionNotWithCustomers',
			displayName: '60 Day # of Transaction w/ Non-Customers',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			cellClass: ReportServices.cellStatus,
			cellFilter: 'currency:"":0'
		},
		{
			field: '60RatioNumberOfTransaction',
			displayName: '60 Day CFTR (# of Transaction)',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			cellClass: ReportServices.cellStatus,
			cellFilter: 'percentage:2'
		},
		{
			field: '60ValueTransactionWithCustomers',
			displayName: '60 Day Value of Transaction w/ Customers',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			cellClass: ReportServices.cellStatus,
			cellFilter: 'currency:"":0'
		},
		{
			field: '60ValueTransactionNotWithCustomers',
			displayName: '60 Day Value of Transaction w/ Non-Customers',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			cellClass: ReportServices.cellStatus,
			cellFilter: 'currency:"":0'
		},
		{
			field: '60RatioValueOfTransactions',
			displayName: '60 Day CFTR (Value)',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			cellClass: ReportServices.cellStatus,
			cellFilter: 'percentage:2'
		},
		{
			field: '90NumberTransactionWithCustomers',
			displayName: '90 Day # of Transaction w/ Customers',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus,
			cellFilter: 'currency:"":0'
		},
		{
			field: '90NumberTransactionNotWithCustomers',
			displayName: '90 Day # of Transaction w/ Non-Customers',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus,
			cellFilter: 'currency:"":0'
		},
		{
			field: '90RatioNumberOfTransaction',
			displayName: '90 Day CFTR (# of Transaction)',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus,
			cellFilter: 'percentage:2'
		},
		{
			field: '90ValueTransactionWithCustomers',
			displayName: '90 Day Value of Transaction w/ Customers',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus,
			cellFilter: 'currency:"":0'
		},
		{
			field: '90ValueTransactionNotWithCustomers',
			displayName: '90 Day Value of Transaction w/ Non-Customers',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus,
			cellFilter: 'currency:"":0'
		},
		{
			field: '90RatioValueOfTransactions',
			displayName: '90 Day CFTR (Value)',
			width: 122,
			// enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus,
			cellFilter: 'percentage:2'
		}
		// {
		// 	displayName: 'VaR Limit Size',
		// 	field: 'dVaRLimitSize',
		// 	enableSorting: false,
		// 	headerCellClass: 'dark-header',
		// 	width: 122,
		// 	cellClass: 'text-right',
		// 	cellFilter: 'currency:"":0'
		// },
		// {
		// 	displayName: 'VaR Val Usage',
		// 	field: 'dVaRUsage',
		// 	enableSorting: false,
		// 	headerCellClass: 'dark-header',
		// 	width: 122,
		// 	cellClass: 'text-right',
		// 	cellFilter: 'currency:"":0'
		// },
		// {
		// 	displayName: 'VaR Limit Usage',
		// 	field: 'dVaRLimitUsage',
		// 	enableSorting: false,
		// 	headerCellClass: 'dark-header',
		// 	width: 75,
		// 	cellClass: 'text-right',
		// 	cellFilter: 'percentage:2'
		// },
		// {
		// 	displayName: 'sVaR Limit Size',
		// 	field: 'dSVaRLimitSize',
		// 	enableSorting: false,
		// 	width: 122,
		// 	cellClass: 'text-right',
		// 	cellFilter: 'currency:"":0'
		// },
		// {
		// 	displayName: 'sVaR Val Usage',
		// 	field: 'dSVaRUsage',
		// 	enableSorting: false,
		// 	width: 122,
		// 	cellClass: 'text-right',
		// 	cellFilter: 'currency:"":0'
		// },
		// {
		// 	displayName: 'sVaR Limit Usage',
		// 	field: 'dSVaRLimitUsage',
		// 	enableSorting: false,
		// 	width: 75,
		// 	cellClass: 'text-right',
		// 	cellFilter: 'percentage:2'
		// },
		// {
		// 	displayName: 'Net MV',
		// 	field: 'dNetMV',
		// 	enableSorting: false,
		// 	width: 122,
		// 	cellClass: 'text-right',
		// 	cellFilter: 'currency:"":0'
		// }
	];

}]);
