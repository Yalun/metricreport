(function () {

var comprehensivePnL = angular.module('app.comprehensive-PnL');

comprehensivePnL.controller('ComprehensivePnLDetailAController', ['$scope', '$modal', '$filter', 'ErrorService', '$timeout', 'uiGridConstants', 'ComprehensivePnLReportServices', 'usSpinnerService', '$controller', 'toastr', function ($scope, $modal,  $filter, ErrorService, $timeout, uiGridConstants, ReportServices, usSpinnerService, $controller, toastr){

	$controller('DetailController', {
		$scope: $scope,
		ReportServices: ReportServices
	});

	var params = {
		p_iVolckerDeskId: $scope.deskID,
		p_sPanel: 4,
		p_dtReviewStart: $filter('date')(new Date($scope.date), $scope.queryFormat),
		p_dtReviewEnd: $filter('date')(new Date($scope.date), $scope.queryFormat),
		p_bIsMultidayView: false,
		p_bBreachOnly: false,
		p_sExtract: 'Tradebook'
	};

	var columnDefs = [
		{
			field: 'sTradebook',
			displayName: 'Tradebook',
			width: 100,
			enableSorting: true,
			enableFiltering: true,
			enableCellEdit: false,
			pinnedLeft: 'true',
			enableColumnResizing: false,
			// cellTemplate: '<div ng-if="row.entity.$$treeLevel === 0" class="ui-grid-cell-contents" ng-bind="row.entity[col.field]">{{row.entity[col.field]}}</div>'
			cellTemplate:'<div class="ui-grid-cell-contents"><a ng-click=\"grid.appScope.showPosition(row)\" ng-bind="row.entity[col.field]">{{row.entity[col.field]}}</a></div>'
		},
		{
			field: 'oComprehensivePnLDTO.dAmount',
			displayName: 'Comprehensive P&L',
			width: 122,
			enableSorting: false,
			cellClass: ReportServices.editableIfDeskLevel,
			cellEditableCondition: ReportServices.isDeskLevel,
			type: 'number',
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellFilter: 'currency:"":0'
		},
		{
			field: 'oExistingPositionPnLDTO.dAmount',
			displayName: 'P&L due to Existing Positions',
			width: 90,
			cellClass: ReportServices.editableIfDeskLevel,
			cellEditableCondition: ReportServices.isDeskLevel,
			type: 'number',
			enableSorting: false,
			enableCellEdit: true,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellFilter: 'currency:"":0'
		},
		{
			field: 'oNewPositionPnLDTO.dAmount',
			displayName: 'P&L due to New Positions',
			width: 122,
			enableCellEdit: true,
			cellClass: ReportServices.editableIfDeskLevel,
			cellEditableCondition: ReportServices.isDeskLevel,
			type: 'number',
			enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellFilter: 'currency:"":0'
		},
		{
			field: 'oResidualPnLDTO.dAmount',
			displayName: 'Residual P&L',
			width: 122,
			enableCellEdit: false,
			enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellFilter: 'currency:"":0',
			cellClass: ReportServices.cellStatus
		},
		{
			field: 'oPercPLExistingPositionDTO.sPercentage',
			displayName: 'Percentage of P&L due to Existing Positions',
			width: 140,
			enableCellEdit: false,
			enableSorting: false,
			enableFiltering: false,
			cellClass: ReportServices.cellStatus
		},
		{
			field: 'oPercPLNewPositionDTO.sPercentage',
			displayName: 'Percentage of P&L due to New Positions',
			width: 120,
			enableCellEdit: false,
			enableSorting: false,
			enableFiltering: false,
			cellClass: ReportServices.cellStatus
		},
		{
			field: 'oPercPLResidualPositionDTO.sPercentage',
			displayName: 'Percentage of P&L categorized as Residual',
			width: 130,
			enableCellEdit: false,
			enableSorting: false,
			enableFiltering: false,
			cellClass: ReportServices.cellStatus
		},
		{
			field: 'oRiskFactorPnLDTO.dAmount',
			displayName: 'P&L due to Changes in Risk Factors',
			width: 122,
			enableSorting: false,
			enableCellEdit: true,
			cellClass: ReportServices.editableIfDeskLevel,
			cellEditableCondition: ReportServices.isDeskLevel,
			type: 'number',
			enableFiltering: false,
			cellFilter: 'currency:"":0',
			headerCellClass: 'dark-header',
		},
		{
			field: 'oActualCashFlowPnLDTO.dAmount',
			displayName: 'P&L due to Actual Cash Flows',
			width: 122,
			cellClass: ReportServices.editableIfDeskLevel,
			cellEditableCondition: ReportServices.isDeskLevel,
			type: 'number',
			enableSorting: false,
			enableCellEdit: true,
			enableFiltering: false,
			cellFilter: 'currency:"":0',
			headerCellClass: 'dark-header',
		},
		{
			field: 'oCarryPnLDTO.dAmount',
			displayName: 'P&L due to Carry',
			width: 122,
			enableCellEdit: true,
			cellClass: ReportServices.editableIfDeskLevel,
			cellEditableCondition: ReportServices.isDeskLevel,
			type: 'number',
			enableSorting: false,
			enableFiltering: false,
			cellFilter: 'currency:"":0',
			headerCellClass: 'dark-header',
		},
		{
			field: 'oValuationAdjustmentPnLDTO.dAmount',
			displayName: 'P&L due to Reserve or Valuation Adjustment Changes',
			width: 160,
			enableCellEdit: true,
			cellClass: ReportServices.editableIfDeskLevel,
			cellEditableCondition: ReportServices.isDeskLevel,
			type: 'number',
			enableSorting: false,
			enableFiltering: false,
			cellFilter: 'currency:"":0',
			headerCellClass: 'dark-header',
		},
		{
			field: 'oTradeChangePnLDTO.dAmount',
			displayName: 'P&L due to Trade Changes',
			width: 122,
			enableCellEdit: true,
			cellClass: ReportServices.editableIfDeskLevel,
			cellEditableCondition: ReportServices.isDeskLevel,
			type: 'number',
			enableSorting: false,
			enableFiltering: false,
			enableHiding: false,
			cellFilter: 'currency:"":0',
			headerCellClass: 'dark-header',
		},
		{
			field: 'oOtherPnLDTO.dAmount',
			displayName: 'Other',
			width: 122,
			enableCellEdit: false,
			enableSorting: false,
			enableFiltering: false,
			enableHiding: false,
			cellFilter: 'currency:"":0',
			headerCellClass: 'dark-header',
			cellClass: ReportServices.cellStatus
		},
		{
			field: 'o30VolatilityDTO.dAmount',
			displayName: 'Volatility of 30 Calendar Day Lag P&L',
			width: 122,
			enableCellEdit: false,
			cellClass: ReportServices.cellStatus,
			enableSorting: false,
			enableFiltering: false,
			enableHiding: false,
			cellFilter: 'currency:"":0'
		},
		{
			field: 'o60VolatilityDTO.dAmount',
			displayName: 'Volatility of 60 Calendar Day Lag P&L',
			width: 122,
			enableCellEdit: false,
			cellClass: ReportServices.cellStatus,
			enableSorting: false,
			enableFiltering: false,
			enableHiding: false,
			cellFilter: 'currency:"":0'
		},
		{
			field: 'o90VolatilityDTO.dAmount',
			displayName: 'Volatility of 90 Calendar Day Lag P&L',
			width: 122,
			enableCellEdit: false,
			cellClass: ReportServices.cellStatus,
			enableSorting: false,
			enableFiltering: false,
			enableHiding: false,
			cellFilter: 'currency:"":0'
		}
	];

	function showPosition (row) {
		var modalInstance = $modal.open({
			templateUrl: 'ComprehensivePnLPositionLimitUsage.html',
			controller: 'ComprehensivePnLPositionUsageController',
			size: 'lg',
			windowClass: 'modal-xl',
			resolve: {
				'params': function () {
					return {
						p_bIsMultidayView: false,
						p_sDesk: $scope.desk.sVolckerDeskName,
						p_iVolckerDeskId: $scope.desk.VolckerDeskID,
						p_dtReviewStart: $scope.desk.dtReview,
						p_dtReviewEnd: $scope.desk.dtReview,
						p_sBook: row.entity.sTradebook,
						p_sExtract: 'Position'
					}
				}
			}
		});
	};

	$scope.gridScope.showPosition = showPosition;

	$scope.exportToExcel = function () {
		ReportServices.exportToCsv(params);
	};

	$scope.reviewReport = function () {
		$scope.isReviewing = true;
		var data = [];
		$scope.desk.bIsReviewClicked = true;
		data.push($scope.desk);

		ReportServices.review(data, function(response) {
			$scope.desk.sDescription = 'Reviewed';
			$scope.desk.ReviewStatusID = 1;
			$scope.desk.sReviewerLogin = response.data[0].sReviewerLogin;
			//$scope.desk.sComment = data[0].sComment;

			$scope.isReviewing = false;
			ReportServices.isOutOfSync.multiDay = true;
			ReportServices.isOutOfSync.day = true;
			toastr.success('Desk Reviewed');
		}, function (error) {
			$scope.isReviewing = false;
			toastr.error(ErrorService.handleError(error.data));
		});
	};
	
	$scope.showDocument = function () {
		$scope.documentModal(null, $scope.desk);
	};

	$scope.showComment = function () {
		$scope.commentModal(null, $scope.desk.ReviewID);
	};

	function overridePnL (rowEntity, colDef, newVal, oldVal) {
		var colDisplayName = colDef.displayName;
		var colDefField = colDef.field;
		// var scenario = colDefField.substring(0, colDefField.indexOf('.'));
		if (newVal === null || newVal === undefined || newVal === '') {
			rowEntity[colDefField] = oldVal;
			return;
		}
		if (newVal !== oldVal) {
			confirmOverride(rowEntity, colDefField, colDisplayName, newVal, oldVal, uiGridConstants);
		}
	}

	function confirmOverride (entity, colDefField, colDisplayName, newVal, oldVal, uiGridConstants ) {
		var modalInstance = $modal.open({
			templateUrl: 'common/templates/overrideModal.html',
			controller: 'OverrideModalController',
			size: 'md',
			scope: $scope,
			backdrop: 'static',
			keyboard: false,
			resolve: {
				'OverrideURL': function () {
					return ReportServices.OverrideURL;
				},
				'entity' : function () {
					return {
						newVal : newVal,
						oldVal : oldVal,
						type: colDisplayName
					}
				},
				'overrideParams' : function () {
					return {
						p_iReviewId: $scope.desk.ReviewID,
						p_sOverrideField: colDefField,
						p_dOverrideValue: newVal,
						p_sComment: ''
					};
				},
				'onOverrideSucess': function () {
					return function () {
						$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.EDIT);
						
						ReportServices.isOutOfSync.multiDay = true;
						ReportServices.isOutOfSync.day = true;

						$scope.getDetailReport();
					}
				},
				'resetVal' : function () {
					return function () {
						entity[colDefField] = oldVal;
						$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.EDIT);
					}
				}
			}
		});
	}

	$scope.detailGridOptions = {
		virtualizationThreshold: 50,
		enableFiltering: true,
		enableCellEdit: true,
		enableCellEditOnFocus: true,
		enableColumnMoving: false,
		enableSorting: true,
		enablePinning: true,
		enableGridMenu: false,
		enableColumnMenus: false,
		// enableColumnResizing: true,
		onRegisterApi : function (gridApi) {
			$scope.gridApi = gridApi;

			// $timeout(function () {
			// 	$scope.gridApi.saveState.restore($scope, $scope.selectedTemplate.state);
			// },0);

			$scope.gridApi.edit.on.afterCellEdit(null, overridePnL);
		},
		appScopeProvider: $scope.gridScope,
		columnDefs: columnDefs
	};

	function flattenBook (data) {
		var oPLItemDTO = data.oPLItemDTO;
		var bookArray = oPLItemDTO.oDetailsDTOData;
		var flattenedArray = [];
		var deskLevelDetail = {
			"oComprehensivePnLDTO": oPLItemDTO.oComprehensivePnLDTO,
			"oExistingPositionPnLDTO": oPLItemDTO.oExistingPositionPnLDTO,
			"oNewPositionPnLDTO": oPLItemDTO.oNewPositionPnLDTO,
			"oResidualPnLDTO": oPLItemDTO.oResidualPnLDTO,
			"oPercPLExistingPositionDTO": oPLItemDTO.oPercPLExistingPositionDTO,
			"oPercPLNewPositionDTO": oPLItemDTO.oPercPLNewPositionDTO,
			"oPercPLResidualPositionDTO": oPLItemDTO.oPercPLResidualPositionDTO,
			"oRiskFactorPnLDTO": oPLItemDTO.oRiskFactorPnLDTO,
			"oActualCashFlowPnLDTO": oPLItemDTO.oActualCashFlowPnLDTO,
			"oCarryPnLDTO": oPLItemDTO.oCarryPnLDTO,
			"oValuationAdjustmentPnLDTO": oPLItemDTO.oValuationAdjustmentPnLDTO,
			"oTradeChangePnLDTO": oPLItemDTO.oTradeChangePnLDTO,
			"oOtherPnLDTO": oPLItemDTO.oOtherPnLDTO,
			"o30VolatilityDTO": oPLItemDTO.o30VolatilityDTO,
			"o60VolatilityDTO": oPLItemDTO.o60VolatilityDTO,
			"o90VolatilityDTO": oPLItemDTO.o90VolatilityDTO,

		};

		flattenedArray.push(deskLevelDetail);
		for (var i = 0; i< bookArray.length; i++) {
			flattenedArray.push(bookArray[i]);
		}
		//re-append the flatten array back to the original data
		data.flattenedDetailsDTOData = flattenedArray;
		return data;
	}

	function storeDeskLevelAdjustments (oPLItemDTO) {
		$scope.deskLevelAdjustment = {};
		if (oPLItemDTO.oComprehensivePnLDTO && oPLItemDTO.oComprehensivePnLDTO.bIsOverridden) {
			$scope.deskLevelAdjustment.oComprehensivePnLDTO = 'Comprehensive P&L';
		}
		if (oPLItemDTO.oExistingPositionPnLDTO && oPLItemDTO.oExistingPositionPnLDTO.bIsOverridden) {
			$scope.deskLevelAdjustment.oExistingPositionPnLDTO = 'P&L due to Existing Positions';
		}
		if (oPLItemDTO.oNewPositionPnLDTO && oPLItemDTO.oNewPositionPnLDTO.bIsOverridden) {
			$scope.deskLevelAdjustment.oNewPositionPnLDTO = 'P&L due to New Positions';
		}
		if (oPLItemDTO.oRiskFactorPnLDTO && oPLItemDTO.oRiskFactorPnLDTO.bIsOverridden) {
			$scope.deskLevelAdjustment.oRiskFactorPnLDTO = 'P&L due to Changes in Risk Factors';
		}
		if (oPLItemDTO.oActualCashFlowPnLDTO && oPLItemDTO.oActualCashFlowPnLDTO.bIsOverridden) {
			$scope.deskLevelAdjustment.oActualCashFlowPnLDTO = 'P&L due to Actual Cash Flows';
		}
		if (oPLItemDTO.oCarryPnLDTO && oPLItemDTO.oCarryPnLDTO.bIsOverridden) {
			$scope.deskLevelAdjustment.oCarryPnLDTO = 'P&L due to Carry';
		}
		if (oPLItemDTO.oValuationAdjustmentPnLDTO && oPLItemDTO.oValuationAdjustmentPnLDTO.bIsOverridden) {
			$scope.deskLevelAdjustment.oValuationAdjustmentPnLDTO = 'P&L due to Reserve or Valuation Adjustment Changes';
		}
		if (oPLItemDTO.oTradeChangePnLDTO && oPLItemDTO.oTradeChangePnLDTO.bIsOverridden) {
			$scope.deskLevelAdjustment.oTradeChangePnLDTO = 'P&L due to Trade Changes';
		}
		
	}

	$scope.getDetailReport = function () {
		usSpinnerService.spin('detail-spinner');
		$scope.noReportData = false;
		ReportServices.DetailReport.query(params, function (data) {
			$scope.detailGridOptions.data = flattenBook(data[0]).flattenedDetailsDTOData;
			// save a reference
			$scope.desk = data[0];

			storeDeskLevelAdjustments(data[0].oPLItemDTO);
			
			$scope.noReportData = $scope.detailGridOptions.data.length === 0 ? true : false;
			usSpinnerService.stop('detail-spinner');
		}, function (error) {
			usSpinnerService.stop('detail-spinner');
			$scope.error = ErrorService.handleError(error.data);
		});
	};

	$scope.getDetailReport();

}]);

// Modal Controller
comprehensivePnL.controller('ComprehensivePnLPositionUsageController', ['$scope', '$modalInstance', 'params', 'ComprehensivePnLReportServices', 'ErrorService', 'usSpinnerService', function ($scope, $modalInstance, params, ReportServices, ErrorService, usSpinnerService) {

	$scope.selectedDesk = params.p_sDesk;
	$scope.selectedBook = params.p_sBook;

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};

	$scope.positionsGridOptions = {
		enableFiltering: true,
		enableColumnResizing: true,
		enablePinning: true,
		enableColumnMoving: false,
		enableGridMenu: false,
		enableColumnMenus: false
	};

	ReportServices.PositionDetailReport.query(params, function (data) {
		$scope.positionsGridOptions.data = data[0].oPLItemDTO.oDetailsDTOData[0].oDetailsDTOData;

		$scope.noPositionData = $scope.positionsGridOptions.data.length === 0 ? true : false;

		usSpinnerService.stop('position-spinner');
	}, function (error) {
		$scope.error = ErrorService.handleError(error.data);
		usSpinnerService.stop('position-spinner');
	});

	$scope.exportPosition = function() {
		ReportServices.exportToCsv(params);
	};

	$scope.positionsGridOptions.columnDefs = [
		{
			field: 'sCusip',
			displayName: 'Position',
			width: 95,
			enableFiltering: true,
			enableSorting: true,
			enableHiding: false,
			pinnedLeft: true,
			enableHiding: false,
			enableColumnResizing: true
		},
		{
			field: 'sSecurityDescription',
			displayName: 'Security Description',
			width: 180,
			enableHiding: false,
			enableFiltering: false
		},
		{
			field: 'sProductHierarchy1',
			displayName: 'Product Hierarchy 1',
			enableHiding: false,
			width: 180,
			enableFiltering: false
		},
		{
			field: 'sProductHierarchy2',
			displayName: 'Product Hierarchy 2',
			enableHiding: false,
			width: 180,
			enableFiltering: false
		},
		{
			field: 'sProductHierarchy3',
			displayName: 'Product Hierarchy 3',
			enableHiding: false,
			width: 180,
			enableFiltering: false
		},
		{
			field: 'oPositionAmountTMinus1DTO.dAmount',
			displayName: 'T-1 Position',
			width: 122,
			enableSorting: false,
			// enableCellEdit: true,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: 'oExistingPositionPnLDTO.dAmount',
			displayName: 'P&L due to Existing Positions',
			width: 122,
			enableSorting: false,
			// enableCellEdit: true,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: 'oNewPositionPnLDTO.dAmount',
			displayName: 'P&L due to New Positions',
			width: 122,
			enableCellEdit: true,
			enableSorting: false,
			enableFiltering: false,
			headerCellClass: 'dark-header',
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: 'oRiskFactorPnLDTO.dAmount',
			displayName: 'P&L due to Changes in Risk Factors',
			width: 122,
			enableSorting: false,
			enableFiltering: false,
			cellFilter: 'currency:"":0',
			headerCellClass: 'dark-header',
			cellClass: 'text-right'
		},
		{
			field: 'oActualCashFlowPnLDTO.dAmount',
			displayName: 'P&L due to Actual Cash Flows',
			width: 132,
			enableSorting: false,
			enableFiltering: false,
			cellFilter: 'currency:"":0',
			headerCellClass: 'dark-header',
			cellClass: 'text-right'
		},
		{
			field: 'oCarryPnLDTO.dAmount',
			displayName: 'P&L due to Carry',
			width: 122,
			enableSorting: false,
			enableFiltering: false,
			cellFilter: 'currency:"":0',
			headerCellClass: 'dark-header',
			cellClass: 'text-right'
		},
		{
			field: 'oValuationAdjustmentPnLDTO.dAmount',
			displayName: 'P&L due to Reserve or Valuation Adjustment Changes',
			width: 160,
			enableSorting: false,
			enableFiltering: false,
			cellFilter: 'currency:"":0',
			headerCellClass: 'dark-header',
			cellClass: 'text-right'
		},
		{
			field: 'oTradeChangePnLDTO.dAmount',
			displayName: 'P&L due to Trade Changes',
			width: 122,
			enableSorting: false,
			enableFiltering: false,
			enableHiding: false,
			cellFilter: 'currency:"":0',
			headerCellClass: 'dark-header',
			cellClass: 'text-right'
		},
		{
			field: 'oOtherPnLDTO.dAmount',
			displayName: 'Other',
			width: 122,
			enableSorting: false,
			enableFiltering: false,
			enableHiding: false,
			cellFilter: 'currency:"":0',
			headerCellClass: 'dark-header',
			cellClass: 'text-right'
		}
	];

}]);

})();