angular.module('app.comprehensive-PnL', ['ui.router']).config(['$stateProvider', function($stateProvider) {
    var moduleName = 'comprehensive-PnL';
    var controllerPrefix = 'ComprehensivePnL';

    $stateProvider.state( moduleName , {
        url:         '/' + moduleName,
        templateUrl: moduleName + '/report.html',
        controller: controllerPrefix + 'Controller',
        resolve: {
            templates : function (TemplateService, $q) {
                return $q.all([
                    [],[],[]
                    // TemplateService.getMultiDayTemplates(controllerPrefix+'A'),
                    // TemplateService.getDayTemplates(controllerPrefix+'A'),
                    // TemplateService.getDetailTemplates(controllerPrefix+'A')
                ]);
            }
        }
    })
        .state( moduleName + '.multi-day-A', {
            url:         '/multi-day-a',
            templateUrl: moduleName + '/report-multi-day-a.html',
            controller: controllerPrefix + 'MultiDayAController',
            resolve: {
                VolckerDesks: function ($q, ResourceService, toastr) {
                    var deferred = $q.defer();

                    ResourceService.Desks.query(function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        toastr.error(error.data);
                        deferred.reject(error);
                    });

                    return deferred.promise;
                }
            }
        })
        .state( moduleName + '.day-A', {
            url:         '/day-a',
            templateUrl: moduleName + '/report-day-a.html',
            controller: controllerPrefix + 'DayAController'
        })
        .state( moduleName + '.detail-A', {
            url:         '/detail-a/:desk/:deskID/:date',
            templateUrl: moduleName + '/report-detail-a.html',
            controller: controllerPrefix + 'DetailAController'
        })
        // .state( moduleName + '.chart-A', {
        //     url:         '/chart-a',
        //     templateUrl: moduleName + '/report-chart-a.html',
        //     controller: controllerPrefix + 'ChartAController'
        // })

        // Sub Panel B
        .state( moduleName + '.multi-day-B', {
            url:         '/multi-day-b',
            templateUrl: moduleName + '/report-multi-day-b.html',
            controller: controllerPrefix + 'MultiDayBController',
            resolve: {
                VolckerDesks: function ($q, ResourceService, toastr) {
                    var deferred = $q.defer();

                    ResourceService.Desks.query(function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        toastr.error(error.data);
                        deferred.reject(error);
                    });

                    return deferred.promise;
                }
            }
        })
        .state( moduleName + '.day-B', {
            url:         '/day-b',
            templateUrl: moduleName + '/report-day-b.html',
            controller: controllerPrefix + 'DayBController'

        })
        .state( moduleName + '.detail-B', {
            url:         '/detail-b/:desk/:deskID/:date',
            templateUrl: moduleName + '/report-detail-b.html',
            controller: controllerPrefix + 'DetailBController'

        })
        // .state( moduleName + '.chart-B', {
        //     url:         '/chart-b',
        //     templateUrl: moduleName + '/report-chart-b.html',
        //     controller: controllerPrefix + 'ChartBController'
        // });

}]);