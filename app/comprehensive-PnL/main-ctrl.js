angular.module('app.comprehensive-PnL').controller('ComprehensivePnLController',['$scope', '$state', '$location', '$controller', 'ComprehensivePnLReportServices', 'ComprehensivePnLReportBServices', 'templates', 'ErrorService', function ($scope, $state, $location, $controller, ReportServices, ReportServicesB, templates, ErrorService) {

	var moduleName = 'comprehensive-PnL';
	
	var baseController = $controller('PanelController', {
		$scope: $scope,
		// ReportServices: ReportServices,
		moduleName: moduleName
	});

	$scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
		if (toState.name.match(/[^.]*/i)[0] !== fromState.name.match(/[^.]*/i)[0]) {
			// $scope.clearCache();
			ReportServices.clearCache();
			ReportServicesB.clearCache();
		} else if (toState.name.slice(-1) !== fromState.name.slice(-1)) {
			// console.log('switch between sub module');
			ReportServices.clearCache();
			ReportServicesB.clearCache();
		} else {
			// console.log('change within same module:', toState.name.match(/[^.]*/i)[0]);
			
			$scope.backTo = fromState.name;
		}
	});

	$scope.seeSubPanelDetail = function (row) {
		// console.log(row);
		var selectedRow = row.entity;

		var selectedDeskDate = encodeURIComponent(selectedRow.dtReview);
		var encodedDeskName = encodeURIComponent(selectedRow.sVolckerDeskName);

		var subPanel = $state.current.name.slice(-1);
		$state.go( '^.detail' + '-' + subPanel ,{desk: encodedDeskName, deskID: selectedRow.VolckerDeskID, date: selectedDeskDate}, {'reload':false});
	};


	function routeToMultiDay (subPanel) {
		$state.transitionTo( moduleName + '.multi-day' + '-' + subPanel);
	}
	
	$scope.transitionToSubView = function (tab, subPanel) {
		$state.transitionTo( moduleName + '.' + tab + '-' + subPanel);
	};

	var pathSuffix = $location.path().slice(-1);

	$scope.subPanel = {
		options: [
			{
				id : 'A',
				name : 'A. Comprehensive P&L Attribution Measurements'
			},
			{
				id : 'B',
				name : 'B. Comprehensive P&L Attribution Measurements by Risk Factor Sensitivity'
			}
		]
	};

	if (pathSuffix === 'a') {
		$scope.subPanel.selected = $scope.subPanel.options[0];
	} else if (pathSuffix === 'b') {
		$scope.subPanel.selected = $scope.subPanel.options[1];
	} else {
		$scope.subPanel.selected = $scope.subPanel.options[0];
	}
	
	$scope.viewSubPanel = function () {
		if ($scope.subPanel.selected.id === 'A') {
			console.log('route to A');
			routeToMultiDay('A');
		} else if ( $scope.subPanel.selected.id === 'B') {
			console.log('route to B');
			routeToMultiDay('B');
		}
	};

	// $scope.multiDayTemplatesA = templates[0];
	// $scope.dayTemplatesA = templates[1];
	// $scope.detailTemplatesA = templates[2];

	// // may not be needed, remove later
	// $scope.multiDayTemplatesB = templates[0];
	// $scope.dayTemplatesB = templates[1];
	// $scope.detailTemplatesB = templates[2];

	function init () {
		$scope.viewSubPanel();
	}
	init();
}]);