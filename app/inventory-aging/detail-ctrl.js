angular.module('app.inventory-aging').controller('InventoryAgingDetailController', ['$scope', '$window', '$modal', '$state', '$location', '$filter', '$anchorScroll', 'ErrorService', 'CommonHelperService', 'CacheService', '$http', 'uiGridConstants', 'InventoryAgingReportServices', 'usSpinnerService', '$controller', '$timeout', 'toastr', function ($scope, $window, $modal, $state, $location, $filter, $anchorScroll, ErrorService, CommonHelperService, CacheService, $http, uiGridConstants, ReportServices, usSpinnerService, $controller, $timeout, toastr){

	$controller('DetailController', {
		$scope: $scope,
		ReportServices: ReportServices
	});
	
	var params = {
		p_iVolckerDeskId: $scope.deskID,
		p_sPanel: 6,
		p_dtReviewStart: $filter('date')(new Date($scope.date), $scope.queryFormat),
		p_dtReviewEnd: $filter('date')(new Date($scope.date), $scope.queryFormat),
		p_bIsMultidayView: false,
		//p_bBreachOnly: false,
		p_sExtract: 'Tradebook'
	};
	
	$scope.exportToExcel = function () {
		console.log('export detail view');
		var exportParams = "p_bIsMultidayView=" + "false" +
					"&p_iVolckerDeskId=" + params.p_iVolckerDeskId +
					"&p_sPanel=" + params.p_sPanel +
					"&p_dtReviewStart=" + params.p_dtReviewStart +
					"&p_dtReviewEnd=" + params.p_dtReviewEnd +
					"&p_sReviewStatus=" + "''" +
					"&p_sExtract=" + "TradeBook" +
					"&p_sDesk=" +
					"&p_sBook=" +
					"&p_bBreachOnly=" + "false";
		//// console.log(encodeURI(params).replace(/'/g, '%27')); //encoding single quote doesn't work...
			
		var sUrl = ReportServices.ExportToCsvURL + encodeURI(exportParams).replace(/'/g, '%27');
		$window.open(sUrl, '_blank', '');
	};

	$scope.showAuditHistory = function () {
		// if ( !$scope.auditHistory || $scope.auditHistory.length === 0) {

			ReportServices.AuditHistory.query(function(data) {
				console.log("got audit history");
				$scope.auditHistory = data;
				$scope.isHistoryShown = !$scope.isHistoryShown;
				
				$location.hash('audit-history');
				$timeout(function () {
					$anchorScroll();
				}, 0);
			});
		// } else {
		// 	$scope.isHistoryShown = !$scope.isHistoryShown;
			
		// 	$location.hash('audit-history');
		// 	$timeout(function () {
		// 		$anchorScroll();
		// 	}, 0);
		// }
	};

	$scope.showDocument = function () {
		$scope.documentModal(null, $scope.desk );
	};

	$scope.showComment = function () {
		$scope.commentModal(null, $scope.desk.ReviewID);
	};
	
	$scope.hidePositionDetail = function () {
		$scope.isPositionLevelShown = false;
	};

	function showPosition (row) {
		$scope.isPositionLevelShown = true;
	};

	var columnDefs = [
		{
			field: 'dataField',
			displayName: 'Data Field',
			pinnedLeft: true,
			width: 150
		},
		// {
		// 	field: 'sDescription',
		// 	displayName: 'Status',
		// 	width: 90,
		// 	enableFiltering: true,
		// 	enableColumnResizing: false,
		// 	enableColumnMenu: false,
		// 	filter: {
		// 		term: '',
		// 		type: uiGridConstants.filter.SELECT,
		// 		selectOptions: [{ value: 'Reviewed', label: 'Reviewed' }, { value: 'Pending', label: 'Pending'} ],
		// 		disableCancelFilterButton: true
		// 		}
		// },
		{
			field: 'securityID',
			displayName: 'Security ID',
			enableFiltering: false,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: 'AggregateValue',
			displayName: 'Aggregate Value',
			enableFiltering: false,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '0to30',
			displayName: '0 to 30 Calendar Days',
			enableFiltering: false,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '31to60',
			displayName: '31 to 60 Calendar Days',
			enableFiltering: false,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '61to90',
			displayName: '61 to 90 Calendar Days',
			enableFiltering: false,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '91to120',
			displayName: '91 to 120 Calendar Days',
			enableFiltering: false,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '121to180',
			displayName: '121 to 180 Calendar Days',
			enableFiltering: false,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '181to365',
			displayName: '181 to 365 Calendar Days',
			enableFiltering: false,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '>365',
			displayName: '> 365 Calendar Days',
			enableFiltering: false,
			width: 142,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		}
	];

	$scope.detailGridOptions = {
		virtualizationThreshold: 50,
		// rowHeight: 30,
		// enableFiltering: true,
		enableCellEdit: false,
		// enableCellEditOnFocus: true,
		enableSorting: true,
		// enablePinning: true,
		enableGridMenu: false,
		enableColumnMenus: false,
		enableColumnResizing: false,
		enableColumnMoving: false,
		expandableRowTemplate: 'inventory-aging/detailSubgridTemplate.html',
		expandableRowHeight: 400,
		onRegisterApi : function (gridApi) {
			$scope.gridApi = gridApi;

		},
		appScopeProvider: $scope.gridScope,
		columnDefs: columnDefs
	};

	$scope.getDetailReport = function () {
		usSpinnerService.spin('detail-spinner');
		$scope.noReportData = false;
		ReportServices.DetailReport.query(params, function (data) {
			// console.log(data);
			// $scope.desk =
			$scope.detailGridOptions.data = data;
			$scope.desk = data;
			for(i = 0; i < $scope.detailGridOptions.data.length; i++){
				$scope.detailGridOptions.data[i].subGridOptions = {
					enableColumnMoving: false,
					enableColumnResizing: false,
					enableColumnMenus: false,
					appScopeProvider: $scope.subGridScope,
					columnDefs: $scope.subGridColDefs,
					data: $scope.detailGridOptions.data[i].securities
				}
			}

			$scope.noReportData = $scope.detailGridOptions.data.length === 0 ? true : false;
			usSpinnerService.stop('detail-spinner');
		}, function (error) {
			usSpinnerService.stop('detail-spinner');
			$scope.error = ErrorService.handleError(error.data);
		});
	};

	$scope.disableReview = function () {
		return $scope.isReviewing;
	};

	$scope.reviewReport = function () {
		$scope.isReviewing = true;
		var data = [];
		$scope.desk.bIsReviewClicked = true;
		data.push($scope.desk);
		console.log($scope.desk);
		$http.post(ReportServices.Review, data).success(function(data) {
			console.log('Success', data);
			$scope.desk.sDescription = 'Reviewed';
			$scope.desk.ReviewStatusID = 1;
			$scope.desk.sReviewerLogin = data[0].sReviewerLogin;
			//$scope.desk.sComment = data[0].sComment;
			
			$scope.isReviewing = false;
			ReportServices.isOutOfSync.multiDay = true;
			ReportServices.isOutOfSync.day = true;
			toastr.success('Desk Reviewed');
		}).error(function (error) {
			console.log('ERROR', error);
			toastr.error(error);
			$scope.isReviewing = false;
		});
	};

	$scope.subGridScope = {
		showPosition: showPosition
	};

	$scope.subGridColDefs = [
		// {
		// 	field: 'sDescription',
		// 	displayName: 'Status',
		// 	width: 90,
		// 	enableFiltering: true,
		// 	enableColumnResizing: false,
		// 	enableColumnMenu: false,
		// 	filter: {
		// 		term: '',
		// 		type: uiGridConstants.filter.SELECT,
		// 		selectOptions: [{ value: 'Reviewed', label: 'Reviewed' }, { value: 'Pending', label: 'Pending'} ],
		// 		disableCancelFilterButton: true
		// 		}
		// },
		{
			field: 'securityID',
			displayName: 'Security ID',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellTemplate:'<div class="ui-grid-cell-contents"><a ng-click=\"grid.appScope.showPosition(row)\" ng-bind="row.entity[col.field]">{{row.entity[col.field]}}</a></div>'
			// cellFilter: 'currency:"":0',
			// cellClass: 'text-right'
		},
		{
			field: 'AggregateValue',
			displayName: 'Aggregate Value',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '0to30',
			displayName: '0 to 30 Calendar Days',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '31to60',
			displayName: '31 to 60 Calendar Days',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '61to90',
			displayName: '61 to 90 Calendar Days',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '91to120',
			displayName: '91 to 120 Calendar Days',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '121to180',
			displayName: '121 to 180 Calendar Days',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '181to365',
			displayName: '181 to 365 Calendar Days',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '>365',
			displayName: '> 365 Calendar Days',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		}
	]

	$scope.getDetailReport();
}]);
