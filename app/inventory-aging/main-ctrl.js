var RFS = angular.module('app.inventory-aging');

RFS.controller('InventoryAgingController',['$scope', '$state', 'InventoryAgingReportServices', 'CacheService', 'templates', '$controller', function ($scope, $state, ReportServices, CacheService, templates, $controller){

	var baseController = $controller('PanelController', {
		$scope: $scope,
		// ReportServices: ReportServices,
		moduleName: 'inventory-aging'
	});

	$scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {

		// clear all cache for previous panel when switching panel
		if (toState.name.match(/[^.]*/i)[0] !== fromState.name.match(/[^.]*/i)[0]) {
			// $scope.clearCache(); // PanelController
			ReportServices.clearCache();
		} else {
			// console.log('change between sibiling routes within ', toState.name.match(/[^.]*/i)[0]);
			$scope.backTo = fromState.name;
		}
	});

	$scope.activeGrid = function (grid) {
		$scope.active = {};
		$scope.active[grid] = true;
	};
	
	$scope.cellStatusClass = function (editable) {
		function classes (grid, row, col, rowRenderIndex, colRenderIndex) {
			var predefinedClasses = ['text-right'];
			// console.log(grid, row, col);
			if (editable && typeof row.entity[col.field] === 'number') {
				predefinedClasses.push('editable');
			}
			return predefinedClasses.join(' ');
		};

		return classes;
	};

	$scope.multiDayTemplates = templates[0];
	$scope.dayTemplates = templates[1];
	$scope.detailTemplates = templates[2];

	$scope.nestedGridColumnDefs = [
		{
			field: 'NameOfRiskFactorSensitivity',
			displayName: 'Name of Risk Factor Sensitivity',
			minWidth: 200
		},
		{
			field: 'IR DV01',
			displayName: 'Total IR DV01',
			cellClass: $scope.cellStatusClass(),
			minWidth: 122,
			cellFilter: 'optionalCurrency:"":0'
		},
		{
			field: '< 1yr',
			displayName: 'KeyRate DV01: < 1yr',
			cellClass: $scope.cellStatusClass(),
			minWidth: 122,
			cellFilter: 'optionalCurrency:"":0'
		},
		{
			field: '1yr-5yr',
			displayName: 'KeyRate DV01: 1yr~5yr',
			cellClass: $scope.cellStatusClass(),
			minWidth: 122,
			cellFilter: 'optionalCurrency:"":0'
		},
		{
			field: '5yr-10yr',
			displayName: 'KeyRate DV01:5yr~10yr',
			cellClass: $scope.cellStatusClass(),
			minWidth: 122,
			cellFilter: 'optionalCurrency:"":0'
		},
		{
			field: '> 10yr',
			displayName: 'KeyRate DV01: > 10yr',
			cellClass: $scope.cellStatusClass(),
			minWidth: 122,
			cellFilter: 'optionalCurrency:"":0'
		},
		{
			field: 'Spread DV01',
			displayName: 'Spread DV01',
			cellClass: $scope.cellStatusClass(),
			minWidth: 122,
			cellFilter: 'optionalCurrency:"":0'
		},
		{
			field: 'SwapSpreadDV01',
			displayName: 'Swap Spread DV01',
			cellClass: $scope.cellStatusClass(),
			minWidth: 122,
			cellFilter: 'optionalCurrency:"":0'
		},
		{
			field: 'Vega',
			displayName: 'IR Vega',
			cellClass: $scope.cellStatusClass(),
			minWidth: 122,
			cellFilter: 'optionalCurrency:"":0'
		},
		{
			field: 'Equity Delta',
			displayName: 'Equity Delta',
			cellClass: $scope.cellStatusClass(),
			minWidth: 122,
			cellFilter: 'optionalCurrency:"":0'
		},
		{
			field: 'Equity Vega',
			displayName: 'Equity Vega',
			cellClass: $scope.cellStatusClass(),
			minWidth: 122,
			cellFilter: 'optionalCurrency:"":0'
		},
		{
			field: 'FX Delta',
			displayName: 'FX Delta',
			cellClass: $scope.cellStatusClass(),
			minWidth: 122,
			cellFilter: 'optionalCurrency:"":0'
		}
	];

	baseController.routeToMultiDay();

}]);