angular.module('app.inventory-aging').controller('InventoryAgingMultiDayController',['$scope', '$filter','$state', '$timeout', '$modal', 'ErrorService', '$http', 'uiGridConstants', 'InventoryAgingReportServices', 'CacheService', 'usSpinnerService', '$controller', '$window', 'toastr', 'VolckerDesks', function ($scope, $filter, $state, $timeout, $modal, ErrorService, $http, uiGridConstants, ReportServices, CacheService, usSpinnerService, $controller, $window, toastr, VolckerDesks){

	var baseController = $controller('MultiDayController', {
		$scope: $scope,
		ReportServices: ReportServices,
		VolckerDesks: VolckerDesks
	});

	// $scope.subGridScope = {
	// 	documentModal: $scope.documentModal,
	// 	commentModal: $scope.commentModal
	// };

	$scope.singleAgeProfile = {
		val: false
	};

	$scope.gridOptions = {
		virtualizationThreshold: 50,
		enableRowSelection: true,
		enablePinning: false,
		enableColumnResizing: true,
		enableFiltering: true,
		enableColumnMenus: false,
		enableSorting: true,
		// expandableRowTemplate: 'inventory-aging/rowTemplate.html',
		// expandableRowHeight: 165,
		onRegisterApi : function (gridApi) {
			$scope.gridApi = gridApi;
			// loading the template
			// $timeout(function () {
			// 	$scope.gridApi.saveState.restore($scope, $scope.selectedTemplate.state);
			// },0);
		},
		appScopeProvider: $scope.gridScope
	};

	$scope.securityAssetsGridOptions = angular.copy($scope.gridOptions);
	$scope.securityLiabilitiesGridOptions = angular.copy($scope.gridOptions);
	$scope.derivativeAssetsGridOptions = angular.copy($scope.gridOptions);
	$scope.derivativesLiabilitiesGridOptions = angular.copy($scope.gridOptions);

	function reviewSingleAgeProfile (selectedReports, selectedRows) {
		var data = getCurrentGridData();
		// $http.post(ReportServices.Review, selectedReports).success(function(data) {
			for (var i = 0; i< selectedRows.length; i++) {
				for (var j = 0; j < data.length; j++) {
					if (selectedRows[i].ReviewID === data[j].ReviewID ) {
						selectedRows[i].sDescription = 'Reviewed';
						// selectedRows[i].ReviewStatusID = 1;
						selectedRows[i].sReviewerLogin = data[j].sReviewerLogin;
						// selectedRows[i].sComment = data[j].sComment;
					}
				}
			}

			$scope.gridApi.selection.clearSelectedRows();
			$scope.isReviewing = false;
		// }).error(function (error) {
			// $scope.gridApi.selection.clearSelectedRows();
			// $scope.isReviewing = false;
			// toastr.error(error);
		// });
	}

	function reviewAllAgeProfiles (selectedReports, selectedRows) {
		// $http.post(ReportServices.Review, selectedReports).success(function(data) {
			for (var i = 0; i< selectedRows.length; i++) {
				for (var data in $scope.allAgeProfileData) {
					for (var j = 0; j < data.length; j++) {
						if (selectedRows[i].ReviewID === data[j].ReviewID ) {
							selectedRows[i].sDescription = 'Reviewed';
							// selectedRows[i].ReviewStatusID = 1;
							selectedRows[i].sReviewerLogin = data[j].sReviewerLogin;
							// selectedRows[i].sComment = data[j].sComment;
						}
					}
				}
				
			}

			$scope.gridApi.selection.clearSelectedRows();
			$scope.isReviewing = false;
		// }).error(function (error) {
			// $scope.gridApi.selection.clearSelectedRows();
			// $scope.isReviewing = false;
			// toastr.error(error);
		// });
	}

	function getCurrentGridData () {
		var currentGrid;
		var currentGridData;
		for (var grid in $scope.active) {
			currentGrid = grid;
		}
		if (!currentGrid || currentGrid === 'securityAssets') {
			currentGridData = $scope.securityAssetsGridOptions.data;
		} else if (currentGrid === 'securityLiabilities') {
			currentGridData = $scope.securityLiabilitiesGridOptions.data;
		} else if (currentGrid === 'derivativeAssets') {
			currentGridData = $scope.derivativeAssetsGridOptions.data;
		} else if (currentGrid === 'derivativeLiabilities') {
			currentGridData = $scope.derivativesLiabilitiesGridOptions.data;
		}
		return currentGridData;
	}

	$scope.reviewReport = function () {
		$scope.isReviewing = true;
		var selectedReports = [];
		var selectedRows = $scope.gridApi.selection.getSelectedRows();

		for (var i = 0; i < selectedRows.length; i++) {
			var entity = angular.copy(selectedRows[i]);
			entity.bIsReviewClicked = true;
			delete entity.subGridOptions;
			// for (var j = 0; j < entity.LimitUsageDTOData.length; j++) {
			// 	delete entity.LimitUsageDTOData[j].sComment;
			// }
			selectedReports.push(entity);
		}

		if ($scope.singleAgeProfile.val) {
			reviewSingleAgeProfile(selectedReports, selectedRows);
		} else {
			reviewAllAgeProfiles(selectedReports, selectedRows);
		}
		
	};

	var exportParams = {};
	$scope.exportToExcel = function() {
		if ($scope.singleAgeProfile.val) {
			//exportParams.singleView = '...'
		} else {
			// export all 4 ...
			//exportParams.singleView = null
		}
		baseController.exportToExcel(6, exportParams);
	};

	function getReports () {
		baseController.initSearch();
		
		var params = {
			p_iVolckerDeskId: $scope.desk.selected.VolckerDeskID,
			p_sDesk: $scope.desk.selected.sVolckerDeskName,
			//p_sPanel: 6,
			p_dtReviewStart: $filter('date')(new Date($scope.dateOptions.startDate), $scope.queryFormat),
			p_dtReviewEnd: $filter('date')(new Date($scope.dateOptions.endDate), $scope.queryFormat),
			// p_sReviewStatus: "''",
			p_bIsMultidayView: true,
			//p_bBreachOnly: $scope.breachOnly.val
		};
		
		exportParams = angular.copy(params);

		ReportServices.MultiDayReport.query(params, function (data) {

			$scope.allAgeProfileData = data;
			// $scope.gridOptions.data = data;
			$scope.securityAssetsGridOptions.data = data.securityAssets;
			$scope.securityLiabilitiesGridOptions.data = data.securityLiabilities;
			$scope.derivativeAssetsGridOptions.data = data.derivativeAssets;
			$scope.derivativesLiabilitiesGridOptions.data = data.derivativeLiability;

			if (data &&
				data.securityAssets.length === 0 &&
				data.securityLiabilities.length === 0 &&
				data.derivativeAssets.length === 0 &&
				data.derivativeLiability.length === 0){
					$scope.noReportData = true;
			} else {
				$scope.noReportData = false;
			}

			// for(var i = 0; i < $scope.gridOptions.data.length; i++){
			// 	$scope.gridOptions.data.subGridOptions = {
			// 		enableColumnResizing: false,
			// 		enableColumnMoving: false,
			// 		enableColumnMenus: false,
			// 		enableSorting: false,
			// 		columnDefs: $scope.nestedGridColumnDefs,
			// 		data: $scope.gridOptions.data.RFSItemDTOData,
			// 		appScopeProvider: $scope.subGridScope
			// 	}
			// }

			baseController.cacheReport($scope.allAgeProfileData);
		}, baseController.onReportError);
	}
	$scope.getReports = getReports;

	$scope.disableExport = function () {
		if ($scope.noReportData === undefined) {
			return true;
		} else {
			return $scope.noReportData;
		}
	};

	$scope.search = function () {
		baseController.searchWrapper(getReports);
	};


	// $scope.gridOptions.columnDefs = [
	var columnDefs = [
		{
			field: 'detail',
			displayName: '',
			//pinnedLeft: true,
			width: 60,
			enableFiltering: false,
			enableSorting: false,
			enableColumnResizing: false,
			cellTemplate:'<div class="ui-grid-cell-contents"><button class="btn btn-xs btn-primary" ng-click="grid.appScope.seeDetail(row)">Detail</button></div>'
		},
		{
			field: 'document',
			displayName: '',
			//pinnedLeft: true,
			enableSorting: false,
			enableFiltering: false,
			enableColumnResizing: false,
			width: 30,
			cellTemplate:'<div class="ui-grid-cell-contents text-center"><a class="glyphicon glyphicon-file" ng-click="grid.appScope.documentModal(row, row.entity)"></a></div>'
		},
		{
			field: 'comment',
			displayName: '',
			//pinnedLeft: true,
			enableSorting: false,
			enableFiltering: false,
			enableColumnResizing: false,
			width: 30,
			cellTemplate:'<div class="ui-grid-cell-contents text-center"><a class="glyphicon glyphicon-pencil" ng-click="grid.appScope.commentModal(row)"></a></div>'
		},
		{
			field: 'dtReview',
			displayName: 'Date',
			//pinnedLeft: true,
			width: 100,
			enableColumnResizing: false,
			filterHeaderTemplate: $scope.datePickerFilter,
			sortingAlgorithm: $scope.dateSortingFn
		},
		{
			field: 'sDescription',
			displayName: 'Status',
			width: 90,
			enableFiltering: true,
			enableColumnResizing: false,
			filter: {
				term: '',
				type: uiGridConstants.filter.SELECT,
				selectOptions: [{ value: 'Reviewed', label: 'Reviewed' }, { value: 'Pending', label: 'Pending'} ],
				disableCancelFilterButton: true
				}
		},
		{
			field: 'sReviewerLogin',
			displayName: 'Reviewer',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: 'AggregateValue',
			displayName: 'Aggregate Value',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '0to30',
			displayName: '0 to 30 Calendar Days',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '31to60',
			displayName: '31 to 60 Calendar Days',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '61to90',
			displayName: '61 to 90 Calendar Days',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '91to120',
			displayName: '91 to 120 Calendar Days',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '121to180',
			displayName: '121 to 180 Calendar Days',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '181to365',
			displayName: '181 to 365 Calendar Days',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '>365',
			displayName: '> 365 Calendar Days',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: 'sComment',
			displayName: 'Comments',
			enableFiltering: false,
			enableSorting: true
		}
	];

	$scope.securityAssetsGridOptions.columnDefs = columnDefs;
	$scope.securityLiabilitiesGridOptions.columnDefs = columnDefs;
	$scope.derivativeAssetsGridOptions.columnDefs = columnDefs;
	$scope.derivativesLiabilitiesGridOptions.columnDefs = columnDefs;
	
	function loadReportFromCache () {
		console.log('load from cache');
		console.log(CacheService.MultiDayReport);
		if (ReportServices.isOutOfSync.multiDay && CacheService.MultiDayReport) {
			console.log('report is modified from detail view, getting an updated version of the list of report');
			$scope.getReports();
		} else {
			if (CacheService.MultiDayReport) {
			// if (CacheService.MultiDayReport && CacheService.MultiDayReport.length > 0) {
				// console.log('load report from cache');

				$scope.allAgeProfileData = CacheService.MultiDayReport;
				$scope.securityAssetsGridOptions.data = $scope.allAgeProfileData.securityAssets;
				$scope.securityLiabilitiesGridOptions.data = $scope.allAgeProfileData.securityLiabilities;
				$scope.derivativeAssetsGridOptions.data = $scope.allAgeProfileData.derivativeAssets;
				$scope.derivativesLiabilitiesGridOptions.data = $scope.allAgeProfileData.derivativeLiability;
				$scope.noReportData = false;
			} else if (CacheService.MultiDayReport &&
						CacheService.MultiDayReport.securityAssets.length === 0 &&
						CacheService.MultiDayReport.securityLiabilities.length === 0 &&
						CacheService.MultiDayReport.derivativeAssets.length === 0 &&
						CacheService.MultiDayReport.derivativeLiability.length === 0 ){
				$scope.noReportData = true;
			}
		}
	}

	loadReportFromCache();
}]);