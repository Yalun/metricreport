angular.module('app.inventory-aging').service('InventoryAgingReportServices', ['BaseFactory', function (BaseFactory) {

	var module = 'inventoryAging';
	var services = new BaseFactory(module);

	return services;

}]);