angular.module('app.inventory-aging').controller('InventoryAgingDayController',['$scope', '$state', '$filter', 'ErrorService', '$http', '$timeout', 'uiGridConstants', 'InventoryAgingReportServices', 'CommonHelperService', 'CacheService', 'usSpinnerService', '$controller', '$window', 'toastr', function ($scope, $state, $filter, ErrorService, $http, $timeout, uiGridConstants, ReportServices, CommonHelperService, CacheService, usSpinnerService, $controller, $window, toastr){

	var baseController = $controller('DayController', {
		$scope: $scope,
		ReportServices: ReportServices
	});

	$scope.singleAgeProfile = {
		val : false
	};

	function reviewSingleAgeProfile (selectedReports, selectedRows) {
		var data = getCurrentGridData();
		// $http.post(ReportServices.Review, selectedReports).success(function(data) {
			for (var i = 0; i< selectedRows.length; i++) {
				for (var j = 0; j < data.length; j++) {
					if (selectedRows[i].ReviewID === data[j].ReviewID ) {
						selectedRows[i].sDescription = 'Reviewed';
						// selectedRows[i].ReviewStatusID = 1;
						selectedRows[i].sReviewerLogin = data[j].sReviewerLogin;
						// selectedRows[i].sComment = data[j].sComment;
					}
				}
			}

			$scope.gridApi.selection.clearSelectedRows();
			$scope.isReviewing = false;
		// }).error(function (error) {
			// $scope.gridApi.selection.clearSelectedRows();
			// $scope.isReviewing = false;
			// toastr.error(error);
		// });
	}

	function reviewAllAgeProfiles (selectedReports, selectedRows) {
		// $http.post(ReportServices.Review, selectedReports).success(function(data) {
			for (var i = 0; i< selectedRows.length; i++) {
				for (var data in $scope.allAgeProfileData) {
					for (var j = 0; j < data.length; j++) {
						if (selectedRows[i].ReviewID === data[j].ReviewID ) {
							selectedRows[i].sDescription = 'Reviewed';
							// selectedRows[i].ReviewStatusID = 1;
							selectedRows[i].sReviewerLogin = data[j].sReviewerLogin;
							// selectedRows[i].sComment = data[j].sComment;
						}
					}
				}
				
			}

			$scope.gridApi.selection.clearSelectedRows();
			$scope.isReviewing = false;
		// }).error(function (error) {
			// $scope.gridApi.selection.clearSelectedRows();
			// $scope.isReviewing = false;
			// toastr.error(error);
		// });
	}

	function getCurrentGridData () {
		var currentGrid;
		var currentGridData;
		for (var grid in $scope.active) {
			currentGrid = grid;
		}
		if (!currentGrid || currentGrid === 'securityAssets') {
			currentGridData = $scope.securityAssetsGridOptions.data;
		} else if (currentGrid === 'securityLiabilities') {
			currentGridData = $scope.securityLiabilitiesGridOptions.data;
		} else if (currentGrid === 'derivativeAssets') {
			currentGridData = $scope.derivativeAssetsGridOptions.data;
		} else if (currentGrid === 'derivativeLiabilities') {
			currentGridData = $scope.derivativesLiabilitiesGridOptions.data;
		}
		return currentGridData;
	}

	$scope.reviewReport = function () {
		$scope.isReviewing = true;
		var selectedReports = [];
		var selectedRows = $scope.gridApi.selection.getSelectedRows();

		for (var i = 0; i < selectedRows.length; i++) {
			var entity = angular.copy(selectedRows[i]);
			entity.bIsReviewClicked = true;
			delete entity.subGridOptions;
			// for (var j = 0; j < entity.LimitUsageDTOData.length; j++) {
			// 	delete entity.LimitUsageDTOData[j].sComment;
			// }
			selectedReports.push(entity);
		}

		if ($scope.singleAgeProfile.val) {
			reviewSingleAgeProfile(selectedReports, selectedRows);
		} else {
			reviewAllAgeProfiles(selectedReports, selectedRows);
		}
		
	};

	$scope.gridOptions = {
		virtualizationThreshold: 50,
		enablePinning: false,
		enableRowSelection: true,
		enableColumnResizing: true,
		enableFiltering: true,
		enableHiding: false,
		enableColumnMenus: false,
		enableSorting: true,
		// expandableRowTemplate: 'inventory-aging/rowTemplate.html',
		// expandableRowHeight: 165,
		onRegisterApi : function (gridApi) {
			$scope.gridApi = gridApi;
			// $timeout(function () {
			// 	$scope.gridApi.saveState.restore($scope, $scope.selectedTemplate.state);
			// },0);
		},
		appScopeProvider: $scope.gridScope
	};

	$scope.securityAssetsGridOptions = angular.copy($scope.gridOptions);
	$scope.securityLiabilitiesGridOptions = angular.copy($scope.gridOptions);
	$scope.derivativeAssetsGridOptions = angular.copy($scope.gridOptions);
	$scope.derivativesLiabilitiesGridOptions = angular.copy($scope.gridOptions);

	var exportParams = {};
	$scope.exportToExcel = function(){
		if ($scope.singleAgeProfile.val) {
			//exportParams.singleView = '...'
		} else {
			// export all 4 ...
			//exportParams.singleView = null
		}
		
		baseController.exportToExcel(6, exportParams);
	};
	
	function getReports() {
		baseController.initSearch();

		var params = {
			p_dtReviewStart: $filter('date')(new Date($scope.dateOptions.datePicked), $scope.queryFormat),
			p_dtReviewEnd: $filter('date')(new Date($scope.dateOptions.datePicked), $scope.queryFormat),
			p_bIsMultidayView: false,
			//p_bBreachOnly: $scope.breachOnly.val
		};
		exportParams = angular.copy(params);

		ReportServices.DayReport.query(params, function (data) {
			$scope.allAgeProfileData = data;
			// $scope.gridOptions.data = data;

			$scope.securityAssetsGridOptions.data = data.securityAssets;
			$scope.securityLiabilitiesGridOptions.data = data.securityLiabilities;
			$scope.derivativeAssetsGridOptions.data = data.derivativeAssets;
			$scope.derivativesLiabilitiesGridOptions.data = data.derivativeLiability;

			if (data &&
				data.securityAssets.length === 0 &&
				data.securityLiabilities.length === 0 &&
				data.derivativeAssets.length === 0 &&
				data.derivativeLiability.length === 0){
					$scope.noReportData = true;
			} else {
				$scope.noReportData = false;
			}

			baseController.cacheReport($scope.allAgeProfileData);
		}, baseController.onReportError);
	}
	$scope.getReports = getReports;

	$scope.disableExport = function () {
		if ($scope.noReportData === undefined) {
			return true;
		} else {
			return $scope.noReportData;
		}
	};
	$scope.searchReports = function () {
		baseController.searchWrapper(getReports);
	};

	var columnDefs = [
		{
			name: 'detail',
			displayName: '',
			//pinnedLeft: true,
			width: 60,
			enableFiltering: false,
			enableSorting: false,
			enableColumnResizing: false,
			cellTemplate:'<div class="ui-grid-cell-contents"><button class="btn btn-xs btn-primary" ng-click="grid.appScope.seeDetail(row)">Detail</button></div>'
		},
		{
			field: 'document',
			displayName: '',
			//pinnedLeft: true,
			enableSorting: false,
			enableFiltering: false,
			enableColumnResizing: false,
			width: 30,
			cellTemplate:'<div class="ui-grid-cell-contents text-center"><a class="glyphicon glyphicon-file" ng-click="grid.appScope.documentModal(row, row.entity)"></a></div>'
		},
		{
			field: 'comment',
			displayName: '',
			//pinnedLeft: true,
			enableSorting: false,
			enableFiltering: false,
			enableColumnResizing: false,
			width: 30,
			cellTemplate:'<div class="ui-grid-cell-contents text-center"><a class="glyphicon glyphicon-pencil" ng-click="grid.appScope.commentModal(row)"></a></div>'
		},
		{
			field: 'sVolckerDeskName',
			displayName: 'Volcker Desk',
			//pinnedLeft: true,
			width: 200,
			enableFiltering: true,
		},
		{
			field: 'sDescription',
			displayName: 'Status',
			width: 90,
			enableFiltering: true,
			enableColumnResizing: false,
			filter: {
				term: '',
				type: uiGridConstants.filter.SELECT,
				selectOptions: [{ value: 'Reviewed', label: 'Reviewed' }, { value: 'Pending', label: 'Pending'} ],
				disableCancelFilterButton: true
				}
		},
		{
			field: 'sReviewerLogin',
			displayName: 'Reviewer',
			enableFiltering: false,
			enableSorting: true,
			width: 100
		},
		{
			field: 'AggregateValue',
			displayName: 'Aggregate Value',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '0to30',
			displayName: '0 to 30 Calendar Days',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '31to60',
			displayName: '31 to 60 Calendar Days',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '61to90',
			displayName: '61 to 90 Calendar Days',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '91to120',
			displayName: '91 to 120 Calendar Days',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '121to180',
			displayName: '121 to 180 Calendar Days',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '181to365',
			displayName: '181 to 365 Calendar Days',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: '>365',
			displayName: '> 365 Calendar Days',
			enableFiltering: false,
			enableSorting: true,
			width: 122,
			cellFilter: 'currency:"":0',
			cellClass: 'text-right'
		},
		{
			field: 'sComment',
			displayName: 'Comments',
			enableFiltering: false,
			enableSorting: true
		}
	];

	$scope.securityAssetsGridOptions.columnDefs = columnDefs;
	$scope.securityLiabilitiesGridOptions.columnDefs = columnDefs;
	$scope.derivativeAssetsGridOptions.columnDefs = columnDefs;
	$scope.derivativesLiabilitiesGridOptions.columnDefs = columnDefs;

	function loadReportFromCache () {
		console.log('load from cache');
		console.log(CacheService.DayReport);
		if (ReportServices.isOutOfSync.day && CacheService.DayReport) {
			console.log('report is modified from detail view, getting an updated version of the list of report');
			$scope.getReports();
		} else {
			if (CacheService.DayReport) {
			// if (CacheService.DayReport && CacheService.DayReport.length > 0) {
				// console.log('load report from cache');

				$scope.allAgeProfileData = CacheService.DayReport;
				$scope.securityAssetsGridOptions.data = $scope.allAgeProfileData.securityAssets;
				$scope.securityLiabilitiesGridOptions.data = $scope.allAgeProfileData.securityLiabilities;
				$scope.derivativeAssetsGridOptions.data = $scope.allAgeProfileData.derivativeAssets;
				$scope.derivativesLiabilitiesGridOptions.data = $scope.allAgeProfileData.derivativeLiability;
				$scope.noReportData = false;
			} else if (CacheService.DayReport &&
						CacheService.DayReport.securityAssets.length === 0 &&
						CacheService.DayReport.securityLiabilities.length === 0 &&
						CacheService.DayReport.derivativeAssets.length === 0 &&
						CacheService.DayReport.derivativeLiability.length === 0 ){
				$scope.noReportData = true;
			}
		}
	}

	loadReportFromCache();
}]);