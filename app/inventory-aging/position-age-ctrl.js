angular.module('app.inventory-aging').controller('PositionAgeController', ['$scope', 'InventoryAgingReportServices', 'usSpinnerService', 'ErrorService', 'uiGridConstants', '$modal', '$filter', function ($scope, ReportServices, usSpinnerService, ErrorService, uiGridConstants, $modal, $filter) {
	
	$scope.cancel = function () {
		// $modalInstance.dismiss('cancel');
	};

	var params = {
		p_iVolckerDeskId: $scope.deskID,
		p_sPanel: 6,
		p_dtReviewStart: $filter('date')(new Date($scope.date), $scope.queryFormat),
		p_dtReviewEnd: $filter('date')(new Date($scope.date), $scope.queryFormat),
		p_bIsMultidayView: false,
		p_bBreachOnly: false,
		p_sExtract: 'Tradebook'
	};

	$scope.positionsGridOptions = {
		enableFiltering: true,
		enableColumnResizing: true,
		enableSorting: true,
		enablePinning: true,
		enableGridMenu: false,
		enableColumnMenus: false,
		enableColumnMoving: false,
		onRegisterApi: function (gridApi) {
			$scope.positionGridApi = gridApi;

			$scope.positionGridApi.edit.on.afterCellEdit(null, overridePositionAge);
		}
	};

	function overridePositionAge (rowEntity, colDef, newVal, oldVal) {
		//console.log(colDef);
		var colDefField = colDef.field;
		if (newVal === null || newVal === undefined || newVal === '') {
			//console.log(rowEntity[colDefField]);
			rowEntity[colDefField] = oldVal;
			return;
		}
		if (newVal !== oldVal) {
			confirmOverride(rowEntity, colDefField, newVal, oldVal, uiGridConstants);
		}
	}
	
	function confirmOverride (entity, colDefField, newVal, oldVal, uiGridConstants ) {
		var modalInstance = $modal.open({
			templateUrl: 'common/templates/overrideModal.html',
			controller: 'OverrideModalController',
			size: 'md',
			scope: $scope,
			backdrop: 'static',
			keyboard: false,
			resolve: {
				'OverrideURL': function () {
					return ReportServices.OverrideURL;
				},
				'entity' : function () {
					return {
						newVal : newVal,
						oldVal : oldVal,
						type: colDefField
					};
				},
				'overrideParams' : function () {
					return {
						p_iReviewId: 1203192381238123,
						p_sScenario: colDefField,
						p_dAggChangePosition: newVal,
						p_sComment: ''
					};
				},
				'onOverrideSucess': function () {
					return function () {
						entity[colDefField].bIsOverridden = true;
						$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.EDIT);
						
						ReportServices.isOutOfSync.multiDay = true;
						ReportServices.isOutOfSync.day = true;
						
						$scope.getDetailReport();
					}
				},
				'resetVal' : function () {
					return function () {
						entity[colDefField] = oldVal;
						$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.EDIT);
					}
				}
			}
		});
	}

	ReportServices.PositionDetailReport.query(params, function(data) {
		$scope.positionsGridOptions.data = data;
		if ($scope.positionsGridOptions.data.length === 0) {
			$scope.noPositionData = true;
		}
		usSpinnerService.stop('position-spinner');
	}, function (error) {
		$scope.error = ErrorService.handleError(error.data);
		usSpinnerService.stop('position-spinner');
	});

	$scope.exportPosition = function() {
		ReportServices.exportToCsv(params);
	};

	function cellStatusClass (editable) {
		function cellClass (grid, row, col, rowRenderIndex, colRenderIndex) {
			var classes = ['text-right'];

			if (editable) {
				classes.push('editable');
			}
			if (row.entity.bIsOverridden) {
				classes.push('revised');
			}
			return classes.join(' ');
		}
		return cellClass;
	}

	$scope.positionsGridOptions.columnDefs = [
		{
			name: 'positionID',
			displayName: 'Security ID',
			width: 120,
			enableFiltering: false, // virtualization causes column header height to change
			enableCellEdit: false,
			pinnedLeft: true
		},
		{
			name: 'positionDate',
			displayName: 'Position Date',
			enableFiltering: false,
			enableCellEdit: false,
			width: 150
		},
		{
			name: 'book',
			displayName: 'Book',
			enableFiltering: false,
			enableCellEdit: false,
			width: 150
		},
		{
			name: 'tradeReference',
			displayName: 'Trade Reference',
			enableFiltering: false,
			enableCellEdit: false,
			width: 150
		},
		{
			name: 'tradeDate',
			displayName: 'Trade Date',
			enableFiltering: false,
			enableCellEdit: false,
			width: 150
		},
		{
			field: 'positionSize',
			displayName: 'Position Size',
			cellFilter: 'optionalCurrency:"":0',
			enableFiltering: false,
			enableCellEdit: false,
			width: 122,
			cellClass: 'text-right'
		},
		{
			field: 'ageBasedOnTradeDate',
			displayName: 'Age based on Trade Date',
			cellFilter: 'optionalCurrency:"":0',
			enableFiltering: false,
			enableCellEdit: false,
			width: 122,
			cellClass: 'text-right'
		},
		{
			field: 'ageAdjustment',
			displayName: 'Age Adjustment',
			cellFilter: 'optionalCurrency:"":0',
			enableFiltering: false,
			enableCellEdit: true,
			type: 'number',
			width: 122,
			cellClass: cellStatusClass(true)
		},
		{
			field: 'postAdjustedAge',
			displayName: 'Post-Adjusted Age',
			cellFilter: 'optionalCurrency:"":0',
			enableFiltering: false,
			enableCellEdit: false,
			width: 122,
			cellClass: cellStatusClass()
		},
		{
			field: 'comment',
			displayName: 'Comments',
			cellFilter: 'optionalCurrency:"":0',
			enableFiltering: false,
			enableCellEdit: false,
			minWidth: 122,
		}
	];

}]);